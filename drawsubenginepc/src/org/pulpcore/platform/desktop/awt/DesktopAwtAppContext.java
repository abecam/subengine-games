/*
    Copyright (c) 2007-2009, Interactive Pulp, LLC
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without 
    modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright 
          notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright 
          notice, this list of conditions and the following disclaimer in the 
          documentation and/or other materials provided with the distribution.
 * Neither the name of Interactive Pulp, LLC nor the names of its 
          contributors may be used to endorse or promote products derived from 
          this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.
 */

package org.pulpcore.platform.desktop.awt;

import java.awt.Component;
import java.awt.DisplayMode;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.net.URL;

import pulpcore.Build;
import pulpcore.Stage;
import pulpcore.image.Colors;
import pulpcore.image.CoreImage;
import pulpcore.platform.AppContext;
import pulpcore.platform.PolledInput;
import pulpcore.platform.Surface;
import pulpcore.platform.applet.BufferedImageSurface;
import pulpcore.scene.Scene;
import pulpcore.util.ByteArray;

public final class DesktopAwtAppContext extends AppContext {

	private final DesktopAwtApplication desktopAwtApplication;
	private SystemTimer timer;
	private Surface surface;
	private DesktopAwtInput inputSystem;
	private Stage stage;
//	private boolean firstFrameDrawn = false;

	public DesktopAwtAppContext(DesktopAwtApplication app, SystemTimer timer) {
		this.desktopAwtApplication = app;
		this.timer = timer;
	}

	void init() {
		setTalkBackField("pulpcore.platform", "Desktop Awt");
		setTalkBackField("pulpcore.platform.timer", timer.getName());
		createSurface(desktopAwtApplication);
		stage = new Stage(surface, this);
	}

	DesktopAwtApplication getApplication() {
		return desktopAwtApplication;
	}

	public int getDefaultBackgroundColor() {
		return desktopAwtApplication.getBackground().getRGB();
	}

	public int getDefaultForegroundColor() {
		return desktopAwtApplication.getForeground().getRGB();
	}

	public Scene createFirstScene() {
		return desktopAwtApplication.createFirstScene();
	}

	public void start() {
		if (stage != null) {
			stage.start();
		}

		if (Build.DEBUG)
			printMemory("App: start");
	}

	public void stop() {
		if (stage != null) {
			stage.stop();
		}
		if (Build.DEBUG)
			printMemory("App: stop");
	}

	public void destroy() {
		super.destroy();
		if (stage != null) {
			stage.destroy();
			stage = null;
		}
		surface = null;
		inputSystem = null;
		setMute(true);
		if (Build.DEBUG)
			printMemory("App: destroy");
	}

	private void createSurface(DesktopAwtApplication app) {
		Component inputComponent = app;
		surface = null;
		surface = new BufferedImageSurface(app);
		setTalkBackField("pulpcore.platform.surface", surface.toString());

		inputSystem = new DesktopAwtInput(inputComponent);
	}

	Component getInputComponent() {
		DesktopAwtInput i = inputSystem;
		if (i != null) {
			return i.getComponent();
		} else {
			return null;
		}
	}

	public int getRefreshRate() {
		// Apply 55 fps limit on old Java 5 versions on Leopard
// FIXME: needs PulpCore 0.11.6+		
//		if (CoreSystem.isMacOSX105()) {
//			try {
//				if (System.getProperty("java.version").compareTo("1.5.0_16") < 0) {
//					return 55;
//				}
//			} catch (Exception ex) {
//			}
//		}
		try {
			GraphicsDevice device = null;
			Component c = getInputComponent();
			if (c != null) {
				GraphicsConfiguration gc = c.getGraphicsConfiguration();
				if (gc != null) {
					device = gc.getDevice();
				}
			}
			if (device == null) {
				device = GraphicsEnvironment.getLocalGraphicsEnvironment()
						.getDefaultScreenDevice();
			}
			int refreshRate = device.getDisplayMode().getRefreshRate();
			if (refreshRate == DisplayMode.REFRESH_RATE_UNKNOWN) {
				return 0;
			} else {
				return refreshRate;
			}
		} catch (Throwable t) {
			// Some old VMs will throw
			// "java.lang.InternalError: Could not get display mode"
			// On device.getDisplayMode(). Perhaps when ddraw is disabled?
			return 0;
		}
	}

	public Stage getStage() {
		return stage;
	}

	public Surface getSurface() {
		return surface;
	}

	public void pollInput() {
		if (inputSystem != null) {
			// This happened a few times at pulpgames.net:
			// java.lang.NullPointerException
			// at pulpcore.platform.applet.AppletAppContext.pollInput(Unknown
			// Source)
			inputSystem.pollInput();
		}
	}

	public PolledInput getPolledInput() {
		return inputSystem.getPolledInput();
	}

	public void requestKeyboardFocus() {
		inputSystem.requestKeyboardFocus();
	}

	public int getCursor() {
		return inputSystem.getCursor();
	}

	public void setCursor(int cursor) {
		inputSystem.setCursor(cursor);
	}

	public String getLocaleLanguage() {
		try {
			return desktopAwtApplication.getLocale().getLanguage();
		} catch (Throwable t) {
			return "";
		}

	}

	public String getLocaleCountry() {
		try {
			return desktopAwtApplication.getLocale().getCountry();
		} catch (Throwable t) {
			return "";
		}
	}

	public CoreImage loadImage(ByteArray in) {

		if (in == null) {
			return null;
		}

		Image image = Toolkit.getDefaultToolkit().createImage(in.getData());

		MediaTracker tracker = new MediaTracker(desktopAwtApplication);
		tracker.addImage(image, 0);
		try {
			tracker.waitForAll();
		} catch (InterruptedException ex) {
		}

		int width = image.getWidth(null);
		int height = image.getHeight(null);
		if (width <= 0 || height <= 0) {
			return null;
		}

		int[] data = new int[width * height];
		PixelGrabber pixelGrabber = new PixelGrabber(image, 0, 0, width,
				height, data, 0, width);
		boolean success = false;
		try {
			success = pixelGrabber.grabPixels();
		} catch (InterruptedException ex) {
		}

		if (success) {
			boolean isOpaque = true;

			// Premultiply alpha
			for (int i = 0; i < data.length; i++) {
				data[i] = Colors.premultiply(data[i]);
				if (isOpaque && (data[i] >>> 24) < 255) {
					isOpaque = false;
				}
			}
			return new CoreImage(width, height, isOpaque, data);
		} else {
			return null;
		}
	}

	@Override
	public String getAppProperty(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putUserData(String key, byte[] data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public byte[] getUserData(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeUserData(String key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showDocument(String url, String target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public URL getBaseURL() {
		// TODO Auto-generated method stub
		return null;
	}
}
