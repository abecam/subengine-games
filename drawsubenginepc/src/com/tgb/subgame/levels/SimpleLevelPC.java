/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subgame.levels;

import java.util.ArrayList;

import com.tgb.subgame.unitspc.Boat;
import com.tgb.subgame.unitspc.Submarine;
import com.tgb.subgame.unitspc.sensors.*;
import com.tgb.subgame.*;
//Simport pulpcore.image.CoreImage;

/**
 * A simple example of level, hard-coded
 * @author Alain Becam
 *
 */
public class SimpleLevelPC implements ILevelPC
{
	LevelMap theMap;
	
	
	public SimpleLevelPC(LevelMap theMap)
	{
		this.theMap=theMap;
	}
	
	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getAlliesSubs()
	 */
	public void addAlliesSubs()
	{	
		;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getAlliesrBoats()
	 */
	public void addAlliesBoats()
	{
		;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getEnemiesBoats()
	 */
	public void addEnemiesBoats()
	{
		
		//CoreImage boatImage = Boat.getImageForMe();
		
		// TODO Auto-generated method stub
		Boat caspian = new Boat(theMap);
		
		caspian.setComplement(100);
		caspian.setCost(200000000);
		caspian.setCurrentSpeed(20);
		caspian.setEnergy(1000);
		caspian.setMaxSpeed(35);
		caspian.setOrientation(0);
		caspian.setName("Caspian");
		caspian.setResistance(10);
		caspian.setVisibilityLevel(0);
		caspian.setNoiseSignature(2);
		caspian.setVisibilitySignature(0);
		caspian.setPosX(200);
		caspian.setPosY(50);
		caspian.setType(Boat.CARRIER);
		caspian.createGfx( 200, 50, 0, 0, 20);
		caspian.hideMe();
		
		theMap.addEnemiesBoats(caspian);
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getEnemiesSubs()
	 */
	public void addEnemiesSubs()
	{
		
		//CoreImage subImage = Submarine.getImageForMe();	
		Submarine lisian = new Submarine(theMap);
		System.out.println("Here 1");
		lisian.setComplement(100);
		lisian.setCost(200000000);
		lisian.setCurrentSpeed(20);
		lisian.setEnergy(1000);
		lisian.setMaxSpeed(35);
		lisian.setOrientation(0);
		lisian.setName("Lisian");
		lisian.setResistance(10);
		lisian.setVisibilityLevel(0);
		lisian.setNoiseSignature(2);
		lisian.setVisibilitySignature(0);
		lisian.setPosX(50);
		lisian.setPosY(500);
		lisian.setDepth(-50);
		lisian.setType(Submarine.NUKE);
		System.out.println("Here 2");
		lisian.createGfx(50, 100, 0, 0, 20);
		lisian.hideMe();
		System.out.println("Here 3");
		theMap.addEnemiesSubs(lisian);
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getNeutralBoats()
	 */
	public void addNeutralBoats()
	{
		// TODO Auto-generated method stub
		;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getNeutralSubs()
	 */
	public void addNeutralSubs()
	{
		// TODO Auto-generated method stub
		;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getOurBoats()
	 */
	public void addOurBoats()
	{
		
		//CoreImage boatImage = Boat.getImageForMe();
		Sonar ourFrontSonar;
		ourFrontSonar= new Sonar(theMap);
		ourFrontSonar.createGfx(0, 0, 0, 0, 1, 1);
		ourFrontSonar.setPosAttach(40, 0, 0);
		ourFrontSonar.activate();
		
		Radar ourRadar;
		ourRadar= new Radar(theMap);
		ourRadar.createGfx(0, 0, 0, 0, 1, 1);
		ourRadar.setPosAttach(2, -7, 0);
		ourRadar.setPower(10);
		ourRadar.activate();
		
		// TODO Auto-generated method stub
		Boat caspian = new Boat(theMap);
		caspian.getSensors().add(ourFrontSonar);
		caspian.addAttachedObject(ourFrontSonar);
		caspian.getSensors().add(ourRadar);
		caspian.addAttachedObject(ourRadar);
		caspian.setComplement(5000);
		caspian.setCost(800000000);
		caspian.setCurrentSpeed(20);
		caspian.setEnergy(1000);
		caspian.setMaxSpeed(35);
		caspian.setOrientation(0);
		caspian.setName("Caspian");
		caspian.setResistance(100);
		caspian.setVisibilityLevel(0);
		caspian.setNoiseSignature(2);
		caspian.setVisibilitySignature(0);
		caspian.setPosX(300);
		caspian.setPosY(50);
		caspian.setType(Boat.CARRIER);
		caspian.createGfx( 300, 50, 0, 0, 20);
		System.out.println("Caspian boat created");
		
	
		Boat lisian = new Boat(theMap);

		lisian.setComplement(5000);
		lisian.setCost(800000000);
		lisian.setCurrentSpeed(30);
		lisian.setEnergy(1000);
		lisian.setMaxSpeed(35);
		lisian.setOrientation(0.3);
		lisian.setName("Lisian");
		lisian.setResistance(100);
		lisian.setVisibilityLevel(0);
		lisian.setNoiseSignature(2);
		lisian.setVisibilitySignature(0);
		lisian.setPosX(50);
		lisian.setPosY(300);
		lisian.setType(Boat.CRUISER);
		lisian.createGfx( 50, 300, 0, 0, 20);
		
		theMap.addOurBoats(lisian);
		theMap.addOurBoats(caspian);
		
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getOurSubs()
	 */
	public void addOurSubs()
	{
		
		//CoreImage subImage = Submarine.getImageForMe();
		
	
		Submarine lisian = new Submarine(theMap);
		lisian.setComplement(100);
		lisian.setCost(200000000);
		lisian.setCurrentSpeed(30);
		lisian.setEnergy(1000);
		lisian.setMaxSpeed(35);
		lisian.setOrientation(-0.5);
		lisian.setName("Lisian");
		lisian.setResistance(10);
		lisian.setVisibilityLevel(0);
		lisian.setNoiseSignature(2);
		lisian.setVisibilitySignature(0);
		lisian.setPosX(50);
		lisian.setPosY(500);
		lisian.setType(Submarine.NUKE);
		lisian.createGfx(50, 500, 0, 0, 20);
		
		
		theMap.addOurSubs(lisian);
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.levels.ILevel#getGlobalSensors()
	 */
	public void addGlobalSensor()
	{
		Satellite myNewSatellite= new Satellite(theMap);
		myNewSatellite.createGfx(100, 100, 0, Math.PI/2 + 0.1, 20, 10);
		
		theMap.addGlobalSensor(myNewSatellite);
	}

	public void addOthers()
	{
		// TODO Auto-generated method stub
		
	}
}
