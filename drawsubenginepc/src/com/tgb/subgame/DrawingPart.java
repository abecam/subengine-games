/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subgame;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;
import java.util.ArrayList;
import pulpcore.Input;
import pulpcore.Stage;
import pulpcore.image.CoreImage;
import pulpcore.scene.Scene2D;
import pulpcore.sprite.*;

import com.tgb.subengine.RenderingManager;
import com.tgb.subengine.particlessystem.ParticleManager;
import com.tgb.subengine.sound.PlayFM;
import com.tgb.subengine.gamesystems.IGamePart;
import com.tgb.subengine.gameentities.*;
import com.tgb.subengine.gfxentities.FlatSpritePC;
import com.tgb.subengine.gfxentities.FilledRectanglePC;
import com.tgb.subengine.gfxentities.SpritePC;
import com.tgb.subgame.unitspc.*;
import com.tgb.subgame.unitspc.sensors.*;

/**
 * The tactical map is the central part, the level, managing the actual (hopefully entertaining)
 * game
 * @author Alain Becam
 * TODO: Have more generic manageable units and not only boat and sub (so find a way to have
 * unit dependent interface).
 */
public class DrawingPart extends LevelMap implements IGamePart
{
	private static final int MINIMAL_NUMBER_OF_ELEMENTS = 2000;

	//public final static int START_MENU=700; // Where the menu start (so where the map shouldn't be) 
	
	private static final int MAX_NB_ON_SCREEN = 80000;

	boolean upKey,downKey,leftKey,rightKey=false;
	
	static final String myName = "DrawingPart";
	
	ProgrammableUnit currentUnit=null; // The selected unit.
	Submarine currentSub=null; // If it is a sub
	Boat currentBoat=null; // If it is a boat
	boolean isBoat=true;

	boolean unitSelected=false;
	boolean unitToSelect=false;
	
	boolean endAsked=false;
	long timeSinceClick=0;
	
	String nameTypeUnit;
	
	static public boolean explosionOn=false;
	
	Label infoLabel;
	Label stateHullLabel;
	Label stateDataLabel;
	Label stateNavLabel;
	Label complementLabel;
	Label speedLabel;
	Slider sliderAlpha;
	Label depthLabel;
	Slider sliderVariation;
	Label weaponLabel;
	Label airplanesLabel;
	Label airplanesLabel2;
	Button blending2Button;
	Button blending1Button;
	Button blending3Button;
	Button attackNeutralsButton; // Not used yet!
	
	Button blending4Button;
	Button buttonHideEffect;
	Button plusButton;
	Button minusButton;
	Button zeroButton;
	
	Button firstButton;
	Button secondButton;
	Button thirdButton;
	Button fourthButton;
	Button fifthButton;
	Button recursiveButton;
	Button timedOnButton;
	Button syncButton;
	Button createButton;
	Button splitButton;
	Button satButton;
	Button clearButton;
	Button seekButton;
	Button noPartButton;
	Button smartButton; // Allow smart projectiles
	Button exploButton;
	Button collideButton;
	Button collideSplitButton;
	Button collideDieButton;
	
	Button colorsButtons[];
	
	FilledRectanglePC wpRectangle; // Show which WP you are editing.
	FilledRectanglePC missileRect; // Show if you can fire missiles (useful for submarine)
	FilledRectanglePC radarRect; // Show if you can activate the radar (submarine again)
	
	final static int ED_NONE=0;
	final static int ED_ADD_WP=1;
	final static int ED_REMOVE_WP=2;
	final static int ED_MOVE_WP=3;
	
	final static int ED_SHOOT_TORPEDO=10;
	final static int ED_SHOOT_MISSILE=11;
	
	int stateEdition=ED_NONE;
	
	Button snapshotButton;
	Button resetButton;
	
	boolean mapFree=true; // can we click freely on the map
	
	boolean moveWP=false;
//	boolean exitMoveWP=false;
	
	Button altButton;
	
	int indexWPToMove=0;
	
	Journey wpMissiles;
	Journey wpTorpedoes;
	
	SpritePC clouds[]; // decorative clouds
	double cloudSpeed; // Their speed
	double xCloudMove,yCloudMove;
	
	long iFrame = 0; // For video recording
	
	boolean gameOver = false;
	
	PlayFM myPlayFM;
	
	public DrawingPart()
	{
		; // should remain empty, push all in start() !!!
	}
	

	/**
	 * Clean the map
	 */
	protected void cleanMap()
	{
		System.out.println("Clean the tactical map");

		this.cleanUnit();
		long totalXPWon=0;
		
		if (enemiesSubs != null)
		{
			int enemiesSubsSize=enemiesSubs.size();
			
			for (int iSub = 0; iSub < enemiesSubsSize ; iSub++)
			{
				// Update !!!
				if (enemiesSubs.get(iSub).isDead())
				{
					if (enemiesSubs.get(iSub).getType() == Submarine.NUKE)
					{
						totalXPWon+=80;
					}
					else if (enemiesSubs.get(iSub).getType() == Submarine.NUKE_SSBN)
					{
						totalXPWon+=100;
					}
					else if (enemiesSubs.get(iSub).getType() == Submarine.DIESEL)
					{
						totalXPWon+=40;
					}
					else if (enemiesSubs.get(iSub).getType() == Submarine.DIESEL_AIP)
					{
						totalXPWon+=20;
					}
					// Inform the game keeper
					GameKeeper.getInstance().addCostEnemies(enemiesSubs.get(iSub).getCost());
					GameKeeper.getInstance().addMIAEnemies(enemiesSubs.get(iSub).getComplementNorm()-enemiesSubs.get(iSub).getComplement());
				}
				enemiesSubs.get(iSub).removeMe();
			}
			enemiesSubs.clear();
		}
		if (enemiesBoats != null)
		{
			int enemiesBoatsSize=enemiesBoats.size();
			
			for (int iBoat = 0; iBoat < enemiesBoatsSize ; iBoat++)
			{
				// Update !!!
				if (enemiesBoats.get(iBoat).isDead())
				{
					if (enemiesBoats.get(iBoat).getType() == Boat.CARRIER)
					{
						totalXPWon+=100;
					}
					else if (enemiesBoats.get(iBoat).getType() == Boat.CRUISER)
					{
						totalXPWon+=50;
						if (enemiesBoats.get(iBoat).getTonnage() == 700000)
						{
							totalXPWon+=1000;
						}
					}
					else if (enemiesBoats.get(iBoat).getType() == Boat.AMPHIBIOUS)
					{
						totalXPWon+=80;
					}
					else if (enemiesBoats.get(iBoat).getType() == Boat.DESTROYER)
					{
						totalXPWon+=30;
					}
					else if (enemiesBoats.get(iBoat).getType() == Boat.FRIGATE)
					{
						totalXPWon+=20;
					}
					else if (enemiesBoats.get(iBoat).getType() == Boat.CORVETTE)
					{
						totalXPWon+=5;
					}
					// Inform the game keeper
					GameKeeper.getInstance().addCostEnemies(enemiesBoats.get(iBoat).getCost());
					GameKeeper.getInstance().addMIAEnemies(enemiesBoats.get(iBoat).getComplementNorm()-enemiesBoats.get(iBoat).getComplement());
				}
				enemiesBoats.get(iBoat).resetAirplaneInFlight();
				enemiesBoats.get(iBoat).removeMe();
			}
			enemiesBoats.clear();
		}
		
		if (ourSubs != null)
		{
			int ourSubsSize=ourSubs.size();
			
			for (int iSub = 0; iSub < ourSubsSize ; iSub++)
			{
				// Update !!!
				if (ourSubs.get(iSub).isDead())
				{
					// Inform the game keeper
					GameKeeper.getInstance().addCostOur(ourSubs.get(iSub).getCost());
					GameKeeper.getInstance().addMIAOur(ourSubs.get(iSub).getComplementNorm()-ourSubs.get(iSub).getComplement());
				}
				else
				{
					ourSubs.get(iSub).addXP(totalXPWon);
					ourSubs.get(iSub).endFire();
				}
				ourSubs.get(iSub).removeMe();
			}
			ourSubs.clear();
		}
		if (ourBoats != null)
		{
			int ourBoatsSize  = ourBoats.size();
			
			for (int iBoat = 0; iBoat < ourBoatsSize ; iBoat++)
			{
				// Update !!!
				if (ourBoats.get(iBoat).isDead())
				{
					// Inform the game keeper
					GameKeeper.getInstance().addCostOur(ourBoats.get(iBoat).getCost());
					GameKeeper.getInstance().addMIAOur(ourBoats.get(iBoat).getComplementNorm()-ourBoats.get(iBoat).getComplement());
				}
				else
				{
					ourBoats.get(iBoat).addXP(totalXPWon);
					ourBoats.get(iBoat).endFire();
				}
				ourBoats.get(iBoat).resetAirplaneInFlight();
				ourBoats.get(iBoat).removeMe();
			}
			ourBoats.clear();
		}
		if (alliesSubs != null)
		{
			int alliesSubsSize  = alliesSubs.size();
			
			for (int iSub = 0; iSub < alliesSubsSize ; iSub++)
			{
				// Update !!!
				if (alliesSubs.get(iSub).isDead())
				{
					// Inform the game keeper
					GameKeeper.getInstance().addCostAllies(alliesSubs.get(iSub).getCost());
					GameKeeper.getInstance().addMIAAllies(alliesSubs.get(iSub).getComplementNorm()-alliesSubs.get(iSub).getComplement());
				}
				else
				{
					alliesSubs.get(iSub).addXP(totalXPWon);
					alliesSubs.get(iSub).endFire();
				}
				alliesSubs.get(iSub).removeMe();
			}
			alliesSubs.clear();
		}
		if (alliesBoats != null)
		{
			int alliesBoatsSize  = alliesBoats.size();
			
			for (int iBoat = 0; iBoat < alliesBoatsSize ; iBoat++)
			{
				// Update !!!
				if (alliesBoats.get(iBoat).isDead())
				{
					// Inform the game keeper
					GameKeeper.getInstance().addCostAllies(alliesBoats.get(iBoat).getCost());
					GameKeeper.getInstance().addMIAAllies(alliesBoats.get(iBoat).getComplementNorm()-alliesBoats.get(iBoat).getComplement());
				}
				else
				{
					alliesBoats.get(iBoat).addXP(totalXPWon);
					alliesBoats.get(iBoat).endFire();
				}
				alliesBoats.get(iBoat).resetAirplaneInFlight();
				alliesBoats.get(iBoat).removeMe();
			}
			alliesBoats.clear();
		}
		
		if (neutralSubs != null)
		{
			for (int iSub = 0; iSub < neutralSubs.size() ; iSub++)
			{
				// Update !!!
				if (neutralSubs.get(iSub).isDead())
				{
					// Inform the game keeper
					GameKeeper.getInstance().addCostOur(neutralSubs.get(iSub).getCost());
					GameKeeper.getInstance().addMIAOur(neutralSubs.get(iSub).getComplementNorm()-neutralSubs.get(iSub).getComplement());
				}
				neutralSubs.get(iSub).removeMe();
			}
			neutralSubs.clear();
		}
		if (neutralBoats != null)
		{
			for (int iBoat = 0; iBoat < neutralBoats.size() ; iBoat++)
			{
				// Update !!!
				if (neutralBoats.get(iBoat).isDead())
				{
					// Inform the game keeper
					GameKeeper.getInstance().addCostOur(neutralBoats.get(iBoat).getCost());
					GameKeeper.getInstance().addMIAOur(neutralBoats.get(iBoat).getComplementNorm()-neutralBoats.get(iBoat).getComplement());
				}
				neutralBoats.get(iBoat).resetAirplaneInFlight();
				neutralBoats.get(iBoat).removeMe();
			}
			neutralBoats.clear();
		}
		if (globalSensors != null)
		{
			for (int iSensor = 0; iSensor < globalSensors.size() ; iSensor++)
			{
				// Update !!!
				globalSensors.get(iSensor).removeMe();
			}
			globalSensors.clear();
		}
		if (missiles != null)
		{
			for (int iBoat = 0; iBoat < missiles.size() ; iBoat++)
			{
				// Update !!!
				missiles.get(iBoat).removeMe();
			}
			missiles.clear();
		}
		if (torpedoes != null)
		{
			for (int iBoat = 0; iBoat < torpedoes.size() ; iBoat++)
			{
				// Update !!!
				torpedoes.get(iBoat).removeMe();
			}
			torpedoes.clear();
		}
		if (alliesAirplanes != null)
		{
			for (int iBoat = 0; iBoat < alliesAirplanes.size() ; iBoat++)
			{
				// Update !!!
				alliesAirplanes.get(iBoat).removeMe();
			}
			alliesAirplanes.clear();
		}
		if (ourAirplanes != null)
		{
			for (int iBoat = 0; iBoat < ourAirplanes.size() ; iBoat++)
			{
				// Update !!!
				ourAirplanes.get(iBoat).removeMe();
			}
			ourAirplanes.clear();
		}
		if (enemiesAirplanes != null)
		{
			for (int iBoat = 0; iBoat < enemiesAirplanes.size() ; iBoat++)
			{
				// Update !!!
				enemiesAirplanes.get(iBoat).removeMe();
			}
			enemiesAirplanes.clear();
		}
		
		trees.clear();
		
		ourKD.removeMeFromMap();
		
		if (myWPDrawer != null)
		{
			myWPDrawer.removeMe();
			myWPDrawer = null;
		}
		else
		{
			System.err.println("Tactical map: myWPDrawer is already null, shouldn't be...");
		}
		RenderingManager.getInstance().washAllAndPrepare();
		RenderingManager.removeMe();
		ParticleManager.removeMe();
		
//		if (!LevelKeeper.getInstance().isBlank())
//			ourScene.remove(spriteLevel);
		
		ourScene.remove(infoLabel);
		ourScene.remove(stateHullLabel);
		ourScene.remove(stateDataLabel);
		ourScene.remove(stateNavLabel);
		ourScene.remove(complementLabel);
		ourScene.remove(speedLabel);
		ourScene.remove(sliderAlpha);
		ourScene.remove(depthLabel);
		ourScene.remove(sliderVariation);
		ourScene.remove(weaponLabel);
		ourScene.remove(blending2Button);
		ourScene.remove(blending1Button);
		ourScene.remove(blending3Button);
		ourScene.remove(attackNeutralsButton); // Not used yet!
		
		ourScene.remove(blending4Button);
		ourScene.remove(buttonHideEffect);
		ourScene.remove(plusButton);
		ourScene.remove(minusButton);
		ourScene.remove(zeroButton);
		
		ourScene.remove(firstButton);
		ourScene.remove(secondButton);
		ourScene.remove(thirdButton);
		ourScene.remove(fourthButton);
		ourScene.remove(fifthButton);
		ourScene.remove(recursiveButton);
		ourScene.remove(timedOnButton);
		ourScene.remove(syncButton);
		ourScene.remove(createButton);
		ourScene.remove(splitButton);
		ourScene.remove(satButton);
		ourScene.remove(clearButton);
		
		ourScene.remove(snapshotButton);
		ourScene.remove(resetButton);
		
		//ourScene.remove(endLevelButton);
		
		cleanMemory();
		
		System.out.println("Tactical map cleaned");
	}

	private void cleanMemory()
	{
		System.out.println("Set the tactical map object references to null (so the gc might clean them :) )");
		currentSignals= null;

		globalSensors = null;

		missiles = null;

		torpedoes = null;


		ourSubs = null;
		ourBoats = null;
		alliesSubs = null;
		alliesBoats = null;
		enemiesSubs = null;
		enemiesBoats = null;
		neutralSubs = null;
		neutralBoats = null;


		ourAirplanes = null;
		alliesAirplanes = null;
		enemiesAirplanes = null;
		neutralAirplanes = null;

		wpMissiles= null;
		wpTorpedoes= null;
		ourScene=null;


		levelImage=null;
		spriteLevel= null;


		ourKD = null;
		myWPDrawer = null;

		infoLabel = null;
		stateHullLabel = null;
		stateDataLabel = null;
		stateNavLabel = null;
		complementLabel = null;
		speedLabel = null;
		sliderAlpha = null;
		depthLabel = null;
		sliderVariation = null;
		weaponLabel = null;
		blending2Button = null;
		blending1Button = null;
		blending3Button = null;
		attackNeutralsButton = null; // Not used yet!
		
		blending4Button = null;
		buttonHideEffect = null;
		plusButton = null;
		minusButton = null;
		zeroButton = null;
		
		firstButton = null;
		secondButton = null;
		thirdButton = null;
		fourthButton = null;
		fifthButton = null;
		recursiveButton = null;
		timedOnButton = null;
		syncButton = null;
		createButton = null;
		splitButton = null;
		satButton = null;
		clearButton = null;
		seekButton = null;
		noPartButton = null;
		
		snapshotButton = null;
		resetButton = null;
		
		altButton = null;
	}
	
	public void cleanUnit()
	{
		if (isBoat)
		{
			if (currentBoat != null)
			{
				currentBoat.endFire();
				currentBoat.setView(false);
			}
		}
		if (!isBoat)
		{
			if (currentSub != null)
			{
				currentSub.endFire();
				currentSub.setView(false);
			}
		}
		missileRect.invalidate();
		//radarRect.invalidate();
	}
	
	/**
	 * Clean the map
	 */
	protected void clearMap()
	{
		System.out.println("Clear the drawing map");

		if (enemiesSubs != null)
		{
			int enemiesSubsSize=enemiesSubs.size();
			
			for (int iSub = 0; iSub < enemiesSubsSize ; iSub++)
			{
				enemiesSubs.get(iSub).removeMe();
			}
			enemiesSubs.clear();
		}
		if (enemiesBoats != null)
		{
			int enemiesBoatsSize=enemiesBoats.size();
			
			for (int iBoat = 0; iBoat < enemiesBoatsSize ; iBoat++)
			{
				enemiesBoats.get(iBoat).resetAirplaneInFlight();
				enemiesBoats.get(iBoat).removeMe();
			}
			enemiesBoats.clear();
		}
		
		if (ourSubs != null)
		{
			int ourSubsSize=ourSubs.size();
			
			for (int iSub = 0; iSub < ourSubsSize ; iSub++)
			{
				// Update !!!
				ourSubs.get(iSub).removeMe();
			}
			ourSubs.clear();
		}
		if (ourBoats != null)
		{
			int ourBoatsSize  = ourBoats.size();
			
			for (int iBoat = 0; iBoat < ourBoatsSize ; iBoat++)
			{
				if (ourBoats.get(iBoat).getIdBoat() != this.currentBoat.getIdBoat())
				{
					ourBoats.get(iBoat).resetAirplaneInFlight();
					ourBoats.get(iBoat).removeMe();
				}
				else
				{
					ourBoats.get(iBoat).resetAirplaneInFlight();
				}
			}
			ourBoats.clear();
			ourBoats.add(currentBoat);
		}
		if (alliesSubs != null)
		{
			int alliesSubsSize  = alliesSubs.size();
			
			for (int iSub = 0; iSub < alliesSubsSize ; iSub++)
			{
				// Update !!!
				alliesSubs.get(iSub).removeMe();
			}
			alliesSubs.clear();
		}
		if (alliesBoats != null)
		{
			int alliesBoatsSize  = alliesBoats.size();
			
			for (int iBoat = 0; iBoat < alliesBoatsSize ; iBoat++)
			{
				alliesBoats.get(iBoat).resetAirplaneInFlight();
				alliesBoats.get(iBoat).removeMe();
			}
			alliesBoats.clear();
		}
		
		if (neutralSubs != null)
		{
			for (int iSub = 0; iSub < neutralSubs.size() ; iSub++)
			{
				neutralSubs.get(iSub).removeMe();
			}
			neutralSubs.clear();
		}
		if (neutralBoats != null)
		{
			for (int iBoat = 0; iBoat < neutralBoats.size() ; iBoat++)
			{
				// Update !!!
				neutralBoats.get(iBoat).resetAirplaneInFlight();
				neutralBoats.get(iBoat).removeMe();
			}
			neutralBoats.clear();
		}
		
		if (alliesAirplanes != null)
		{
			for (int iBoat = 0; iBoat < alliesAirplanes.size() ; iBoat++)
			{
				// Update !!!
				alliesAirplanes.get(iBoat).removeMe();
			}
			alliesAirplanes.clear();
		}
		if (ourAirplanes != null)
		{
			for (int iBoat = 0; iBoat < ourAirplanes.size() ; iBoat++)
			{
				// Update !!!
				ourAirplanes.get(iBoat).removeMe();
			}
			ourAirplanes.clear();
		}
		if (enemiesAirplanes != null)
		{
			for (int iBoat = 0; iBoat < enemiesAirplanes.size() ; iBoat++)
			{
				// Update !!!
				enemiesAirplanes.get(iBoat).removeMe();
			}
			enemiesAirplanes.clear();
		}
		
		trees.clear();
		
		
		if (missiles != null)
		{
			for (int iBoat = 0; iBoat < missiles.size() ; iBoat++)
			{
				// Update !!!
				missiles.get(iBoat).removeMe();
			}
			missiles.clear();
		}
		if (torpedoes != null)
		{
			for (int iBoat = 0; iBoat < torpedoes.size() ; iBoat++)
			{
				// Update !!!
				torpedoes.get(iBoat).removeMe();
			}
			torpedoes.clear();
		}
		
		System.out.println("Drawing map cleared");
	}
//	//DEBUG///////////////////
//	double timeSinceStart=0;
	
	boolean nameToShow=false;
	boolean nameToHide=false;
	double timeChange=0;
	
	boolean cutRadar=false;
	
	int typeOfFigure=0;
	double rotationFigure=0;
	
	boolean timedMode; // The drawn elements "explode" after a fixed time
	boolean syncMode; // The elements explode when we release the button.
	boolean toRelease; // The button has been released, start the elements
	
	boolean satWanted; // Satellite wanted ?

	private boolean smartAllowed; // Do we allows the following smart option
	
	private boolean exploWanted; // Do the elements explode when colliding
	private boolean collideWanted; // Do the elements collide
	private boolean collideSplitWanted; // Do the elements split on collision
	private boolean collideDieWanted; // Do the elements disappear on collision
	
	boolean seekWanted; // Do the element seek others
	boolean partWanted; // Switch on and off the particles
	
	int nbSeparation = 3;

	private int currentOwnerID = 0;

	

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#doLoop()
	 */
	public String doLoop()
	{
		int mouseX=0,mouseY=0;
		double divider=10000;
//		double divider=20000; // For screenshots, see bellow 
		
		//boolean doNotHide=false;
		FileOutputStream scrFile;
		
		tmpTime=new java.util.Date().getTime();
		timeInterval=tmpTime-lastTime;
		lastTime = tmpTime;
		if (Input.isDown(Input.KEY_N))
			divider=2500;
		if (Input.isDown(Input.KEY_B))
			divider=1250;
		timeUsed= ((double )timeInterval)/divider;
		
//		Screenshots for each frames (very slow, but allow to do videos, w/o sound)
//		try
//		{
//			scrFile = new FileOutputStream("C:\\Gfx\\SW\\SW"+iFrame+".png");
//			iFrame++;
//
//			scrFile.write(pulpcore.image.PNGWriter.write(Stage.getScreenshot()).getData());
//			
//			scrFile.close();
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
		
		// Screenshots
		if (Input.isDown(Input.KEY_Z))
		{
			try
			{
				scrFile = new FileOutputStream("DW\\DW"+iFrame+".png");
				iFrame++;

				scrFile.write(pulpcore.image.PNGWriter.write(Stage.getScreenshot()).getData());

				scrFile.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
//		if (clouds != null)
//		{
//			xCloudMove = timeUsed*cloudSpeed*Math.random();
//			yCloudMove = timeUsed*cloudSpeed*Math.random();
//			int cloudsLength=clouds.length;
//			
//			for (int iClouds=0;iClouds < cloudsLength ; iClouds++)
//			{
//				
//				clouds[iClouds].moveRelative(xCloudMove, yCloudMove);
//				if ((clouds[iClouds].getX() < -200) || (clouds[iClouds].getX() > 1200) || (clouds[iClouds].getY() < -200) || (clouds[iClouds].getY() > 1000))
//				{
//					clouds[iClouds].setPos(com.tgb.subgame.RandomContainer.getInstance().myRandom.nextInt(700), com.tgb.subgame.RandomContainer.getInstance().myRandom.nextInt(800), 0);
//				}
//			}
//		}
		//System.out.println("Tactical map: do update");
/////DEBUG//////////////////////////////////		
//		timeSinceStart+=timeUsed;
//		if (timeSinceStart > 2.0)
//		{
//			timeSinceStart=0;
//			return StrategicMap.myName;
//		}
//////////////////////////////////////		
		
		if (Input.isDown(Input.KEY_UP))
		{
			upKey= true;
		}
		else
			upKey = false;
		if (Input.isDown(Input.KEY_DOWN))
		{
			downKey=true;
		}
		else
			downKey=false;
		if (Input.isDown(Input.KEY_RIGHT))
		{
			rightKey= true;
		}
		else
			rightKey = false;
		if (Input.isDown(Input.KEY_LEFT))
		{
			leftKey=true;
		}
		else
			leftKey=false;
		
		if (Input.isDown(Input.KEY_F5))
		{
			for (int iDummy=0;iDummy<10000;iDummy++)
			{
				System.out.println("Do something slow...");
			}
		}
		
		
		if (Input.isDown(Input.KEY_F1))
		{
			if ((!nameToShow) && (timeChange < 0))
			{
				nameToShow=true;
				nameToHide=false;
				timeChange=0.02;
			}
			else if (timeChange < 0)
			{
				nameToHide=true;
				nameToShow=false;
				timeChange=0.02;
			}
		}
		if (timeChange >= 0)
		{
			timeChange-=timeUsed;
		}
		
		{
			double newSpeed= sliderAlpha.value.get();
			
			mouseX = Input.getMouseX();
			mouseY = Input.getMouseY();
			
			currentBoat.setPosX(mouseX);
			currentBoat.setPosY(mouseY);
			// Reset the editing state of the WP if needed
			// Should be moved only when the state is removed to gain some FPS
			
			
			
			// Do something if needed
			
			{
				
				if (upKey)
				{
					//currentBoat.accelerate(0.4);
					sliderAlpha.value.set(currentBoat.getWantedSpeed());
				}
				if (downKey)
				{
//					currentBoat.deccelerate(0.4);
					sliderAlpha.value.set(currentBoat.getWantedSpeed());
				}
				if (rightKey)
				{
//					currentBoat.setFollowWP(false);
//					followWPButton.setSelected(false);
//					currentBoat.turn(0.01);
				}
				else if (leftKey)
				{
//					currentBoat.setFollowWP(false);
//					followWPButton.setSelected(false);
//					currentBoat.turn(-0.01);
				}
				else
				{
//					currentBoat.turn(0.00);
				}

				

				if (blending1Button.isClicked() && blending1Button.isSelected())
				{
					com.tgb.subengine.gfxentities.FlatSpritePCAdd.setBlending(0);
					DrawBackground.showBack();
					blending2Button.setSelected(false);
					blending3Button.setSelected(false);
					blending4Button.setSelected(false);
					buttonHideEffect.setSelected(false);
				}
				if (blending2Button.isClicked() && blending2Button.isSelected())
				{
					com.tgb.subengine.gfxentities.FlatSpritePCAdd.setBlending(1);
					DrawBackground.showBack();
					blending1Button.setSelected(false);
					blending3Button.setSelected(false);
					blending4Button.setSelected(false);
					buttonHideEffect.setSelected(false);
				}
				if (blending3Button.isClicked() && blending3Button.isSelected())
				{
					com.tgb.subengine.gfxentities.FlatSpritePCAdd.setBlending(2);
					DrawBackground.showBack();
					blending1Button.setSelected(false);
					blending2Button.setSelected(false);
					blending4Button.setSelected(false);
					buttonHideEffect.setSelected(false);
				}
				if (blending4Button.isClicked() && blending4Button.isSelected())
				{
					com.tgb.subengine.gfxentities.FlatSpritePCAdd.setBlending(3);
					DrawBackground.showBack();
					blending1Button.setSelected(false);
					blending2Button.setSelected(false);
					blending3Button.setSelected(false);
					buttonHideEffect.setSelected(false);
				}
				
				if (sliderAlpha.isAdjusting())
				{
					DrawBackground.setAlphaBack(sliderAlpha.value.getAsInt());;
				}
				if (sliderVariation.isAdjusting())
				{
					randomFactor=sliderVariation.value.get()/10;
				}
				
				if (buttonHideEffect.isClicked())
				{
					if (buttonHideEffect.isSelected())
					{
						DrawBackground.hideBack();
						blending1Button.setSelected(false);
						blending2Button.setSelected(false);
						blending3Button.setSelected(false);
						blending4Button.setSelected(false);
					}
					else
					{
						DrawBackground.showBack();
					}
				}
				
				if (zeroButton.isClicked())
				{
					typeOfFigure=0;
					selectFigure(typeOfFigure);
				}
				if (firstButton.isClicked())
				{
					typeOfFigure=1;
					selectFigure(typeOfFigure);
				}
				if (secondButton.isClicked())
				{
					typeOfFigure=2;
					selectFigure(typeOfFigure);
				}
				if (thirdButton.isClicked())
				{
					typeOfFigure=3;
					selectFigure(typeOfFigure);
				}
				if (fourthButton.isClicked())
				{
					typeOfFigure=4;
					selectFigure(typeOfFigure);
				}
				if (fifthButton.isClicked())
				{
					typeOfFigure=5;
					selectFigure(typeOfFigure);
				}
				
				
				if (clearButton.isClicked())
				{
					this.clearMap();
				}
				
				if (satButton.isClicked())
				{
					if (satButton.isSelected())
					{
						satWanted = true;
					}
					else
					{
						satWanted = false;
					}
					
					satButton.setSelected(satWanted);
//					currentLevel = LevelKeeper.getInstance().getNextLevelWanted();
//					
//					// TODO Auto-generated method stub
//					try
//					{
//						setLevel(-1);
//					}
//					catch (Exception e)
//					{
//						e.printStackTrace();
//					}
				}
				
				if (smartButton.isClicked())
				{
					if (smartButton.isSelected())
					{
						smartAllowed = true;
						
					}
					else
					{
						smartAllowed = false;
					}
					smartButton.setSelected(smartAllowed);
				}
				if (exploButton.isClicked())
				{
					if (exploButton.isSelected())
					{
						exploWanted = true;
						exploButton.setSelected(true);
					}
					else
					{
						exploWanted = false;
						exploButton.setSelected(false);
					}
				}
				if (collideButton.isClicked())
				{
					if (collideButton.isSelected())
					{
						collideWanted = true;
						collideButton.setSelected(true);
					}
					else
					{
						collideWanted = false;
						collideButton.setSelected(false);
					}
				}
				if (collideSplitButton.isClicked())
				{
					if (collideSplitButton.isSelected())
					{
						collideSplitWanted = true;
						collideSplitButton.setSelected(true);
					}
					else
					{
						collideSplitWanted = false;
						collideSplitButton.setSelected(false);
					}
				}
				if (collideDieButton.isClicked())
				{
					if (collideDieButton.isSelected())
					{
						collideDieWanted = true;
						collideDieButton.setSelected(true);
					}
					else
					{
						collideDieWanted = false;
						collideDieButton.setSelected(false);
					}
				}
				if (seekButton.isClicked())
				{
					if (seekButton.isSelected())
					{
						seekWanted = true;
						seekButton.setSelected(true);
					}
					else
					{
						seekWanted = false;
						seekButton.setSelected(false);
					}
				}
				if (noPartButton.isClicked())
				{
					if (noPartButton.isSelected())
					{
						partWanted = true;
						ParticleManager.getInstance().reset();
					}
					else
					{
						partWanted = false;
						ParticleManager.getInstance().setMaxNbOfParticles(0);
					}
					noPartButton.setSelected(partWanted);
				}
				
				ourScene.add(timedOnButton);
				ourScene.add(syncButton);
				
				if (timedOnButton.isClicked())
				{
					timedMode = timedOnButton.isSelected(); // The drawn elements "explode" after a fixed time
					if (timedMode)
					{
						syncButton.setSelected(false);
						syncMode = false;
					}
				}
				if (syncButton.isClicked())
				{
					syncMode = syncButton.isSelected(); // The elements explode when we release the button.
					if (syncMode)
					{
						timedOnButton.setSelected(false);
						timedMode = false;
					}
				}
				if (snapshotButton.isClicked())
				{
					DrawBackground.setBack(Stage.getScreenshot());
				}
				
				if (resetButton.isClicked())
				{
					DrawBackground.resetBack();
				}
				
				if (plusButton.isClicked())
				{
					nbSeparation++;
					if (nbSeparation > 20)
					{
						nbSeparation = 20;
					}
					infoLabel.setText(""+nbSeparation);
				}
				if (minusButton.isClicked())
				{
					nbSeparation--;
					if (nbSeparation < 1)
					{
						nbSeparation = 1;
					}
					infoLabel.setText(""+nbSeparation);
				}
				if (createButton.isClicked())
				{
					if (createButton.isSelected())
					{
						splitButton.setSelected(false);
					}
				}
				if (splitButton.isClicked())
				{
					if (splitButton.isSelected())
					{
						createButton.setSelected(false);
					}
				}
				
				for (int iColors=0;iColors < 15; iColors++)
				{
					
					if (colorsButtons[iColors].isClicked())
					{
						selectColor(iColors);
					}
				}
			}
		}
		if (Input.isMouseDown())
		{
			mouseX = Input.getMousePressX();
			mouseY = Input.getMousePressY();
			toRelease = false;
			
			if (mouseX < xMinForMenu)
			{
//				currentBoat.requestFire(false, false, true, true, ProgrammableUnit.FIRE_MISSILE, mouseX-50, mouseY,wpMissiles);
//
//				if (currentBoat.getType() == Boat.CARRIER)
//				{
//					if (currentBoat.getTypeFaction() == FUnit.BOAT_ALLIED)
//					{
//
//						if (alliesAirplanes != null)
//						{
//							int alliesAirpSize=alliesAirplanes.size();
//
//							for (int iBoat = 0; iBoat < alliesAirpSize ; iBoat++)
//							{
//								// Request missile fire from the airplanes too
//								tmpAirplane = alliesAirplanes.get(iBoat);
//								if (!tmpAirplane.isDead() && (tmpAirplane.getOwner().getIdBoat() == currentBoat.getIdBoat()))
//								{
//									tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_MISSILE, mouseX, mouseY,wpMissiles);
//								}
//							}
//						}
//
//					}
//					if (currentBoat.getTypeFaction() == FUnit.BOAT_OUR)
//					{
//
//						if (ourAirplanes != null)
//						{
//							int  ourAirplSize=ourAirplanes.size();
//
//							for (int iBoat = 0; iBoat < ourAirplSize ; iBoat++)
//							{
//								// Request missile fire from the airplanes too
//								tmpAirplane = ourAirplanes.get(iBoat);
//								if (!tmpAirplane.isDead() && (tmpAirplane.getOwner().getIdBoat() == currentBoat.getIdBoat()))
//								{
//									tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_MISSILE, mouseX, mouseY,wpMissiles);
//								}
//							}
//						}
//
//					}
//				}
//				else
//				{
//					// Command the base's planes
//					if (currentBoat.getTypeFaction() == FUnit.BOAT_ALLIED)
//					{
//
//						if (alliesAirplanes != null)
//						{
//							for (int iBoat = 0; iBoat < alliesAirplanes.size() ; iBoat++)
//							{
//								// Request missile fire from the airplanes too
//								tmpAirplane = alliesAirplanes.get(iBoat);
//								if (!tmpAirplane.isDead() && ((tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_ALLIED) || (tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_OUR)))
//								{
//									tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_MISSILE, mouseX, mouseY,wpMissiles);
//								}
//							}
//						}
//
//					}
//					if (currentBoat.getTypeFaction() == FUnit.BOAT_OUR)
//					{
//
//						if (ourAirplanes != null)
//						{
//							for (int iBoat = 0; iBoat < ourAirplanes.size() ; iBoat++)
//							{
//								// Request missile fire from the airplanes too
//								tmpAirplane = ourAirplanes.get(iBoat);
//								if (!tmpAirplane.isDead() && ((tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_OUR) || (tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_ALLIED)))
//								{
//									tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_MISSILE, mouseX, mouseY,wpMissiles);
//								}
//							}
//						}
//					}
//
//				}
				
				if (missiles.size() < MAX_NB_ON_SCREEN)
				{
					switch (typeOfFigure)
					{
						case 0:
//							myPlayFM.generateAndPlaySin();
							
							// Fire a missile !!!
							for (int iSalve=0;iSalve < 50;iSalve++)
							{
								Missile oneMissile= new Missile(this);
								
								oneMissile.setIdOwner(currentOwnerID );
								if (satWanted)
								{
									oneMissile.setSatLev1(true);
									oneMissile.setSatLev2(true);
									oneMissile.setSatLev3(true);
								}
								oneMissile.setSmart(smartAllowed);
								oneMissile.setSeeking(seekWanted);
								oneMissile.setWillExplod(exploWanted);
								oneMissile.setWillCollide(collideWanted);
								oneMissile.setWillSplitOnCollision(collideSplitWanted);
								oneMissile.setWillDieOnCollision(collideDieWanted);
								
								oneMissile.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
								oneMissile.setColoredElement(currentColor);
								oneMissile.setTargetPos(Input.getMousePressX()+8*iSalve, Input.getMousePressY()+50, 0);
								oneMissile.setCurrentSpeed(500);
								oneMissile.setOrientation(currentBoat.getOrientation()+8*iSalve);
								oneMissile.setPosX(currentBoat.getPosX());
								oneMissile.setPosY(currentBoat.getPosY());
								if (timedMode)
								{
									oneMissile.setTimed(1);
								}
								if (syncMode)
								{
									oneMissile.setWaiting();
								}
								if (createButton.isSelected())
								{
									oneMissile.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
								}
								if (splitButton.isSelected())
								{
									oneMissile.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
								}

								if (satWanted)
								{
									this.addMissile(oneMissile,4);
								}
								else
								{
									this.addMissile(oneMissile,1);
								}
							}
							break;
						case 1:
//							myPlayFM.stopPlay();
							
							// Fire a missile !!!
							for (int iSalve=0;iSalve < 3;iSalve++)
							{
								Missile oneMissile= new Missile(this);
								
								oneMissile.setIdOwner(currentOwnerID );
								if (satWanted)
								{
									oneMissile.setSatLev1(true);
									oneMissile.setSatLev2(true);
									oneMissile.setSatLev3(true);
								}
								
								oneMissile.setSmart(smartAllowed);
								oneMissile.setSeeking(seekWanted);
								oneMissile.setWillExplod(exploWanted);
								oneMissile.setWillCollide(collideWanted);
								oneMissile.setWillSplitOnCollision(collideSplitWanted);
								oneMissile.setWillDieOnCollision(collideDieWanted);
								
								oneMissile.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
								oneMissile.setColoredElement(currentColor);
								oneMissile.setTargetPos(currentBoat.getPosX()+50*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)), currentBoat.getPosY()+50*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)), 0);
								oneMissile.setCurrentSpeed(500);
								oneMissile.setOrientation(rotationFigure+iSalve*(2*Math.PI/3));
								oneMissile.setPosX(currentBoat.getPosX()+8*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)));
								oneMissile.setPosY(currentBoat.getPosY()+8*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)));
								if (timedMode)
								{
									oneMissile.setTimed(1);
								}
								if (syncMode)
								{
									oneMissile.setWaiting();
								}
								if (createButton.isSelected())
								{
									oneMissile.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.TRIANGLE);
								}
								if (splitButton.isSelected())
								{
									oneMissile.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.TRIANGLE);
								}
								
								if (satWanted)
								{
									this.addMissile(oneMissile,4);
								}
								else
								{
									this.addMissile(oneMissile,1);
								}
							}
							rotationFigure+=Math.PI/30;
							break;
						case 2:
							// Fire a missile !!!
							for (int iSalve=0;iSalve < 20;iSalve++)
							{
								Missile oneMissile= new Missile(this);
								
								oneMissile.setIdOwner(currentOwnerID );
								if (satWanted)
								{
									oneMissile.setSatLev1(true);
									oneMissile.setSatLev2(true);
									oneMissile.setSatLev3(true);
								}
								
								oneMissile.setSmart(smartAllowed);
								oneMissile.setSeeking(seekWanted);
								oneMissile.setWillExplod(exploWanted);
								oneMissile.setWillCollide(collideWanted);
								oneMissile.setWillSplitOnCollision(collideSplitWanted);
								oneMissile.setWillDieOnCollision(collideDieWanted);
								
								oneMissile.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
								oneMissile.setColoredElement(currentColor);
								oneMissile.setTargetPos(Input.getMousePressX()+8*iSalve, Input.getMousePressY()+50, 0);
								oneMissile.setCurrentSpeed(500);
								oneMissile.setOrientation(currentBoat.getOrientation()+8*iSalve);
								oneMissile.setPosX(currentBoat.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
								oneMissile.setPosY(currentBoat.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
								if (timedMode)
								{
									oneMissile.setTimed(1);
								}
								if (syncMode)
								{
									oneMissile.setWaiting();
								}
								if (createButton.isSelected())
								{
									oneMissile.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.CIRCLE_LINE);
								}
								if (splitButton.isSelected())
								{
									oneMissile.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.CIRCLE_LINE);
								}
								
								if (satWanted)
								{
									this.addMissile(oneMissile,4);
								}
								else
								{
									this.addMissile(oneMissile,1);
								}
							}
							rotationFigure+=Math.PI/10;
							break;
						case 3:
							// Fire a missile !!!
							for (int iSalve=0;iSalve < 20;iSalve++)
							{
								Missile oneMissile= new Missile(this);
								
								oneMissile.setIdOwner(currentOwnerID );
								if (satWanted)
								{
									oneMissile.setSatLev1(true);
									oneMissile.setSatLev2(true);
									oneMissile.setSatLev3(true);
								}
								
								oneMissile.setSmart(smartAllowed);
								oneMissile.setSeeking(seekWanted);
								oneMissile.setWillExplod(exploWanted);
								oneMissile.setWillCollide(collideWanted);
								oneMissile.setWillSplitOnCollision(collideSplitWanted);
								oneMissile.setWillDieOnCollision(collideDieWanted);
								
								oneMissile.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
								oneMissile.setColoredElement(currentColor);
								oneMissile.setTargetPos(currentBoat.getPosX()+50*Math.cos(rotationFigure+iSalve*(Math.PI/10)), currentBoat.getPosY()+50*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
								oneMissile.setCurrentSpeed(500);
								oneMissile.setOrientation(rotationFigure+iSalve*(Math.PI/10));
								oneMissile.setPosX(currentBoat.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
								oneMissile.setPosY(currentBoat.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
								if (timedMode)
								{
									oneMissile.setTimed(1);
								}
								if (syncMode)
								{
									oneMissile.setWaiting();
								}
								if (createButton.isSelected())
								{
									oneMissile.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.CIRCLE);
								}
								if (splitButton.isSelected())
								{
									oneMissile.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.CIRCLE);
								}
								
								if (satWanted)
								{
									this.addMissile(oneMissile,4);
								}
								else
								{
									this.addMissile(oneMissile,1);
								}
							}
							rotationFigure+=Math.PI/10;
							break;
						case 4:
							// Fire a missile !!!
							for (int iSalve=-10;iSalve < 10;iSalve++)
							{
								Missile oneMissile= new Missile(this);
								
								oneMissile.setIdOwner(currentOwnerID );
								if (satWanted)
								{
									oneMissile.setSatLev1(true);
									oneMissile.setSatLev2(true);
									oneMissile.setSatLev3(true);
								}
								
								oneMissile.setSmart(smartAllowed);
								oneMissile.setSeeking(seekWanted);
								oneMissile.setWillExplod(exploWanted);
								oneMissile.setWillCollide(collideWanted);
								oneMissile.setWillSplitOnCollision(collideSplitWanted);
								oneMissile.setWillDieOnCollision(collideDieWanted);
								
								oneMissile.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
								oneMissile.setColoredElement(currentColor);
								oneMissile.setTargetPos(currentBoat.getPosX()+50*Math.cos(rotationFigure+iSalve*(Math.PI/10)), currentBoat.getPosY()+50*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
								oneMissile.setCurrentSpeed(500);
								oneMissile.setOrientation(rotationFigure+iSalve*(Math.PI/10));
								oneMissile.setPosX(currentBoat.getPosX()+2*iSalve*Math.cos(rotationFigure));
								oneMissile.setPosY(currentBoat.getPosY()+2*iSalve*Math.sin(rotationFigure));
								if (timedMode)
								{
									oneMissile.setTimed(1);
								}
								if (syncMode)
								{
									oneMissile.setWaiting();
								}
								if (createButton.isSelected())
								{
									oneMissile.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.SPIRAL);
								}
								if (splitButton.isSelected())
								{
									oneMissile.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.2, nbSeparation, Torpedo.SPIRAL);
								}
								
								if (satWanted)
								{
									this.addMissile(oneMissile,4);
								}
								else
								{
									this.addMissile(oneMissile,1);
								}
							}
							rotationFigure+=Math.PI/10;
							break;
						default:
							// Fire a missile !!!
							for (int iSalve=0;iSalve < 50;iSalve++)
							{
								Missile oneMissile= new Missile(this);
								
								oneMissile.setIdOwner(currentOwnerID );
								if (satWanted)
								{
									oneMissile.setSatLev1(true);
									oneMissile.setSatLev2(true);
									oneMissile.setSatLev3(true);
								}
								
								oneMissile.setSmart(smartAllowed);
								oneMissile.setSeeking(seekWanted);
								oneMissile.setWillExplod(exploWanted);
								oneMissile.setWillCollide(collideWanted);
								oneMissile.setWillSplitOnCollision(collideSplitWanted);
								oneMissile.setWillDieOnCollision(collideDieWanted);
								
								oneMissile.createGfx(currentBoat.getPosX()+2*iSalve-50, currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
								oneMissile.setColoredElement(currentColor);
								oneMissile.setTargetPos(Input.getMousePressX(), Input.getMousePressY()+50, 0);
								oneMissile.setCurrentSpeed(500);
								oneMissile.setOrientation(currentBoat.getOrientation()+8*iSalve);
								oneMissile.setPosX(currentBoat.getPosX()+2*iSalve-50);
								oneMissile.setPosY(currentBoat.getPosY());
								if (timedMode)
								{
									oneMissile.setTimed(1);
								}
								if (syncMode)
								{
									oneMissile.setWaiting();
								}
								if (createButton.isSelected())
								{
									oneMissile.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
								}
								if (splitButton.isSelected())
								{
									oneMissile.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
								}
								
								if (satWanted)
								{
									this.addMissile(oneMissile,4);
								}
								else
								{
									this.addMissile(oneMissile,1);
								}
							}
					}
				}
				
			}
			
			
			
//			if (torpedoes.size() < 8000)
//			{
//				// Fire a missile !!!
//				for (int iSalve=0;iSalve < 2;iSalve++)
//				{
//					Missile oneMissile= new Missile(this);
//					oneMissile.createGfx(ourSubs.get(0).getPosX()+2*iSalve, ourSubs.get(0).getPosY(), 0, ourSubs.get(0).getOrientation(), 500);
//					oneMissile.setTargetPos(Input.getMousePressX(), Input.getMousePressY(), 0);
//					oneMissile.setCurrentSpeed(500);
//					oneMissile.setOrientation(ourSubs.get(0).getOrientation());
//					oneMissile.setPosX(ourSubs.get(0).getPosX()+2*iSalve);
//					oneMissile.setPosY(ourSubs.get(0).getPosY());
//
//					missiles.add(oneMissile);
//				}
//				// Fire a torpedo !!!
//				for (int iSalve=0;iSalve < 4;iSalve++)
//				{
//					Torpedo oneTorpedo= new Torpedo(this);
//					oneTorpedo.createGfx(ourSubs.get(0).getPosX()+2*iSalve, ourSubs.get(0).getPosY(), 0, ourSubs.get(0).getOrientation(), 500);
//					oneTorpedo.setTargetPos(Input.getMousePressX(), Input.getMousePressY(), 0);
//					oneTorpedo.setCurrentSpeed(50);
//					oneTorpedo.setOrientation(ourSubs.get(0).getOrientation());
//					oneTorpedo.setPosX(ourSubs.get(0).getPosX()+2*iSalve);
//					oneTorpedo.setPosY(ourSubs.get(0).getPosY());
//					torpedoes.add(oneTorpedo);
//				}
//			}
		}
		if (Input.isDown(Input.KEY_MOUSE_BUTTON_3) & (mouseX < xMinForMenu))
		{
			toRelease=false;
			
			if (torpedoes.size() < MAX_NB_ON_SCREEN)
			{
				switch (typeOfFigure)
				{
					case 0:
						// Fire a missile !!!
						for (int iSalve=0;iSalve < 50;iSalve++)
						{
							Torpedo oneTorpedo= new Torpedo(this);
							if (satWanted)
							{
								oneTorpedo.setSatLev1(true);
								oneTorpedo.setSatLev2(true);
								oneTorpedo.setSatLev3(true);
							}
							
							oneTorpedo.setSmart(smartAllowed);
							oneTorpedo.setSeeking(seekWanted);
							oneTorpedo.setWillExplod(exploWanted);
							oneTorpedo.setWillCollide(collideWanted);
							oneTorpedo.setWillSplitOnCollision(collideSplitWanted);
							oneTorpedo.setWillDieOnCollision(collideDieWanted);
							
							oneTorpedo.createGfx(currentBoat.getPosX()+4*iSalve-100, currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
							oneTorpedo.setColoredElement(currentColor);
							oneTorpedo.setTargetPos(Input.getMousePressX()+8*iSalve, Input.getMousePressY()+50, 0);
							oneTorpedo.setCurrentSpeed(150);
							oneTorpedo.setOrientation(currentBoat.getOrientation()+8*iSalve);
							oneTorpedo.setPosX(currentBoat.getPosX());
							oneTorpedo.setPosY(currentBoat.getPosY());
							if (timedMode)
							{
								oneTorpedo.setTimed(1);
							}
							if (syncMode)
							{
								oneTorpedo.setWaiting();
							}
							if (createButton.isSelected())
							{
								oneTorpedo.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
							}
							if (splitButton.isSelected())
							{
								oneTorpedo.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
							}
							
							if (satWanted)
							{
								this.addTorpedo(oneTorpedo,4);
							}
							else
							{
								this.addTorpedo(oneTorpedo,1);
							}
						}
						break;
					case 1:
						// Fire a missile !!!
						for (int iSalve=0;iSalve < 3;iSalve++)
						{
							Torpedo oneTorpedo= new Torpedo(this);
							if (satWanted)
							{
								oneTorpedo.setSatLev1(true);
								oneTorpedo.setSatLev2(true);
								oneTorpedo.setSatLev3(true);
							}
							
							oneTorpedo.setSmart(smartAllowed);
							oneTorpedo.setSeeking(seekWanted);
							oneTorpedo.setWillExplod(exploWanted);
							oneTorpedo.setWillCollide(collideWanted);
							oneTorpedo.setWillSplitOnCollision(collideSplitWanted);
							oneTorpedo.setWillDieOnCollision(collideDieWanted);
							
							oneTorpedo.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
							oneTorpedo.setTargetPos(currentBoat.getPosX()+50*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)), currentBoat.getPosY()+50*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)), 0);
							oneTorpedo.setColoredElement(currentColor);
							oneTorpedo.setCurrentSpeed(150);
							oneTorpedo.setOrientation(rotationFigure+iSalve*(2*Math.PI/3));
							oneTorpedo.setPosX(currentBoat.getPosX()+8*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)));
							oneTorpedo.setPosY(currentBoat.getPosY()+8*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)));
							//oneTorpedo.setColoredElement(iSalve);
							if (timedMode)
							{
								oneTorpedo.setTimed(1);
							}
							if (syncMode)
							{
								oneTorpedo.setWaiting();
							}
							if (createButton.isSelected())
							{
								oneTorpedo.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.TRIANGLE);
							}
							if (splitButton.isSelected())
							{
								oneTorpedo.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.TRIANGLE);
							}
							
							if (satWanted)
							{
								this.addTorpedo(oneTorpedo,4);
							}
							else
							{
								this.addTorpedo(oneTorpedo,1);
							}
						}
						rotationFigure+=Math.PI/30;
						break;
					case 2:
						// Fire a missile !!!
						for (int iSalve=0;iSalve < 20;iSalve++)
						{
							Torpedo oneTorpedo= new Torpedo(this);
							if (satWanted)
							{
								oneTorpedo.setSatLev1(true);
								oneTorpedo.setSatLev2(true);
								oneTorpedo.setSatLev3(true);
							}
							
							oneTorpedo.setSmart(smartAllowed);
							oneTorpedo.setSeeking(seekWanted);
							oneTorpedo.setWillExplod(exploWanted);
							oneTorpedo.setWillCollide(collideWanted);
							oneTorpedo.setWillSplitOnCollision(collideSplitWanted);
							oneTorpedo.setWillDieOnCollision(collideDieWanted);
							
							oneTorpedo.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
							oneTorpedo.setColoredElement(currentColor);
							oneTorpedo.setTargetPos(currentBoat.getPosX()+20, currentBoat.getPosY(), 0);
							oneTorpedo.setCurrentSpeed(150);
							oneTorpedo.setOrientation(currentBoat.getOrientation()+8*iSalve);
							oneTorpedo.setPosX(currentBoat.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
							oneTorpedo.setPosY(currentBoat.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
							//oneTorpedo.setColoredElement(iSalve++);
							if (timedMode)
							{
								oneTorpedo.setTimed(1);
							}
							if (syncMode)
							{
								oneTorpedo.setWaiting();
							}
							if (createButton.isSelected())
							{
								oneTorpedo.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.CIRCLE_LINE);
							}
							if (splitButton.isSelected())
							{
								oneTorpedo.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.CIRCLE_LINE);
							}
							
							if (satWanted)
							{
								this.addTorpedo(oneTorpedo,4);
							}
							else
							{
								this.addTorpedo(oneTorpedo,1);
							}
						}
						rotationFigure+=Math.PI/10;
						break;
					case 3:
						// Fire a missile !!!
						for (int iSalve=0;iSalve < 20;iSalve++)
						{
							Torpedo oneTorpedo= new Torpedo(this);
							if (satWanted)
							{
								oneTorpedo.setSatLev1(true);
								oneTorpedo.setSatLev2(true);
								oneTorpedo.setSatLev3(true);
							}
							
							oneTorpedo.setSmart(smartAllowed);
							oneTorpedo.setSeeking(seekWanted);
							oneTorpedo.setWillExplod(exploWanted);
							oneTorpedo.setWillCollide(collideWanted);
							oneTorpedo.setWillSplitOnCollision(collideSplitWanted);
							oneTorpedo.setWillDieOnCollision(collideDieWanted);
							
							oneTorpedo.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
							oneTorpedo.setTargetPos(currentBoat.getPosX()+20*Math.cos(rotationFigure+iSalve*(Math.PI/10)), currentBoat.getPosY()+20*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
							oneTorpedo.setColoredElement(currentColor);
							oneTorpedo.setCurrentSpeed(150);
							oneTorpedo.setOrientation(rotationFigure+iSalve*(Math.PI/10));
							oneTorpedo.setPosX(currentBoat.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
							oneTorpedo.setPosY(currentBoat.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
							if (timedMode)
							{
								oneTorpedo.setTimed(1);
							}
							if (syncMode)
							{
								oneTorpedo.setWaiting();
							}
							if (createButton.isSelected())
							{
								oneTorpedo.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.CIRCLE);
							}
							if (splitButton.isSelected())
							{
								oneTorpedo.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.CIRCLE);
							}
							
							if (satWanted)
							{
								this.addTorpedo(oneTorpedo,4);
							}
							else
							{
								this.addTorpedo(oneTorpedo,1);
							}
						}
						rotationFigure+=Math.PI/10;
						break;
					case 4:
						// Fire a missile !!!
						for (int iSalve=-10;iSalve < 10;iSalve++)
						{
							Torpedo oneTorpedo= new Torpedo(this);
							if (satWanted)
							{
								oneTorpedo.setSatLev1(true);
								oneTorpedo.setSatLev2(true);
								oneTorpedo.setSatLev3(true);
							}
							
							oneTorpedo.setSmart(smartAllowed);
							oneTorpedo.setSeeking(seekWanted);
							oneTorpedo.setWillExplod(exploWanted);
							oneTorpedo.setWillCollide(collideWanted);
							oneTorpedo.setWillSplitOnCollision(collideSplitWanted);
							oneTorpedo.setWillDieOnCollision(collideDieWanted);
							
							oneTorpedo.createGfx(currentBoat.getPosX(), currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
							oneTorpedo.setTargetPos(currentBoat.getPosX()+20*Math.cos(rotationFigure+iSalve*(Math.PI/10)), currentBoat.getPosY()+20*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
							oneTorpedo.setColoredElement(currentColor);
							oneTorpedo.setCurrentSpeed(150);
							oneTorpedo.setOrientation(rotationFigure+iSalve*(Math.PI/10));
							oneTorpedo.setPosX(currentBoat.getPosX()+2*iSalve*Math.cos(rotationFigure));
							oneTorpedo.setPosY(currentBoat.getPosY()+2*iSalve*Math.sin(rotationFigure));
							if (timedMode)
							{
								oneTorpedo.setTimed(1);
							}
							if (syncMode)
							{
								oneTorpedo.setWaiting();
							}
							if (createButton.isSelected())
							{
								oneTorpedo.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.SPIRAL);
							}
							if (splitButton.isSelected())
							{
								oneTorpedo.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.SPIRAL);
							}
							if (satWanted)
							{
								this.addTorpedo(oneTorpedo,4);
							}
							else
							{
								this.addTorpedo(oneTorpedo,1);
							}
						}
						rotationFigure+=Math.PI/10;
						break;
					default:
						// Fire a torpedo !!!
						for (int iSalve=0;iSalve < 50;iSalve++)
						{
							Torpedo oneTorpedo= new Torpedo(this);
							if (satWanted)
							{
								oneTorpedo.setSatLev1(true);
								oneTorpedo.setSatLev2(true);
								oneTorpedo.setSatLev3(true);
							}
							
							oneTorpedo.setSmart(smartAllowed);
							oneTorpedo.setSeeking(seekWanted);
							oneTorpedo.setWillExplod(exploWanted);
							oneTorpedo.setWillCollide(collideWanted);
							oneTorpedo.setWillSplitOnCollision(collideSplitWanted);
							oneTorpedo.setWillDieOnCollision(collideDieWanted);
							
							oneTorpedo.createGfx(currentBoat.getPosX()+4*iSalve-100, currentBoat.getPosY(), 0, currentBoat.getOrientation()+8*iSalve, 500);
							oneTorpedo.setColoredElement(currentColor);
							oneTorpedo.setTargetPos(Input.getMousePressX(), Input.getMousePressY()+50, 0);
							oneTorpedo.setCurrentSpeed(150);
							oneTorpedo.setOrientation(currentBoat.getOrientation()+8*iSalve);
							oneTorpedo.setPosX(currentBoat.getPosX()+4*iSalve-100);
							oneTorpedo.setPosY(currentBoat.getPosY());
							if (timedMode)
							{
								oneTorpedo.setTimed(1);
							}
							if (syncMode)
							{
								oneTorpedo.setWaiting();
							}
							if (createButton.isSelected())
							{
								oneTorpedo.setCreating(true, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
							}
							if (splitButton.isSelected())
							{
								oneTorpedo.setCreating(false, recursiveButton.isSelected(), altButton.isSelected(), 0.5, nbSeparation, Torpedo.LINE);
							}
							
							if (satWanted)
							{
								this.addTorpedo(oneTorpedo,4);
							}
							else
							{
								this.addTorpedo(oneTorpedo,1);
							}
						}
				}
			}
			
			
//			currentBoat.requestFire(false, false, true, true, ProgrammableUnit.FIRE_TORPEDO, mouseX, mouseY+50,wpTorpedoes);

//			if (currentBoat.getType() == Boat.CARRIER)
//			{
//				if (currentBoat.getTypeFaction() == FUnit.BOAT_ALLIED)
//				{
//
//					if (alliesAirplanes != null)
//					{
//						for (int iBoat = 0; iBoat < alliesAirplanes.size() ; iBoat++)
//						{
//							// Request missile fire from the airplanes too
//							tmpAirplane = alliesAirplanes.get(iBoat);
//							if (!tmpAirplane.isDead() && (tmpAirplane.getOwner().getIdBoat() == currentBoat.getIdBoat()))
//							{
//								tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_TORPEDO, mouseX, mouseY+50,wpTorpedoes);
//							}
//						}
//					}
//
//				}
//				if (currentBoat.getTypeFaction() == FUnit.BOAT_OUR)
//				{
//
//					if (ourAirplanes != null)
//					{
//						for (int iBoat = 0; iBoat < ourAirplanes.size() ; iBoat++)
//						{
//							// Request missile fire from the airplanes too
//							tmpAirplane = ourAirplanes.get(iBoat);
//							if (!tmpAirplane.isDead() && (tmpAirplane.getOwner().getIdBoat() == currentBoat.getIdBoat()))
//							{
//								tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_TORPEDO, mouseX, mouseY+50,wpTorpedoes);
//							}
//						}
//					}
//
//				}
//			}
//			else
//			{
//				// Command the base's planes
//				if (currentBoat.getTypeFaction() == FUnit.BOAT_ALLIED)
//				{
//
//					if (alliesAirplanes != null)
//					{
//						for (int iBoat = 0; iBoat < alliesAirplanes.size() ; iBoat++)
//						{
//							// Request missile fire from the airplanes too
//							tmpAirplane = alliesAirplanes.get(iBoat);
//							if (!tmpAirplane.isDead() && ((tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_ALLIED) || (tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_OUR)))
//							{
//								tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_TORPEDO, mouseX, mouseY+50,wpTorpedoes);
//							}
//						}
//					}
//
//				}
//				if (currentBoat.getTypeFaction() == FUnit.BOAT_OUR)
//				{
//
//					if (ourAirplanes != null)
//					{
//						for (int iBoat = 0; iBoat < ourAirplanes.size() ; iBoat++)
//						{
//							// Request missile fire from the airplanes too
//							tmpAirplane = ourAirplanes.get(iBoat);
//							if (!tmpAirplane.isDead() && ((tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_OUR) || (tmpAirplane.getOwner().getTypeFaction() == FUnit.BASE_ALLIED)))
//							{
//								tmpAirplane.requestFire(false, false, true, true, ProgrammableUnit.FIRE_TORPEDO, mouseX, mouseY+50,wpTorpedoes);
//							}
//						}
//					}
//
//				}
//			}
		}
		if (Input.isMouseReleased())
		{
			toRelease=true;
			currentOwnerID++;
		}
		if (Input.isReleased(Input.KEY_MOUSE_BUTTON_3))
		{
			toRelease=true;
			currentOwnerID++;
		}
				
		// Go through the units and update them!
//		if (ourSubs != null)
//		{
//			int ourSubsSize = ourSubs.size();
//			
//			for (int iSub = 0; iSub < ourSubsSize ; iSub++)
//			{
//				// Update !!!
//				ourSubs.get(iSub).doUpdate(timeUsed);
//			}
//		}
		if (ourBoats != null)
		{
			int ourBoatsSize = ourBoats.size();
			
			for (int iBoat = 0; iBoat < ourBoatsSize ; iBoat++)
			{
				//System.out.println("Update our boat "+iBoat);
				// Update !!!
				ourBoats.get(iBoat).doUpdate(timeUsed);
			}
		}
//		if (alliesSubs != null)
//		{
//			int alliesSubsSize = alliesSubs.size();
//			
//			for (int iSub = 0; iSub < alliesSubsSize ; iSub++)
//			{
//				// Update !!!
//				alliesSubs.get(iSub).doUpdate(timeUsed);
//			}
//		}
//		if (alliesBoats != null)
//		{
//			int alliesBoatsSize = alliesBoats.size();
//			
//			for (int iBoat = 0; iBoat < alliesBoatsSize ; iBoat++)
//			{
//				// Update !!!
//				alliesBoats.get(iBoat).doUpdate(timeUsed);
//			}
//		}
//		if (enemiesSubs != null)
//		{
//			int enemiesSubsSize = enemiesSubs.size();
//			
//			for (int iSub = 0; iSub < enemiesSubsSize ; iSub++)
//			{
//				// Update !!!
//				enemiesSubs.get(iSub).doUpdate(timeUsed);
//			}
//		}
//		if (enemiesBoats != null)
//		{
//			int enemiesBoatsSize = enemiesBoats.size();
//			
//			for (int iBoat = 0; iBoat < enemiesBoatsSize ; iBoat++)
//			{
//				// Update !!!
//				enemiesBoats.get(iBoat).doUpdate(timeUsed);
//			}
//		}
//		if (neutralSubs != null)
//		{
//			int neutralSubsSize = neutralSubs.size();
//			
//			for (int iSub = 0; iSub < neutralSubsSize ; iSub++)
//			{
//				// Update !!!
//				neutralSubs.get(iSub).doUpdate(timeUsed);
//			}
//		}
//		if (neutralBoats != null)
//		{
//			int neutralBoatsSize = neutralBoats.size();
//			
//			for (int iBoat = 0; iBoat < neutralBoatsSize ; iBoat++)
//			{
//				// Update !!!
//				neutralBoats.get(iBoat).doUpdate(timeUsed);
//			}
//		}
//		if (globalSensors != null)
//		{
//			int globalSensorsSize = globalSensors.size();
//			
//			for (int iSensors = 0; iSensors < globalSensorsSize ; iSensors++)
//			{
//				// Update !!!
//				globalSensors.get(iSensors).doUpdate(timeUsed);
//			}
//		}
//		if (alliesAirplanes != null)
//		{
//			int alliesAirplanesSize = alliesAirplanes.size();
//			
//			for (int iBoat = 0; iBoat < alliesAirplanesSize ; iBoat++)
//			{
//				// Update !!!
//				tmpAirplane = alliesAirplanes.get(iBoat);
//				tmpAirplane.doUpdate(timeUsed);
//				if (tmpAirplane.toRemove())
//				{
//					//System.out.println("Remove missile "+missiles.size());
//					tmpAirplane.removeMe();
//					alliesAirplanes.remove(iBoat);
//					alliesAirplanesSize--;
//					iBoat--;
//				}
//			}
//		}
//		if (ourAirplanes != null)
//		{
//			int ourAirplanesSize = ourAirplanes.size();
//			
//			for (int iBoat = 0; iBoat < ourAirplanesSize ; iBoat++)
//			{
//				// Update !!!
//				tmpAirplane = ourAirplanes.get(iBoat);
//				tmpAirplane.doUpdate(timeUsed);
//				if (tmpAirplane.toRemove())
//				{
//					//System.out.println("Remove missile "+missiles.size());
//					tmpAirplane.removeMe();
//					ourAirplanes.remove(iBoat);
//					ourAirplanesSize--;
//					iBoat--;
//				}
//			}
//		}
//		if (enemiesAirplanes != null)
//		{
//			int enemiesAirplanesSize = enemiesAirplanes.size();
//			
//			for (int iBoat = 0; iBoat < enemiesAirplanesSize ; iBoat++)
//			{
//				// Update !!!
//				tmpAirplane = enemiesAirplanes.get(iBoat);
//				tmpAirplane.doUpdate(timeUsed);
//				if (tmpAirplane.toRemove())
//				{
//					//System.out.println("Remove missile "+missiles.size());
//					tmpAirplane.removeMe();
//					enemiesAirplanes.remove(iBoat);
//					enemiesAirplanesSize--;
//					iBoat--;
//				}
//			}
//		}
//		if (ourBases != null)
//		{
//			int ourBasesSize = ourBases.size();
//			
//			for (int iBoat = 0; iBoat < ourBasesSize ; iBoat++)
//			{
//				// Update !!!
//				ourBases.get(iBoat).doUpdate(timeUsed);
//				
//				if (nameToShow)
//				{
//					ourBases.get(iBoat).showLabel();
//				}
//				if (nameToHide)
//				{
//					ourBases.get(iBoat).hideLabel();
//				}
//			}
//		}
//		if (alliesBases != null)
//		{
//			int alliesBasesSize = alliesBases.size();
//			
//			for (int iBoat = 0; iBoat < alliesBasesSize ; iBoat++)
//			{
//				// Update !!!
//				alliesBases.get(iBoat).doUpdate(timeUsed);
//				
//				if (nameToShow)
//				{
//					alliesBases.get(iBoat).showLabel();
//				}
//				if (nameToHide)
//				{
//					alliesBases.get(iBoat).hideLabel();
//				}
//			}
//		}
//		if (enemiesBases != null)
//		{
//			int enemiesBasesSize = enemiesBases.size();
//			
//			for (int iBoat = 0; iBoat < enemiesBasesSize ; iBoat++)
//			{
//				// Update !!!
//				enemiesBases.get(iBoat).doUpdate(timeUsed);
//				
//				if (nameToShow)
//				{
//					enemiesBases.get(iBoat).showLabel();
//				}
//				if (nameToHide)
//				{
//					enemiesBases.get(iBoat).hideLabel();
//				}
//			}
//		}
//		if (neutralBases != null)
//		{
//			int neutralBasesSize = neutralBases.size();
//			
//			for (int iBoat = 0; iBoat < neutralBasesSize ; iBoat++)
//			{
//				// Update !!!
//				neutralBases.get(iBoat).doUpdate(timeUsed);
//			}
//		}
		if (missiles != null)
		{
			int missilesSize = missiles.size();
			
			for (int iMissiles = 0; iMissiles < missilesSize ; iMissiles++)
			{
				tmpMissile = missiles.get(iMissiles);
				
				tmpMissile.setSmart(smartAllowed);
				tmpMissile.setSeeking(seekWanted);
				tmpMissile.setWillExplod(exploWanted);
				tmpMissile.setWillCollide(collideWanted);
				tmpMissile.setWillSplitOnCollision(collideSplitWanted);
				tmpMissile.setWillDieOnCollision(collideDieWanted);
				
				tmpMissile.doUpdate(timeUsed);
				if (tmpMissile.toRemove())
				{
					//System.out.println("Remove missile "+missiles.size());
					tmpMissile.removeMe();
					missiles.remove(iMissiles);
					missilesSize--;
					iMissiles--;
				}
				if (toRelease)
				{
					tmpMissile.startMe();
				}
			}
		}
		if (torpedoes != null)
		{
			int torpedoesSize = torpedoes.size();
			
			for (int iTorpedoes = 0; iTorpedoes < torpedoesSize ; iTorpedoes++)
			{
				tmpTorpedo = torpedoes.get(iTorpedoes);
				
				tmpTorpedo.setSmart(smartAllowed);
				tmpTorpedo.setSeeking(seekWanted);
				tmpTorpedo.setWillExplod(exploWanted);
				tmpTorpedo.setWillCollide(collideWanted);
				tmpTorpedo.setWillSplitOnCollision(collideSplitWanted);
				tmpTorpedo.setWillDieOnCollision(collideDieWanted);
				
				tmpTorpedo.doUpdate(timeUsed);
				if (tmpTorpedo.toRemove())
				{
					//System.out.println("Remove torpedo "+torpedoes.size());
					tmpTorpedo.removeMe();
					torpedoes.remove(iTorpedoes);
					torpedoesSize--;
					iTorpedoes--;
				}
				if (toRelease)
				{
					tmpTorpedo.startMe();
				}
			}
		}
		if (trees != null)
		{
			int treesSize = trees.size();
		
			for (int iTree = 0; iTree < treesSize ; iTree++)
			{
				// Update !!!
				tmpTree = trees.get(iTree);
				tmpTree.doUpdate(timeUsed);
//				if (tmpTree.toRemove())
//				{
//					//System.out.println("Remove missile "+missiles.size());
//					tmpTree.removeMe();
//					trees.remove(iBoat);
//					treesSize--;
//					iBoat--;
//				}
			}
		}
//		if (Input.isDown(Input.KEY_E))
//			return DrawMap.myName;
		return null;
	}

	private void selectFigure(int nbSelected)
	{
		// Switch off everything
		zeroButton.setSelected(false);
		firstButton.setSelected(false);
		secondButton.setSelected(false);
		thirdButton.setSelected(false);
		fourthButton.setSelected(false);
		fifthButton.setSelected(false);
		
		// Then select the good one
		switch (nbSelected)
		{
			case 0: zeroButton.setSelected(true);
			break;
			
			case 1: firstButton.setSelected(true);
			break;
			
			case 2: secondButton.setSelected(true);
			break;
			
			case 3: thirdButton.setSelected(true);
			break;
			
			case 4: fourthButton.setSelected(true);
			break;
			
			case 5: fifthButton.setSelected(true);
			break;			
		}
	}


	/**
	 * Do something when a sub die.
	 * Check here if we print the win/lose message
	 * Cannot be in the parent class as it is also used in the title page,
	 * might appear here so.
	 * @see com.tgb.subgame.LevelMap#subDead(int)
	 */
	public void subDead(int typeFaction)
	{
		super.subDead(typeFaction);
	}

	public void boatDead(int typeFaction)
	{
		super.boatDead(typeFaction);
	}

	public void baseDead(int typeFaction)
	{
		super.baseDead(typeFaction);
	}
	
	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#getStateName()
	 */
	public String getStateName()
	{
		// TODO Auto-generated method stub
		return myName;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#keyPressed(java.awt.event.KeyEvent)
	 */
	public String keyPressed(KeyEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#keyReleased(java.awt.event.KeyEvent)
	 */
	public String keyReleased(KeyEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#keyTyped(java.awt.event.KeyEvent)
	 */
	public String keyTyped(KeyEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#mouseClicked(java.awt.event.MouseEvent)
	 */
	public String mouseClicked(MouseEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#mouseEntered(java.awt.event.MouseEvent)
	 */
	public String mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#mouseExited(java.awt.event.MouseEvent)
	 */
	public String mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#mousePosition(int, int)
	 */
	public String mousePosition(int x, int y)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#mousePressed(java.awt.event.MouseEvent)
	 */
	public String mousePressed(MouseEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#mouseReleased(java.awt.event.MouseEvent)
	 */
	public String mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#quit()
	 */
	public void quit()
	{
		// TODO Auto-generated method stub
		try
		{
			System.out.println("Quit the Tactical map");
			cleanMap();
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#sleep()
	 */
	public void sleep()
	{
		// TODO Auto-generated method stub

	}
	    
	int currentColor=0;

	/**
	 * Select a color and set it, reset the button
	 * @param iColor
	 */
	private void selectColor(int iColor)
	{
		colorsButtons[currentColor].setSelected(false);
		currentColor = iColor;
		colorsButtons[currentColor].setSelected(true);
	}
	/* (non-Javadoc)
	 * @see com.tgb.subengine.gamesystems.IGamePart#start()
	 */
	public void start()
	{
		System.out.println("Create tactical map");
		
		// Init all
		nbOurSubs=0;
		nbOurBoats=0;
		nbAlliesSubs=0;
		nbAlliesBoats=0;
		nbTotalGood=0;
		
		nbEnemiesSubs=0;
		nbEnemiesBoats=0;
		nbEnemiesTotal=0;
		
		nbOurAirplanes=0;
		nbEnemiesAirplanes=0;
		nbAlliesAirplanes=0;
		
		nbSignal = 0;
		
		currentUnit=null; // The selected unit.
		currentSub=null; // If it is a sub
		currentBoat=null; // If it is a boat
		isBoat=true;

		unitSelected=false;
		unitToSelect=false;
		
		endAsked=false;
		timeSinceClick=0;
		
		mapFree=true; // can we click freely on the map
		
		moveWP=false;
		
		stateEdition=ED_NONE;
		
		currentSignals= new ArrayList<Signal>();
		
		globalSensors = new ArrayList<Sensor>();
		
		missiles = new ArrayList<Missile>();
		
		torpedoes = new ArrayList<Torpedo>();

		
		ourSubs = new ArrayList<Submarine>();
		ourBoats = new ArrayList<Boat>();
		alliesSubs = new ArrayList<Submarine>();
		alliesBoats = new ArrayList<Boat>();
		enemiesSubs = new ArrayList<Submarine>();
		enemiesBoats = new ArrayList<Boat>();
		neutralSubs = new ArrayList<Submarine>();
		neutralBoats = new ArrayList<Boat>();
		
		
		ourAirplanes = new ArrayList<Airplane>();
		alliesAirplanes = new ArrayList<Airplane>();
		enemiesAirplanes = new ArrayList<Airplane>();
		neutralAirplanes = new ArrayList<Airplane>();
		
		trees = new ArrayList<Trees>();
		
		wpMissiles= new Journey();
		wpTorpedoes= new Journey();
		// Load the map
		DrawBackground.drawBack();
		
		// Load the sprites
		gfxSprites.callMeFirst();
		ourScene=(Scene2D )Stage.getScene();
		
		if (!LevelKeeper.getInstance().isBlank())
		{
			levelImage=LevelKeeper.getInstance().getCurrentLevel();
			spriteLevel= new FlatSpritePC(levelImage);
			spriteLevel.setPos(Stage.getWidth()/2,  Stage.getHeight()/2, 0);
			spriteLevel.setAlpha(10);
			
			idSpriteLevel = RenderingManager.getInstance().addDrawableEntity(spriteLevel,1);
		}
		
		xMinForMenu = Stage.getWidth() - 200;
				
        
		speedLabel= new Label("Effect strength", xMinForMenu + 100, 160);
		speedLabel.anchorX.set(0.5f);
		ourScene.add(speedLabel);
		sliderAlpha = new Slider("SpeedBarre.png", "SpeedCursor.png", xMinForMenu + 100, 180);
		sliderAlpha.anchorX.set(0.5f);
		sliderAlpha.setRange(0, 255);
		sliderAlpha.value.set(10);
		ourScene.add(sliderAlpha);
		
		depthLabel= new Label("Variation", xMinForMenu + 100, 780);
		depthLabel.anchorX.set(0.5f);
		ourScene.add(depthLabel);
		
		

		sliderVariation = new Slider("SpeedBarre.png", "SpeedCursor.png", xMinForMenu + 100, 800);
		sliderVariation.anchorX.set(0.5f);
		sliderVariation.setRange(0, 100);
		sliderVariation.value.set(0);
		ourScene.add(sliderVariation);
		
		blending1Button=Button.createLabeledToggleButton("Effect 1", xMinForMenu, 240);
		blending2Button=Button.createLabeledToggleButton("Effect 2", xMinForMenu, 270);
		blending3Button=Button.createLabeledToggleButton("Effect 3", xMinForMenu, 300);
		ourScene.add(blending1Button);
		ourScene.add(blending2Button);
		ourScene.add(blending3Button);
		
		// WP buttons.
		blending4Button=Button.createLabeledToggleButton("Effect 4", xMinForMenu, 330);
		ourScene.add(blending4Button);
		buttonHideEffect=Button.createLabeledToggleButton("No Effect", xMinForMenu + 80, 330);
		ourScene.add(buttonHideEffect);
		
		plusButton=Button.createLabeledButton("+", xMinForMenu, 720);
		ourScene.add(plusButton);
		minusButton=Button.createLabeledButton("-", xMinForMenu + 80, 720);
		ourScene.add(minusButton);
		
		infoLabel = new Label(""+nbSeparation, xMinForMenu+50, 760);
		infoLabel.setAnchor(Sprite.CENTER);
		ourScene.add(infoLabel);
		
		zeroButton=Button.createLabeledToggleButton("Line", xMinForMenu + 120, 360);
		ourScene.add(zeroButton);
		
		// Fire buttons
		firstButton=Button.createLabeledToggleButton("Triangle", xMinForMenu, 390);
		secondButton=Button.createLabeledToggleButton("Circle 1", xMinForMenu+ 120, 390);
		thirdButton=Button.createLabeledToggleButton("Circle 2", xMinForMenu, 420);
		fourthButton=Button.createLabeledToggleButton("Spiral", xMinForMenu + 120, 420);
		fifthButton=Button.createLabeledToggleButton("Line 2", xMinForMenu, 450);
		recursiveButton=Button.createLabeledToggleButton("Recursive", xMinForMenu + 120, 450);
		
		recursiveButton.setSelected(false);
		
		timedOnButton=Button.createLabeledToggleButton("Timed", xMinForMenu, 480);
		syncButton=Button.createLabeledToggleButton("Sync", xMinForMenu + 120, 480);
		createButton=Button.createLabeledToggleButton("Create", xMinForMenu, 510);
		splitButton=Button.createLabeledToggleButton("Split", xMinForMenu + 120, 510);
		satButton=Button.createLabeledToggleButton("Sat", xMinForMenu, 540);
		clearButton=Button.createLabeledButton("Clear", xMinForMenu + 120, 540);
		
		satButton.setSelected(false);
		clearButton.setSelected(false);
		
		ourScene.add(firstButton);
		ourScene.add(secondButton);
		ourScene.add(thirdButton);
		ourScene.add(fourthButton);
		ourScene.add(fifthButton);
		ourScene.add(recursiveButton);
		ourScene.add(timedOnButton);
		ourScene.add(syncButton);
		ourScene.add(createButton);
		ourScene.add(splitButton);
		ourScene.add(satButton);
		ourScene.add(clearButton);
		
		smartButton=Button.createLabeledToggleButton("Smarts", xMinForMenu, 570);
		exploButton=Button.createLabeledToggleButton("Explosion", xMinForMenu, 600);
		collideButton=Button.createLabeledToggleButton("Collide", xMinForMenu + 120, 600);
		collideSplitButton=Button.createLabeledToggleButton("Coll. Split", xMinForMenu, 630);
		collideDieButton=Button.createLabeledToggleButton("Coll. Death", xMinForMenu + 120, 630);
		
		smartButton.setSelected(false);
		exploButton.setSelected(false);
		collideButton.setSelected(false);
		collideSplitButton.setSelected(false);
		collideDieButton.setSelected(false);
		
		ourScene.add(smartButton);
		ourScene.add(exploButton);
		ourScene.add(collideButton);
		ourScene.add(collideSplitButton);
		ourScene.add(collideDieButton);
		
		seekButton=Button.createLabeledToggleButton("Seeking", xMinForMenu, 660);
		noPartButton=Button.createLabeledToggleButton("Particles", xMinForMenu + 120, 660);
		
		seekButton.setSelected(false);
		noPartButton.setSelected(true);
		
		ourScene.add(seekButton);
		ourScene.add(noPartButton);
		
		snapshotButton = Button.createLabeledButton("Snapshot", xMinForMenu, 690);
		resetButton = Button.createLabeledButton("Reset", xMinForMenu + 120, 690);
		
		ourScene.add(snapshotButton);
		ourScene.add(resetButton);
		
//		wpRectangle = new FilledRectanglePC(); // Show which WP you are editing.
//		wpRectangle.setSizeRect(200, 100);
//		wpRectangle.setPos(700, 200, 0);
//		wpRectangle.setOurColor(0xFFFF0000);
		//RenderingManager.getInstance().addDrawableEntity(wpRectangle,39);
		
		missileRect = new FilledRectanglePC(); // Show if you can fire missiles (useful for submarine)
		missileRect.is3D = false;
		missileRect.setSizeRect(200, 900); // 870
		missileRect.setPos(xMinForMenu, 0, 0); // 130
		missileRect.setOurColor(0x99CCAAAA);
		missileRect.validate();
		RenderingManager.getInstance().addDrawableEntity(missileRect,39);
		
//		radarRect = new FilledRectanglePC(); // Show if you can activate the radar (submarine again)
//		radarRect.setSizeRect(120, 32);
//		radarRect.setPos(818, 578, 0);
//		radarRect.setOurColor(0xFFFF0000);
//		radarRect.invalidate();
//		RenderingManager.getInstance().addDrawableEntity(radarRect,39);
		
		
		altButton= Button.createLabeledToggleButton("Alternative", xMinForMenu, 850);
		
		ourScene.add(altButton);
		
		colorsButtons = new Button[15];
		
		gfxSprites.callMeFirst();
			
		// Build the colors' buttons
		{
			int xBut=0;
			int yBut=0;
			
			CoreImage buttons[];
			
			buttons= new CoreImage[6];

			for (int iColors=0;iColors < 15; iColors++)
			{
				System.out.println("Draw button "+iColors);
				
				buttons[0] = new CoreImage(40,40,false);
				buttons[1] = new CoreImage(40,40,false);
				buttons[2] = new CoreImage(40,40,false);
				buttons[3] = new CoreImage(40,40,false);
				buttons[4] = new CoreImage(40,40,false);
				buttons[5] = new CoreImage(40,40,false);
				
				buttons[0].createGraphics().drawImage(CoreImage.load("ButtonOn.png"));
				buttons[1].createGraphics().drawImage(CoreImage.load("ButtonOnPres.png"));
				buttons[2].createGraphics().drawImage(CoreImage.load("ButtonOnPass.png"));
				buttons[3].createGraphics().drawImage(CoreImage.load("ButtonOff.png"));
				buttons[4].createGraphics().drawImage(CoreImage.load("ButtonOffPres.png"));
				buttons[5].createGraphics().drawImage(CoreImage.load("ButtonOffPass.png"));
				
//				buttons[0].setOpaque(false);
//				buttons[1].setOpaque(false);
//				buttons[2].setOpaque(false);
//				buttons[3].setOpaque(false);
//				buttons[4].setOpaque(false);
//				buttons[5].setOpaque(false);
				
				buttons[0].createGraphics().drawImage(gfxSprites.getImageFl(iColors).scale(0.8),buttons[0].getWidth()/2-gfxSprites.getImageFl(iColors).scale(0.8).getWidth()/2,buttons[0].getHeight()/2-gfxSprites.getImageFl(iColors).scale(0.8).getHeight()/2);
				buttons[1].createGraphics().drawImage(gfxSprites.getImageFl(iColors).scale(0.8),buttons[0].getWidth()/2-gfxSprites.getImageFl(iColors).scale(0.8).getWidth()/2,buttons[0].getHeight()/2-gfxSprites.getImageFl(iColors).scale(0.8).getHeight()/2);
				buttons[2].createGraphics().drawImage(gfxSprites.getImageFl(iColors).scale(0.8),buttons[0].getWidth()/2-gfxSprites.getImageFl(iColors).scale(0.8).getWidth()/2,buttons[0].getHeight()/2-gfxSprites.getImageFl(iColors).scale(0.8).getHeight()/2);
				buttons[3].createGraphics().drawImage(gfxSprites.getImageFl(iColors).scale(0.8),buttons[0].getWidth()/2-gfxSprites.getImageFl(iColors).scale(0.8).getWidth()/2,buttons[0].getHeight()/2-gfxSprites.getImageFl(iColors).scale(0.8).getHeight()/2);
				buttons[4].createGraphics().drawImage(gfxSprites.getImageFl(iColors).scale(0.8),buttons[0].getWidth()/2-gfxSprites.getImageFl(iColors).scale(0.8).getWidth()/2,buttons[0].getHeight()/2-gfxSprites.getImageFl(iColors).scale(0.8).getHeight()/2);
				buttons[5].createGraphics().drawImage(gfxSprites.getImageFl(iColors).scale(0.8),buttons[0].getWidth()/2-gfxSprites.getImageFl(iColors).scale(0.8).getWidth()/2,buttons[0].getHeight()/2-gfxSprites.getImageFl(iColors).scale(0.8).getHeight()/2);

				colorsButtons[iColors] = new Button(buttons, xMinForMenu+xBut*40,10+yBut*40, true);
				colorsButtons[iColors].setSelected(false);
				
				ourScene.add(colorsButtons[iColors]);
				
				xBut++;
				if (xBut > 4)
				{
					xBut = 0;
					yBut++;
				}
				
				selectColor(0);
			}

//			buttons[14][0] = CoreImage.load("ButtonOn.png");
//			buttons[14][1] = CoreImage.load("ButtonOnPres.png");
//			buttons[14][2] = CoreImage.load("ButtonOnPass.png");
//			buttons[14][3] = CoreImage.load("ButtonOff.png");
//			buttons[14][4] = CoreImage.load("ButtonOffPres.png");
//			buttons[14][5] = CoreImage.load("ButtonOffPass.png");
//			
//			buttons[14][0].createGraphics().drawImage(gfxSprites.getImageAirplane().scale(1),buttons[0][0].getWidth()/2-gfxSprites.getImageAirplane().scale(1).getWidth()/2,buttons[0][0].getHeight()/2-gfxSprites.getImageAirplane().scale(1).getHeight()/2);
//			buttons[14][1].createGraphics().drawImage(gfxSprites.getImageAirplane().scale(1),buttons[0][0].getWidth()/2-gfxSprites.getImageAirplane().scale(1).getWidth()/2,buttons[0][0].getHeight()/2-gfxSprites.getImageAirplane().scale(1).getHeight()/2);
//			buttons[14][2].createGraphics().drawImage(gfxSprites.getImageAirplane().scale(1),buttons[0][0].getWidth()/2-gfxSprites.getImageAirplane().scale(1).getWidth()/2,buttons[0][0].getHeight()/2-gfxSprites.getImageAirplane().scale(1).getHeight()/2);
//			buttons[14][3].createGraphics().drawImage(gfxSprites.getImageAirplane().scale(1),buttons[0][0].getWidth()/2-gfxSprites.getImageAirplane().scale(1).getWidth()/2,buttons[0][0].getHeight()/2-gfxSprites.getImageAirplane().scale(1).getHeight()/2);
//			buttons[14][4].createGraphics().drawImage(gfxSprites.getImageAirplane().scale(1),buttons[0][0].getWidth()/2-gfxSprites.getImageAirplane().scale(1).getWidth()/2,buttons[0][0].getHeight()/2-gfxSprites.getImageAirplane().scale(1).getHeight()/2);
//			buttons[14][5].createGraphics().drawImage(gfxSprites.getImageAirplane().scale(1),buttons[0][0].getWidth()/2-gfxSprites.getImageAirplane().scale(1).getWidth()/2,buttons[0][0].getHeight()/2-gfxSprites.getImageAirplane().scale(1).getHeight()/2);
//
//			colorsButtons[14] = new Button(buttons[14], 700+xBut*40,10+yBut*40, true);
//			
//			ourScene.add(colorsButtons[14]);
			
		}
		//endLevelButton.enabled.set(false);
		
		ourKD = new KnownDatas(this);
		
		// Load the good level
		
		currentLevel = LevelKeeper.getInstance().getNextLevelWanted();
		
		// TODO Auto-generated method stub
		try
		{
			setLevel(currentLevel);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		lastTime=new java.util.Date().getTime();
		myWPDrawer = new WPDrawer();
		
//		clouds = null;
//		
//		if (Math.random() < 0.2)
//		{
//			clouds = new SpritePC[5];
//			CoreImage cloudImage=CoreImage.load("Nuage2.png");
//			for (int iClouds=0;iClouds < clouds.length ; iClouds++)
//			{
//				clouds[iClouds]=new SpritePC(cloudImage);
//				clouds[iClouds].setSize(Math.random()*3+1);
//				clouds[iClouds].setRotation(Math.random()*4);
//				clouds[iClouds].setPos(com.tgb.subgame.RandomContainer.getInstance().myRandom.nextInt(700), com.tgb.subgame.RandomContainer.getInstance().myRandom.nextInt(800), 0);
//				RenderingManager.getInstance().addDrawableEntity(clouds[iClouds],37);
//				
//				cloudSpeed = Math.random()*50;
//			}
//		}
//		else if (Math.random() < 0.3)
//		{
//			clouds = new SpritePC[5];
//			CoreImage cloudImage=CoreImage.load("Nuage3.png");
//			for (int iClouds=0;iClouds < clouds.length ; iClouds++)
//			{
//				clouds[iClouds]=new SpritePC(cloudImage);
//				clouds[iClouds].setSize(Math.random()*3+1);
//				clouds[iClouds].setRotation(Math.random()*4);
//				clouds[iClouds].setPos(com.tgb.subgame.RandomContainer.getInstance().myRandom.nextInt(700), com.tgb.subgame.RandomContainer.getInstance().myRandom.nextInt(800), 0);
//				RenderingManager.getInstance().addDrawableEntity(clouds[iClouds],37);
//				
//				cloudSpeed = Math.random()*120;
//			}
//		}
			
		gameOver = false;
		
		addCarrierOur();
		this.unitSelected=true;
		
		timedMode = false; // The drawn elements "explode" after a fixed time
		syncMode = false; // The elements explode when we release the button.
		
		// Create the FM player
		myPlayFM = new PlayFM();
	}
	
	private void addCarrierOur()
	{

		Boat oneCarrier = new Boat();
		oneCarrier.setType(Boat.CARRIER);
		oneCarrier.setTypeFaction(FUnit.BOAT_OUR);
		oneCarrier.setComplement(500);
		oneCarrier.setComplementNorm(500);
		GameKeeper.getInstance().addComplementOur(5000);
		oneCarrier.setTonnage(70000);
		oneCarrier.setCost(400000000L);

		oneCarrier.setNbAwacs(0);
		oneCarrier.setNbFighters(0);
		oneCarrier.setNbTankers(0);
		
		//GameKeeper.getInstance().addCostOur(400000000L);
		oneCarrier.setMaxSpeed(0);
		oneCarrier.setStandardSpeed(0);
		oneCarrier.setName("Controlled boat");
		oneCarrier.setFireAtWill(true);
		oneCarrier.setNbMissiles(1000);
		oneCarrier.setNbTorpedoes(40);
		oneCarrier.setResistance(2000);
		// And add the absolute coordinates.
		oneCarrier.setPosMap(500, 500);

		oneCarrier.setFollowTargetMap(true);
		oneCarrier.setAttackSREnabled(false);

		oneCarrier.setTheMap(this);
		
		oneCarrier.createGfx(500, 500, 0, 0, 0);
		
		this.addBoat(oneCarrier);
		this.currentBoat = oneCarrier;
		this.isBoat = true;
	}


	public void doMore()
	{
		if (Missile.MAX_NB_ON_SCREEN <= missiles.size())
		{
			Missile.MAX_NB_ON_SCREEN+=100;
		}
		if (Torpedo.MAX_NB_ON_SCREEN <= torpedoes.size())
		{
			Torpedo.MAX_NB_ON_SCREEN+=100;
		}
	}


	public void doLess()
	{
		if (Missile.MAX_NB_ON_SCREEN > missiles.size())
		{
			Missile.MAX_NB_ON_SCREEN =  missiles.size() - 100;
			Torpedo.MAX_NB_ON_SCREEN = torpedoes.size() - 100;
		}
		else if (Missile.MAX_NB_ON_SCREEN > MINIMAL_NUMBER_OF_ELEMENTS)
		{
			Missile.MAX_NB_ON_SCREEN -= 100;
			Torpedo.MAX_NB_ON_SCREEN -= 100;
		}
	}
}
