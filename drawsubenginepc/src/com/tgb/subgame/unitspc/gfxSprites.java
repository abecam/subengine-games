/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subgame.unitspc;

import pulpcore.image.CoreImage;

/**
 * Static facility class to create the sprite gfx. Avoid the overload and some
 * serious slowdown during load! Might not be the smartest way though.
 * @author Alain Becam
 *
 */
public class gfxSprites
{
	public static CoreImage imageFl2;
	public static CoreImage imageFl6;
	public static CoreImage imageFl7;
	public static CoreImage imageFl8;
	public static CoreImage imageFl9;
	public static CoreImage imageFl10;
	public static CoreImage imageFl11;
	public static CoreImage imageFl5;
	public static CoreImage imageBoatDestroyerEne;
	public static CoreImage imageBoatFrigateEne;
	public static CoreImage imageBoatCorvetteEne;
	public static CoreImage imageFl13;
	public static CoreImage imageFl12;
	public static CoreImage imageFl14;
	public static CoreImage imageFl15;
	public static CoreImage imageSmoke;
	public static CoreImage imageSub3D;
	public static CoreImage imageSub3DEne;
	public static CoreImage imageSub;
	public static CoreImage imageFlash;
	public static CoreImage imageFlash2;
	public static CoreImage imageSBoat;
	public static CoreImage imageDarkSmoke;
	public static CoreImage selImage;
	public static CoreImage imageAirplane;
	public static CoreImage imageAwaks;
	public static CoreImage imageAirplaneDark;
	public static CoreImage imageAwaksDark;
	public static CoreImage imageAirplaneLight;
	public static CoreImage imageAwaksLight;
	public static CoreImage imageBase1;
	public static CoreImage imageBase1Dest;
	public static CoreImage imageBase2;
	public static CoreImage imageBase2Dest;
	
	// SM gfxs
	public static CoreImage imageBaseSmallOur;
	public static CoreImage imageBaseSmallEne;
	public static CoreImage imageBaseSmallAllies;
	public static CoreImage imageBaseSmallDest;
	
	public static CoreImage imageCarrierSmallOur;
	public static CoreImage imageCarrierSmallEne;
	public static CoreImage imageCarrierSmallAllies;
	
	public static CoreImage imageExcl;
	public static CoreImage imageInt;
	public static CoreImage imageConfront;
	
	public static CoreImage imageWP;
	
	public static CoreImage imageArrow;
	// Does not appear if dead
	
	public static boolean created=false;
	
	public static void callMeFirst()
	{
		if (!created)
			createGfx();
		created = true;
	}
	
	public static void removeMe()
	{
		imageFl2 = null;
		imageFl6 =  null;
		imageFl7 = null;
		imageFl8 = null;
		imageFl9 = null;
		imageFl10 = null;
		imageFl11 = null;
		imageFl5 = null;
		imageBoatDestroyerEne = null;
		imageBoatFrigateEne = null;
		imageBoatCorvetteEne = null;
		imageFl13 = null;
		imageFl12 = null;
		imageFl14 = null;
		imageFl15 = null;
		imageSmoke = null;
		imageSub3D = null;
		imageSub3DEne = null;
		imageSub = null;
		imageFlash = null;
		imageFlash2 = null;
		imageSBoat = null;
		imageDarkSmoke = null;
		selImage = null;
		imageAirplane = null;
		imageAwaks = null;
		imageAirplaneDark = null;
		imageAwaksDark = null;
		imageAirplaneLight = null;
		imageAwaksLight = null;
		imageBase1 = null;
		imageBase1Dest = null;
		imageBase2 = null;
		imageBase2Dest = null;

		imageBaseSmallOur = null;
		imageBaseSmallEne = null;
		imageBaseSmallAllies = null;
		imageBaseSmallDest = null;
		
		imageCarrierSmallOur = null;
		imageCarrierSmallEne = null;
		imageCarrierSmallAllies = null;
		
		imageExcl = null;
		imageInt = null;
		imageConfront = null;
		
		imageWP = null;
		
		imageArrow = null;
	}
	
	public static void createGfx()
	{	
		imageFl2 = CoreImage.load("Flash2.png");
		imageFl5 = CoreImage.load("Flash5.png");
		imageFl6 = CoreImage.load("Flash6.png");
		imageFl7 = CoreImage.load("Flash7.png");
		imageFl8 = CoreImage.load("Flash8.png");
		imageFl9 = CoreImage.load("BallGold1.png");
		imageFl10 = CoreImage.load("Flash10.png");
		imageFl11 = CoreImage.load("Flash11.png");
		imageFl12 = CoreImage.load("Flash12.png");
		imageFl13 = CoreImage.load("Confro.png");
		imageFl14 = CoreImage.load("Flash14.png");
		imageFl15 = CoreImage.load("Flash15.png");
		
		imageBoatDestroyerEne = CoreImage.load("Flash2.png");
		
		imageBoatFrigateEne = CoreImage.load("Flash2.png");
		
		imageBoatCorvetteEne = CoreImage.load("Flash2.png");
		
		imageSmoke = CoreImage.load("Flash2.png");
		imageSub3D = CoreImage.load("Flash2.png");
		imageSub = CoreImage.load("Flash2.png");
		imageSub3DEne = CoreImage.load("Flash3.png");
		imageFlash = CoreImage.load("Flash.png");
		imageFlash2 = CoreImage.load("Flash2.png");
		imageSBoat = CoreImage.load("BallSilver1.png");
		imageDarkSmoke = CoreImage.load("Flash15.png");
		selImage = CoreImage.load("SelNor.png");
		imageAirplane = CoreImage.load("Flash.png");
		imageAwaks = CoreImage.load("Flash2.png");
		imageAirplaneDark = CoreImage.load("Flash8.png");
		imageAwaksDark = CoreImage.load("Flash2.png");
		imageAirplaneLight = CoreImage.load("Flash7.png");
		imageAwaksLight = CoreImage.load("Flash11.png");
		imageBase1 = CoreImage.load("Flash2.png");
		imageBase1Dest = CoreImage.load("Flash3.png");
		imageBase2 = CoreImage.load("Flash2.png");
		imageBase2Dest = CoreImage.load("Flash4.png");

		imageBaseSmallOur = CoreImage.load("BaseSmall.png");
		imageBaseSmallEne = CoreImage.load("BaseSmallEne.png");
		imageBaseSmallAllies = CoreImage.load("BaseSmallAllies.png");
		imageBaseSmallDest = CoreImage.load("BaseSmallDest.png");
		
		imageCarrierSmallOur = CoreImage.load("CarrierSmallOur.png");
		imageCarrierSmallEne = CoreImage.load("CarrierSmallEne.png");
		imageCarrierSmallAllies = CoreImage.load("CarrierSmallAllies.png");
		
		imageExcl = CoreImage.load("BallNorm1.png");
		imageInt = CoreImage.load("Int16.png");
		imageConfront = CoreImage.load("Confro.png");
		
		imageWP = CoreImage.load("WP.png");
		
		imageArrow = CoreImage.load("Fleche.png");
	}
	
	public static CoreImage getImageBoat() {
		return imageFl2;
	}
	public static CoreImage getImageBoatNorm()
	{
		return imageFl7;
	}
	public static CoreImage getImageBoatBattleship()
	{
		return imageFl9;
	}
	public static CoreImage getImageBoatDestroyer()
	{
		return imageFl10;
	}
	public static CoreImage getImageBoatFrigate()
	{
		return imageFl11;
	}
	public static CoreImage getImageBoatCorvette()
	{
		return imageFl5;
	}
	public static CoreImage getImageBoatMed()
	{
		return imageFl13;
	}
	public static CoreImage getImageBoatSmall()
	{
		return imageFl12;
	}
	public static CoreImage getImageBoatSmallEne()
	{
		return imageFl14;
	}
	public static CoreImage getImageBoatSmallAllies()
	{
		return imageFl15;
	}
	public static boolean isCreated()
	{
		return created;
	}
	public static CoreImage getImageSmoke() {
		return imageSmoke;
	}
	public static CoreImage getImageSub3D() {
		return imageSub3D;
	}
	public static CoreImage getImageSub() {
		return imageSub;
	}
	public static CoreImage getImageFlash() {
		return imageFlash;
	}
	public static CoreImage getImageFlash2() {
		return imageFlash2;
	}
	public static CoreImage getImageSBoat() {
		return imageSBoat;
	}
	public static CoreImage getImageDarkSmoke() {
		return imageDarkSmoke;
	}
	public static CoreImage getSelImage() {
		return selImage;
	}
	public static CoreImage getImageAirplane() {
		return imageAirplane;
	}
	public static CoreImage getImageAwaks() {
		return imageAwaks;
	}
	public static CoreImage getImageAirplaneDark() {
		return imageAirplaneDark;
	}
	public static CoreImage getImageAwaksDark() {
		return imageAwaksDark;
	}
	public static CoreImage getImageAirplaneLight() {
		return imageAirplaneLight;
	}
	public static CoreImage getImageAwaksLight() {
		return imageAwaksLight;
	}
	public static CoreImage getImageBase1() {
		return imageBase1;
	}
	public static CoreImage getImageBase1Dest() {
		return imageBase1Dest;
	}
	public static CoreImage getImageBase2() {
		return imageBase2;
	}
	public static CoreImage getImageBase2Dest() {
		return imageBase2Dest;
	}
	public static CoreImage getImageBaseSmallOur() {
		return imageBaseSmallOur;
	}
	public static CoreImage getImageBaseSmallEne() {
		return imageBaseSmallEne;
	}
	public static CoreImage getImageBaseSmallAllies() {
		return imageBaseSmallAllies;
	}
	public static CoreImage getImageBaseSmallDest() {
		return imageBaseSmallDest;
	}
	public static CoreImage getImageCarrierSmallOur() {
		return imageCarrierSmallOur;
	}
	public static CoreImage getImageCarrierSmallEne() {
		return imageCarrierSmallEne;
	}
	public static CoreImage getImageCarrierSmallAllies() {
		return imageCarrierSmallAllies;
	}
	
	public static CoreImage getImageBoatCarrierEne()
	{
		return imageFl6;
	}

	public static CoreImage getImageBoatCruiserEne()
	{
		return imageFl8;
	}

	public static CoreImage getImageSub3DEne()
	{
		return imageSub3DEne;
	}

	public static CoreImage getImageExcl()
	{
		return imageExcl;
	}
	public static CoreImage getImageInt()
	{
		return imageInt;
	}
	public static CoreImage getImageConfront()
	{
		return imageConfront;
	}
	
	public static CoreImage getImageBoatDestroyerEne()
	{
		return imageBoatDestroyerEne;
	}

	public static CoreImage getImageBoatFrigateEne()
	{
		return imageBoatFrigateEne;
	}

	public static CoreImage getImageBoatCorvetteEne()
	{
		return imageBoatCorvetteEne;
	}

	public static CoreImage getImageWP()
	{
		return imageWP;
	}

	public static CoreImage getImageArrow()
	{
		return imageArrow;
	}
	/////////////////////////////////

	/**
	 * @return the image iFl
	 */
	public static CoreImage getImageFl(int iFl)
	{
		switch (iFl)
		{
			case 0:
				return imageFl2;
			case 1:
				return imageSub3DEne;
			case 2:
				return imageSBoat;
			case 3:
				return imageFl5;
			case 4:
				return imageFl6;
			case 5:
				return imageFl7;
			case 6:
				return imageFl8;
			case 7:
				return imageFl9;
			case 8:
				return imageFl10;
			case 9:
				return imageFl11;
			case 10:
				return imageFl12;
			case 11:
				return imageFl13;
			case 12:
				return imageFl14;
			case 13:
				return imageFl15;
			default:
				return imageAirplane;	
		}
		
	}
	
	/**
	 * @return the imageFl2
	 */
	public static CoreImage getImageFl2()
	{
		return imageFl2;
	}

	
	/**
	 * @return the imageFl5
	 */
	public static CoreImage getImageFl5()
	{
		return imageFl5;
	}

	/**
	 * @return the imageFl6
	 */
	public static CoreImage getImageFl6()
	{
		return imageFl6;
	}

	/**
	 * @return the imageFl7
	 */
	public static CoreImage getImageFl7()
	{
		return imageFl7;
	}

	/**
	 * @return the imageFl8
	 */
	public static CoreImage getImageFl8()
	{
		return imageFl8;
	}

	/**
	 * @return the imageFl9
	 */
	public static CoreImage getImageFl9()
	{
		return imageFl9;
	}

	/**
	 * @return the imageFl10
	 */
	public static CoreImage getImageFl10()
	{
		return imageFl10;
	}

	/**
	 * @return the imageFl11
	 */
	public static CoreImage getImageFl11()
	{
		return imageFl11;
	}

	/**
	 * @return the imageFl13
	 */
	public static CoreImage getImageFl13()
	{
		return imageFl13;
	}

	/**
	 * @return the imageFl12
	 */
	public static CoreImage getImageFl12()
	{
		return imageFl12;
	}

	/**
	 * @return the imageFl14
	 */
	public static CoreImage getImageFl14()
	{
		return imageFl14;
	}

	/**
	 * @return the imageFl15
	 */
	public static CoreImage getImageFl15()
	{
		return imageFl15;
	}
	
	///////////////////////////////////////////
	
	
}
