/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subgame.unitspc;

import java.util.ArrayList;

import pulpcore.image.CoreImage;

import com.tgb.subengine.RenderingManager;
import com.tgb.subengine.Utils;
import com.tgb.subengine.gfxentities.SimpleLinePC;
import com.tgb.subengine.gfxentities.SpritePCAdd;
import com.tgb.subengine.gfxentities.FlatSpritePC;
import com.tgb.subengine.gfxentities.SimpleLineFromCenterPC;
import com.tgb.subengine.particlessystem.Emitter;
import com.tgb.subengine.particlessystem.Particles;
import com.tgb.subengine.particlessystem.ParticlePC;
import com.tgb.subgame.LevelMap;
import com.tgb.subgame.unitspc.sensors.ISignalCreator;
import com.tgb.subgame.unitspc.sensors.Signal;

/**
 * A Torpedo. Due to the speed, the sensors are faked in it (not "actual ones" like in
 * other units)
 * @author Alain Becam
 *
 */
public class Torpedo extends ProgrammableUnit implements ISignalCreator
{

	public static int MAX_NB_ON_SCREEN = 80000;
	int idMissile;
	int idOwner; // Who shot it.
	
	double alt=0;
	double orientation;
	
	double xShift,yShift;
	
	double currentSpeed=0; // In knots
	double speedX,speedY,speedZ=0;
	
	double wantedSpeedX,wantedSpeedY,wantedSpeedZ=0;
	double tmpSpeedX,tmpSpeedY,tmpSpeedZ,tmpSpeedN=0;
	
	double maxSpeed=55; // In knots
	double standardSpeed=45;
	double accelerationMax=20; // In knots/sec
	
	double energyLeft=20;
	
	int noiseLevel; // 0-> None, 100-> enough :)
	int visibilityLevel;
	
	int damage=200; // Damage potential
	double damageRadiusSq=1000; // Square of damage radius !!!
	
	double stopTargetingRadiusSq=100;
	
	int damageType; // To be defined
	
	
	FlatSpritePC torpedoBody;
	Emitter bubblesEmitter; // For enemies's torpedoes, both the torpedo and the bubbles will appear if found (by gravitation or passive/active sonar)
	Particles bubbles;
	long idBubbles;
	
	SpritePCAdd explosion,explosion2;
	
	long idBody;
	long idExplosion,idExplosion2; // Generally, only one flash will be used in the same time!
	
	FlatSpritePC satBody; // ID for the satellite
	SimpleLineFromCenterPC lineSat;
	long idSatBody, idLineSat;
	
	FlatSpritePC satBody2; // ID for the satellite
	SimpleLineFromCenterPC lineSat2;
	long idSatBody2, idLineSat2;
	
	FlatSpritePC satBody3; // ID for the satellite
	SimpleLineFromCenterPC lineSat3;
	long idSatBody3, idLineSat3;
	

	public static final int ASM=0x50;
	public static final int ASW=0x51;
	public static final int ALM=0x52;
	public static final int AA=0x53;
	public static final int CRUISE=0x54;
	
	boolean seek=true; // Does this missile seek the enemy (or is WP-Target Coord based only, explose on arrival)
	boolean seeking=false; // Are we actually seeking?
	boolean wpbased=false;
	boolean target=true;
	boolean knowAllies=false; // Ignore allies' units?
	boolean knowEnemies=false; // Or enemies
	
	double distToTarget;
	
	double detectionStrength; // For seeking missile, strength of its sensor
	double power=10; // Power of its sensor.
	boolean active=false; // Might be passive or active. Typical passive sensor will be for Radar-seeking missiles !
	
	Signal ourSignal;
	boolean existingSignal=false; // Is a signal currently emitted?
	double angle= Math.PI/4; // Angle of detection
	
	SimpleLinePC detectLine1,detectLine2,detectLine3; // To draw the detection triangle
	SimpleLinePC targetLine;
	
	static boolean debugView = false; // Do we see the seeking lines
	
	long idDL1,idDL2,idDL3,idTL;
	/* 
	 * From the tactical map
	 */
	ArrayList<Submarine> ourSubs;
	ArrayList<Boat> ourBoats;
	ArrayList<Submarine> alliesSubs;
	ArrayList<Boat> alliesBoats;
	ArrayList<Submarine> enemiesSubs;
	ArrayList<Boat> enemiesBoats;
	ArrayList<Submarine> neutralSubs;
	ArrayList<Boat> neutralBoats;
	
	
	boolean exploding=false; // Tell the updater (TacticalMap) to explode us!
	boolean exploded=false; // Damage done?
	boolean toRemove=false; // Will be removed from TacticalMap
	int explodeTime=0;
	
	boolean timed; // Is this timed (will start after some time)
	boolean notWaiting; // Is this waiting to start or not
	double localTime; // The time when we were started
	double thresholdTime; // threshold after which we start
	
	boolean seeding; // Will create other torpedoes (or missile ?)
	boolean stayAlive; // Continue to live after creating other torpedoes
	boolean recursive; // Will the created units seed ?
	boolean alternative; // Will we create missiles ? 
	double timeSeparation;
	int nbSeparation;
	int typeCreation;
	double timeStarted;
	double rotationFigure;
	
	public final static int CIRCLE=0;
	public final static int TRIANGLE=1;
	public final static int CIRCLE_LINE=2; // Circle but go straight right
	public final static int LINE=3; // Horizontal line
	public final static int SPIRAL=4; // Spiralling line
	
	int myColor=-1;
	
	boolean satLev1; // Satellite 1
	boolean satLev2; // Satellite 1
	boolean satLev3; // Satellite 1
	
	String Name;
	
	LevelMap theMap;
	/**
	 * @param theMap
	 */
	public Torpedo(LevelMap theMap)
	{
		super(theMap);
		this.theMap=theMap;
		
		ourSignal = new Signal();
		// Default
		type = ASM;
		
		detectLine1 = new SimpleLinePC();
		detectLine2 = new SimpleLinePC();
		detectLine3 = new SimpleLinePC();
		targetLine = new SimpleLinePC();
		detectLine1.invalidate();
		detectLine2.invalidate();
		detectLine3.invalidate();
		if (!debugView)
			targetLine.invalidate();
		
		idDL1 = RenderingManager.getInstance().addDrawableEntity(detectLine1,39);
		idDL2 = RenderingManager.getInstance().addDrawableEntity(detectLine2,39);
		idDL3 = RenderingManager.getInstance().addDrawableEntity(detectLine3,39);
		idTL  = RenderingManager.getInstance().addDrawableEntity(targetLine,39);
		
		timed  = false;
		notWaiting = true;
		localTime = 0;
		thresholdTime = 5;		
		
		seeding = false; // Will create other torpedoes (or missile ?)
		stayAlive = true; // Continue to live after creating other torpedoes
		timeSeparation = 0.5;
		nbSeparation = 3;
		typeCreation = Torpedo.TRIANGLE;
		timeStarted = 0;
		rotationFigure = 0;
		
		satLev1=false; // Satellite 1
		satLev2=false; // Satellite 1
		satLev3=false; // Satellite 1
	}
	
	
	/**
	 * @return the accelerationMax
	 */
	public double getAccelerationMax()
	{
		return accelerationMax;
	}

	/**
	 * @param accelerationMax the accelerationMax to set
	 */
	public void setAccelerationMax(double accelerationMax)
	{
		this.accelerationMax = accelerationMax;
	}

	/**
	 * @return the alt
	 */
	public double getAlt()
	{
		return alt;
	}

	/**
	 * @param alt the alt to set
	 */
	public void setAlt(double alt)
	{
		this.alt = alt;
	}

	/**
	 * @return the currentSpeed
	 */
	public double getCurrentSpeed()
	{
		return currentSpeed;
	}

	/**
	 * @param currentSpeed the currentSpeed to set
	 */
	public void setCurrentSpeed(double currentSpeed)
	{
		this.currentSpeed = currentSpeed;
		speedX=currentSpeed*Math.cos(this.orientation);
		speedY=currentSpeed*Math.sin(this.orientation);
	}

	/**
	 * @return the damage
	 */
	public int getDamage()
	{
		return damage;
	}

	/**
	 * @param damage the damage to set
	 */
	public void setDamage(int damage)
	{
		this.damage = damage;
	}

	/**
	 * @return the maxSpeed
	 */
	public double getMaxSpeed()
	{
		return maxSpeed;
	}

	/**
	 * @param maxSpeed the maxSpeed to set
	 */
	public void setMaxSpeed(double maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}

	/**
	 * @return the noiseLevel
	 */
	public int getNoiseLevel()
	{
		return noiseLevel;
	}

	/**
	 * @param noiseLevel the noiseLevel to set
	 */
	public void setNoiseLevel(int noiseLevel)
	{
		this.noiseLevel = noiseLevel;
	}

	/**
	 * @return the orientation
	 */
	public double getOrientation()
	{
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(double orientation)
	{
		this.orientation = orientation;
	}

	/**
	 * @return the standardSpeed
	 */
	public double getStandardSpeed()
	{
		return standardSpeed;
	}

	/**
	 * @param standardSpeed the standardSpeed to set
	 */
	public void setStandardSpeed(double standardSpeed)
	{
		this.standardSpeed = standardSpeed;
	}

	/**
	 * @return the visibilityLevel
	 */
	public int getVisibilityLevel()
	{
		return visibilityLevel;
	}

	/**
	 * @param visibilityLevel the visibilityLevel to set
	 */
	public void setVisibilityLevel(int visibilityLevel)
	{
		this.visibilityLevel = visibilityLevel;
	}


	public boolean isKnowAllies() {
		return knowAllies;
	}


	public void setKnowAllies(boolean knowAllies) {
		this.knowAllies = knowAllies;
	}


	public boolean isKnowEnemies() {
		return knowEnemies;
	}


	public void setKnowEnemies(boolean knowEnemies) {
		this.knowEnemies = knowEnemies;
	}
	

	public boolean isSeek() {
		return seek;
	}


	public void setSeek(boolean seek) {
		this.seek = seek;
	}


	public boolean isWpbased() {
		return wpbased;
	}


	public void setWpbased(boolean wpbased) {
		this.wpbased = wpbased;
	}


	public boolean isTarget() {
		return target;
	}


	public void setTarget(boolean target) {
		this.target = target;
	}
	
	/**
	 * @return the idMissile
	 */
	public int getIdMissile()
	{
		return idMissile;
	}

	/**
	 * @param idMissile the idMissile to set
	 */
	public void setIdMissile(int idMissile)
	{
		this.idMissile = idMissile;
	}
	
	public Signal getOurSignal() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setOurSignal(Signal ourSignal) {
		// TODO Auto-generated method stub
		
	}

	public boolean isExploding() {
		return exploding;
	}


	public boolean isDead() {
		return dead;
	}
	
	public void explode() {
		exploding=true;
	}


	public void kill() {
		dead= true;
	}


	/**
	 * Return the default image for the missile.
	 * @return the correct image
	 */
	public static CoreImage getImageForMe()
	{
		return gfxSprites.getImageSub();
	}
	/**
	 * Create the graphics entity
	 * If you have more than one torpedo (should be :) ), it is better to use the second
	 * method, with Image as parameter: so the image is shared, not loaded for all.
	 */
	public void createGfx(double x, double y, double z, double direction, double speed)
	{
		
		CoreImage missileImage=gfxSprites.getImageSub();
		
		createGfx(missileImage, x, y, z, direction, speed);
	}
	
	/**
	 * Create the graphics entity
	 * If you have more than one torpedo (should be :) ), it is better to use this
	 * method.
	 */
	public void createGfx(CoreImage torpedoImage, double x, double y, double z, double direction, double speed)
	{		
		torpedoBody = new FlatSpritePC(torpedoImage);
		torpedoBody.setRotation(direction);

		torpedoBody.setPos(x, y, z);
		torpedoBody.setSize(0.2);
		torpedoBody.setColored(false);
		torpedoBody.setOurColor(0xFF00FF00);
		idBody = RenderingManager.getInstance().addDrawableEntity(torpedoBody,5); // High
		
		ParticlePC bubbleExample=new ParticlePC();
		bubbleExample.setAlive(true);
		bubbleExample.setEnergy(10);
		bubbleExample.setSize(1);
		bubbleExample.setWeight(10);
		bubbleExample.setTimeLeft(200);
		bubbleExample.setMyColor(0xAA0030AA);
		bubbleExample.setColorAIncDec(-1);
		
		if (!satLev1)
		{
			bubblesEmitter = new Emitter(x, y, Math.PI, 0.1, Emitter.POINT_DIRECTIONNAL, 4, 10, 0, speed/20, 200,bubbleExample);
			//bubblesEmitter = new Emitter(x, y, 1, 10, 10, Math.PI, 0.1, Emitter.POINT_DIRECTIONNAL, 4, 10, 0, speed/20, 200);
			bubbles=new Particles(50,bubblesEmitter,ParticlePC.class);
			torpedoBody.addAttachedObject(bubblesEmitter);

			idBubbles = RenderingManager.getInstance().addParticles(bubbles, 5);
		}
	    // If wanted, create satellites
	    if (satLev1)
	    {
	    	satBody = new FlatSpritePC(torpedoImage);
	    	satBody.setRotation(direction);

	    	satBody.setPos(0, 0, 0);
	    	satBody.setPosAttach(50, 0, 0);
	    	satBody.setSize(0.2);
	    	satBody.setColored(false);
	    	satBody.setOurColor(0xFF00FF00);
	    	satBody.setRotation(0);
			idSatBody = RenderingManager.getInstance().addDrawableEntity(satBody,5);

			torpedoBody.addAttachedObject(satBody);
			
			lineSat = new SimpleLineFromCenterPC();
			lineSat.setSize(1);
			lineSat.setPos(0, 0, 0);
			lineSat.setPosAttach(0, 0, 0);
			lineSat.setOurColor(0xFFFFBBBB);
			idLineSat = RenderingManager.getInstance().addDrawableEntity(lineSat,5);
			torpedoBody.addAttachedObject(lineSat);
			if (satLev2)
			{
				satBody2 = new FlatSpritePC(gfxSprites.getImageFl8());
		    	satBody2.setRotation(direction);

		    	satBody2.setPos(0, 0, 0);
		    	satBody2.setPosAttach(30, 0, 0);
		    	satBody2.setSize(0.2);
		    	satBody2.setColored(false);
		    	satBody2.setOurColor(0xFF00FF00);
		    	satBody2.setRotation(0);
				idSatBody2 = RenderingManager.getInstance().addDrawableEntity(satBody2,5);

				satBody.addAttachedObject(satBody2);
				
				lineSat2 = new SimpleLineFromCenterPC();
				lineSat2.setSize(1);
				lineSat2.setPos(0, 0, 0);
				lineSat2.setPosAttach(0, 0, 0);
				lineSat2.setOurColor(0xFF44BB44);
				idLineSat2 = RenderingManager.getInstance().addDrawableEntity(lineSat2,5);
				satBody.addAttachedObject(lineSat2);
				if (satLev3)
				{
					satBody3 = new FlatSpritePC(gfxSprites.getImageFl10());
			    	satBody3.setRotation(direction);

			    	satBody3.setPos(0, 0, 0);
			    	satBody3.setPosAttach(25, 0, 0);
			    	satBody3.setSize(0.2);
			    	satBody3.setColored(false);
			    	satBody3.setOurColor(0xFF00FF00);
			    	satBody3.setRotation(0);
					idSatBody3 = RenderingManager.getInstance().addDrawableEntity(satBody3,5);

					satBody2.addAttachedObject(satBody3);
					
					lineSat3 = new SimpleLineFromCenterPC();
					lineSat3.setSize(5);
					lineSat3.setPos(0, 0, 0);
					lineSat3.setPosAttach(0, 0, 0);
					lineSat3.setOurColor(0xFF4444CC);
					idLineSat3 = RenderingManager.getInstance().addDrawableEntity(lineSat3,5);
					satBody2.addAttachedObject(lineSat3);
				}
			}
	    }
	}
	
//	public int addAttachedObject(IAttachable newObject)
//	{
//		newObject.setOrientationAttach(this.orientation);
//
//		transformToApply.setToIdentity();
//		transformToApply.translate(this.getPosX(), this.getPosY());
//		transformToApply.rotate(this.getOrientation(), 0, 0);
//		//transformToApply.scale(this.getSize(), this.getSize());
//		onePoint2D.setLocation(newObject.getXAttach(), newObject.getYAttach());
//		transformToApply.transform(onePoint2D, onePoint2Dtmp);
//		newObject.setAbsPos(onePoint2Dtmp.x, onePoint2Dtmp.y,0);
//		
//		attachedObjects.add(newObject);
//		return (attachedObjects.size() - 1);
//	}
//	
//	public void updateAttachedObject(IAttachable objectToUpdate)
//	{
//		objectToUpdate.setOrientationAttach(this.orientation);
//
//		transformToApply.setToIdentity();
//		transformToApply.translate(this.getPosX(), this.getPosY());
//		transformToApply.rotate(this.getOrientation(), 0, 0);
//		//transformToApply.scale(this.getSize(), this.getSize());
//		onePoint2D.setLocation(objectToUpdate.getXAttach(), objectToUpdate.getYAttach());
//		transformToApply.transform(onePoint2D, onePoint2Dtmp);
//		objectToUpdate.setAbsPos(onePoint2Dtmp.x, onePoint2Dtmp.y,0);
//	}
//	
//	public void updateAttachedObjects()
//	{
//		for (int iObjects=0;iObjects < attachedObjects.size() ; iObjects++)
//		{
//			updateAttachedObject(attachedObjects.get(iObjects));
//		}
//	}
//	
//	public void washAllAttachedObjects()
//	{
//		attachedObjects.clear();
//	}
	
	public void setColoredElement(int nbColor)
	{
		if (nbColor > 14)
		{
			nbColor = nbColor % 14;
		}
		if (nbColor < 0)
		{
			nbColor = 14;
		}
		myColor = nbColor;
		
		torpedoBody.setImageToDraw(gfxSprites.getImageFl(nbColor));
	}
	
	double orientationSat=0;
	double growing=0.1;
	double sizeSat=20;
	double sizeSat2=15;
	double sizeSat3=22; // 10
	double incRot=0.1; // 0.1
	
	private boolean willExplode = false;
	private boolean willCollide = false;
	private boolean willSplitOnCollision = false;
	private boolean willDieOnCollision = false;
	private boolean isSmart = false;
	
	/**
	 * Update the torpedo
	 * NB: We do not simulate the torpedo here (yet)
	 */
	public void updateMe(double x, double y, double z, double direction, double speed)
	{
		if (satLev1)
		{
			this.torpedoBody.setOrientationAttach(orientationSat);
			this.satBody.setPosAttach(5*(sizeSat+growing*orientationSat), 0, 0);
			this.lineSat.setSize(sizeSat+growing*orientationSat);
			if (satLev2)
			{
				this.satBody.setRotation(orientationSat*2);
				this.satBody2.setPosAttach(5*(sizeSat2+growing*orientationSat*1.1), 0, 0);
				this.lineSat2.setSize(sizeSat2+growing*orientationSat*1.1);
				if (satLev3)
				{
					this.satBody2.setRotation(orientationSat*3);
					this.satBody3.setPosAttach(5*(sizeSat3+growing*orientationSat*1.2), 0, 0);
					this.lineSat3.setSize(sizeSat3+growing*orientationSat*1.2);
				}
			}
		}
		
		
		orientationSat+=incRot;
		
		torpedoBody.setRotation(direction);
		torpedoBody.setPos(x, y, z);
		if (!satLev1)
		{
			bubblesEmitter.setSpeed(speed/20);
		}
	}
	
	public void hideMe()
	{
		torpedoBody.invalidate();
		bubbles.invalidate();
	}
	
	public void showMe()
	{
		torpedoBody.validate();
		bubbles.validate();
	}

	/**
	 * Remove the gfx elements
	 *
	 */
	public void removeMe()
	{
		RenderingManager.getInstance().removeEntity(idBody, 5);
		RenderingManager.getInstance().removeParticles(idBubbles, 5);
		RenderingManager.getInstance().removeEntity(idExplosion, 30);
		
		RenderingManager.getInstance().removeEntity(idDL1, 39);
		RenderingManager.getInstance().removeEntity(idDL2, 39);
		RenderingManager.getInstance().removeEntity(idDL3, 39);
		RenderingManager.getInstance().removeEntity(idTL, 39);
		if (satLev1)
		{
			RenderingManager.getInstance().removeEntity(idSatBody,5);
			RenderingManager.getInstance().removeEntity(idLineSat,5);
			if (satLev2)
			{
				RenderingManager.getInstance().removeEntity(idSatBody2,5);
				RenderingManager.getInstance().removeEntity(idLineSat2,5);
				if (satLev3)
				{
					RenderingManager.getInstance().removeEntity(idSatBody3,5);
					RenderingManager.getInstance().removeEntity(idLineSat3,5);
				}
			}
		}
		theMap.removeSignal(ourSignal);
	}
	
	public void accX(double xAcc)
	{
		speedX+=xAcc;
		//checkAndNormaliseSpeed();
	}
	
	public void accY(double yAcc)
	{
		speedY+=yAcc;
		//checkAndNormaliseSpeed();
	}
	

	public boolean seek(double time)
	{
		// For this type of missile: front radar-based.
		double xT2,yT2,xT3,yT3; // Coordinate (+ position of the Missile), of the corner of the detection triangle
		
		Submarine tmpSub;
		Boat tmpBoat;
		double distanceTmp;
		
		
		boolean found=false;
		double distanceFound = 100000; // Reset the distance of detection.
		
		// If active, will find a lot more of units in the surrounding
		// More in direct front and close.
		// It also generates a signal.


		if (!existingSignal)
		{
			theMap.addSignal(ourSignal);
			existingSignal = true;
		}
		detectionStrength = 1;

		
		// Determine the detection triangle...
		xT2=this.posX+detectionStrength*this.power*30*Math.cos(this.orientation+this.angle/2);
		yT2=this.posY+detectionStrength*this.power*30*Math.sin(this.orientation+this.angle/2);
		xT3=this.posX+detectionStrength*this.power*30*Math.cos(this.orientation-this.angle/2);
		yT3=this.posY+detectionStrength*this.power*30*Math.sin(this.orientation-this.angle/2);
		
		detectLine1.setPos(this.posX, this.posY, 0);
		detectLine1.setPosEnd(xT2,yT2);
		
		detectLine2.setPos(this.posX, this.posY, 0);
		detectLine2.setPosEnd(xT3,yT3);
		
		detectLine3.setPos(xT3, yT3, 0);
		detectLine3.setPosEnd(xT2,yT2);
		
		
		ourSubs=theMap.getOurSubs();
		ourBoats=theMap.getOurBoats();
		alliesBoats=theMap.getAlliesBoats();
		alliesSubs=theMap.getAlliesSubs();
		enemiesSubs=theMap.getEnemiesSubs();
		enemiesBoats=theMap.getEnemiesBoats();
		neutralBoats=theMap.getNeutralBoats();
		neutralSubs=theMap.getNeutralSubs();

		{
			if (!knowAllies)
			{
				if (ourSubs != null)
				{
					for (int iSub = 0; iSub < ourSubs.size() ; iSub++)
					{
						tmpSub = ourSubs.get(iSub);
						// Update !!!

						if (Utils.isInTriangle(tmpSub.getPosX(), tmpSub.getPosY(), this.posX, this.posY, xT2, yT2, xT3, yT3))
						{
							distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpSub.getPosX(), tmpSub.getPosY());
							//System.out.println("Found our sub - dist "+distanceTmp);
							if (distanceTmp < distanceFound)
							{
								distanceFound= distanceTmp;
								this.targetX = tmpSub.getPosX();
								this.targetY = tmpSub.getPosY();
								this.targetDepth = tmpSub.getDepth();

								found = true;
							}
						}

					}
				}
				if (ourBoats != null)
				{
					for (int iBoat = 0; iBoat < ourBoats.size() ; iBoat++)
					{		
						tmpBoat = ourBoats.get(iBoat);
						// Update !!!
						if (Utils.isInTriangle(tmpBoat.getPosX(), tmpBoat.getPosY(), this.posX, this.posY, xT2, yT2, xT3, yT3))
						{
							distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpBoat.getPosX(), tmpBoat.getPosY());
							//System.out.println("Found our boat - dist "+distanceTmp);
							if (distanceTmp < distanceFound)
							{
								distanceFound= distanceTmp;
								this.targetX = tmpBoat.getPosX();
								this.targetY = tmpBoat.getPosY();
								this.targetDepth = tmpBoat.getDepth();
								
								found = true;
							}
						}
					}
				}
				if (alliesSubs != null)
				{
					for (int iSub = 0; iSub < alliesSubs.size() ; iSub++)
					{
						tmpSub = alliesSubs.get(iSub);
						// Update !!!

						if (Utils.isInTriangle(tmpSub.getPosX(), tmpSub.getPosY(), this.posX, this.posY, xT2, yT2, xT3, yT3))
						{
							distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpSub.getPosX(), tmpSub.getPosY());
							//System.out.println("Found allie sub - dist "+distanceTmp);
							if (distanceTmp < distanceFound)
							{
								distanceFound= distanceTmp;
								this.targetX = tmpSub.getPosX();
								this.targetY = tmpSub.getPosY();
								this.targetDepth = tmpSub.getDepth();

								found = true;
							}
						}

					}
				}
				if (alliesBoats != null)
				{
					for (int iBoat = 0; iBoat < alliesBoats.size() ; iBoat++)
					{
						tmpBoat = alliesBoats.get(iBoat);
						// Update !!!
						if (Utils.isInTriangle(tmpBoat.getPosX(), tmpBoat.getPosY(), this.posX, this.posY, xT2, yT2, xT3, yT3))
						{
							distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpBoat.getPosX(), tmpBoat.getPosY());
							//System.out.println("Found allies boat - dist "+distanceTmp);
							if (distanceTmp < distanceFound)
							{
								distanceFound= distanceTmp;
								this.targetX = tmpBoat.getPosX();
								this.targetY = tmpBoat.getPosY();
								this.targetDepth = tmpBoat.getDepth();
								
								found = true;
							}
						}
					}
				}
			}
			if (!knowEnemies)
			{
				if (enemiesSubs != null)
				{
					for (int iSub = 0; iSub < enemiesSubs.size() ; iSub++)
					{
						tmpSub = enemiesSubs.get(iSub);
						// Update !!!

						if (Utils.isInTriangle(tmpSub.getPosX(), tmpSub.getPosY(), this.posX, this.posY, xT2, yT2, xT3, yT3))
						{
							distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpSub.getPosX(), tmpSub.getPosY());
							//System.out.println("Found enemy sub - dist "+distanceTmp);
							if (distanceTmp < distanceFound)
							{
								distanceFound= distanceTmp;
								this.targetX = tmpSub.getPosX();
								this.targetY = tmpSub.getPosY();
								this.targetDepth = tmpSub.getDepth();

								found = true;
							}
						}

					}
				}
				if (enemiesBoats != null)
				{
					for (int iBoat = 0; iBoat < enemiesBoats.size() ; iBoat++)
					{
						tmpBoat = enemiesBoats.get(iBoat);
						// Update !!!
						if (Utils.isInTriangle(tmpBoat.getPosX(), tmpBoat.getPosY(), this.posX, this.posY, xT2, yT2, xT3, yT3))
						{
							distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpBoat.getPosX(), tmpBoat.getPosY());
							//System.out.println("Found enemy boat - dist "+distanceTmp);
							if (distanceTmp < distanceFound)
							{
								distanceFound= distanceTmp;
								this.targetX = tmpBoat.getPosX();
								this.targetY = tmpBoat.getPosY();
								this.targetDepth = tmpBoat.getDepth();

								found = true;
							}
						}
					}
				}
			}
			if (neutralSubs != null)
			{
				for (int iSub = 0; iSub < neutralSubs.size() ; iSub++)
				{
					// Update !!!
					neutralSubs.get(iSub);
				}
			}
			if (neutralBoats != null)
			{
				for (int iBoat = 0; iBoat < neutralBoats.size() ; iBoat++)
				{
					// Update !!!
					neutralBoats.get(iBoat);
				}
			}
		}
		
		return found;
	}
	
	public void checkAndNormaliseSpeed()
	{
		actualSpeed = Math.sqrt(speedX*speedX+ speedY*speedY);
		
		
		if (speedX != 0)
		{
			this.orientation = Math.acos(speedX/actualSpeed);
		}
		else
		{
			this.orientation = Math.PI/2;
		}
		if (speedY < 0)
		{
			this.orientation=-this.orientation + 2*Math.PI;
		}
		
		if (actualSpeed > this.maxSpeed)
		{
			this.speedX=maxSpeed*(this.speedX/actualSpeed);
			this.speedY=maxSpeed*(this.speedY/actualSpeed);
			
			//this.currentSpeed = this.maxSpeed;	
		}	
		//System.out.println("Current speed "+currentSpeed);
	}
	
	public static double distSq(double x,double y,double x1, double y1)
	{
		return (Math.pow(x-x1, 2)+Math.pow(y-y1, 2));
	}
	
	public void seekWP()
	{
		if (!this.programmedWPs.isEmpty() && this.wpbased)
		{
			if (!started)
			{
				this.targetX=programmedWPs.getFirst().getXWP();
				this.targetY=programmedWPs.getFirst().getYWP();
				started = true;
			}
			// Target is the next WP, if we reach it, we go to the next one.
			distToTarget = Torpedo.distSq(this.posX, this.posY, this.targetX, this.targetY);
			if ( distToTarget < stopTargetingRadiusSq)
			{
				if (!seek && !programmedWPs.hasOneOrMoreElement())
				{
					// Explode !!!
					dead=true;
					this.removeMe();
					
					exploded=true;
					explodeTime=100;
					toRemove = true;
				}
				else
				{
					indexWP++;
					if (indexWP >= programmedWPs.size())
						this.wpbased = false;
					else
					{
						this.targetX=programmedWPs.getWP(indexWP).getXWP();
						this.targetY=programmedWPs.getWP(indexWP).getYWP();		
					}
				}
			}
			else
			{
				this.tmpSpeedX = (this.targetX - this.posX);
				this.tmpSpeedY = (this.targetY - this.posY);
				this.tmpSpeedN = Math.sqrt(tmpSpeedX*tmpSpeedX+ tmpSpeedY*tmpSpeedY);
				this.wantedSpeedX = this.tmpSpeedX / tmpSpeedN;
				this.wantedSpeedY = this.tmpSpeedY / tmpSpeedN;
				this.wantedSpeedX*=this.standardSpeed/8;
				this.wantedSpeedY*=this.standardSpeed/8;

				// Try to accelerate
				if ( (distToTarget < stopTargetingRadiusSq*2) && (this.currentSpeed > this.standardSpeed))
					this.currentSpeed -= 2;
				else if (this.currentSpeed < this.maxSpeed)
					this.currentSpeed += 2;

				//checkAndNormaliseSpeed();

				this.accX(wantedSpeedX);
				this.accY(wantedSpeedY);

				checkAndNormaliseSpeed();
			}
		}
	}
	
	public void setTimed()
	{
		localTime=0;
		timed=true;
		notWaiting=false;
	}
	
	public void setTimed(double threshold)
	{
		localTime=0;
		thresholdTime=threshold;
		timed=true;
		notWaiting=false;
	}
	
	public void setWaiting()
	{
		timed=false;
		notWaiting=false;
	}
	
	public void startMe()
	{
		if (!timed)
			notWaiting=true;
	}
	
//	boolean seeding; // Will create other torpedoes (or missile ?)
//	boolean stayAlive; // Continue to live after creating other torpedoes
//	double timeSeparation;
//	int nbSeparation;
//	int typeCreation;
	
	public void setCreating(boolean willStay,boolean recursive,boolean alternative, double timeBetween,int nbSeparation,int typeCreation)
	{
		this.seeding = true;
		this.stayAlive = willStay;
		this.timeSeparation = timeBetween;
		this.nbSeparation = nbSeparation;
		this.typeCreation = typeCreation;
		this.recursive = recursive;
		this.alternative = alternative;
		
		timeStarted = 0;
	}

	/* (non-Javadoc)
	 * @see com.tgb.subgame.units.ProgrammableUnit#doUpdate(double)
	 */
	@Override
	public synchronized void doUpdate(double time)
	{
		if (!dead && notWaiting)
		{
			//System.out.println("Energy left "+ energyLeft);
			energyLeft-=time;
	
			
			if (((this.posX < 0) || (this.posX > theMap.xMinForMenu) || (this.posY < 0) || (this.posY > theMap.yMax)) && ((nbSeparation <= 0) || (!seeding)))
			{
				dead=true;

				exploded=true;
				explodeTime=100;
				toRemove = true;
			}
			if (seeding)
			{
				if (nbSeparation > 0)
				{
					timeStarted+=time;
					if (timeStarted > timeSeparation)
					{
						nbSeparation--;
						timeStarted = 0;
						if (!stayAlive)
						{
							energyLeft=-1;
						}
						if (!alternative)
						{
							if (this.theMap.getTorpedoes().size() < MAX_NB_ON_SCREEN)
							{
								switch (typeCreation)
								{
									case Torpedo.LINE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 50;iSalve++)
										{
											Torpedo oneTorpedo= new Torpedo(this.theMap);
											oneTorpedo.setSatLev1(this.satLev1);
											oneTorpedo.setSatLev2(this.satLev2);
											oneTorpedo.setSatLev3(this.satLev3);
											oneTorpedo.createGfx(this.getPosX()+4*iSalve-100, this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneTorpedo.setTargetPos(this.getPosX()+8*iSalve, this.getPosY()+50, 0);
											oneTorpedo.setCurrentSpeed(150);
											oneTorpedo.setOrientation(this.getOrientation()+8*iSalve);
											oneTorpedo.setPosX(this.getPosX());
											oneTorpedo.setPosY(this.getPosY());
											oneTorpedo.setColoredElement(this.myColor+1+nbSeparation);									
											
											if (recursive)
											{
												oneTorpedo.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.LINE);
											}
											if (satLev3)
											{
												this.theMap.addTorpedo(oneTorpedo,4);
											}
											else if (satLev2)
											{
												this.theMap.addTorpedo(oneTorpedo,3);
											}
											else if (satLev1)
											{
												this.theMap.addTorpedo(oneTorpedo,2);
											}
											else
											{
												this.theMap.addTorpedo(oneTorpedo,1);
											}
										}
										break;
									case Torpedo.TRIANGLE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 3;iSalve++)
										{
											Torpedo oneTorpedo= new Torpedo(this.theMap);
											oneTorpedo.setSatLev1(this.satLev1);
											oneTorpedo.setSatLev2(this.satLev2);
											oneTorpedo.setSatLev3(this.satLev3);
											oneTorpedo.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneTorpedo.setTargetPos(this.getPosX()+50*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)), this.getPosY()+50*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)), 0);
											oneTorpedo.setCurrentSpeed(150);
											oneTorpedo.setOrientation(rotationFigure+iSalve*(2*Math.PI/3));
											oneTorpedo.setPosX(this.getPosX()+8*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)));
											oneTorpedo.setPosY(this.getPosY()+8*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)));
											oneTorpedo.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneTorpedo.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.TRIANGLE);
											}
											if (satLev3)
											{
												this.theMap.addTorpedo(oneTorpedo,4);
											}
											else if (satLev2)
											{
												this.theMap.addTorpedo(oneTorpedo,3);
											}
											else if (satLev1)
											{
												this.theMap.addTorpedo(oneTorpedo,2);
											}
											else
											{
												this.theMap.addTorpedo(oneTorpedo,1);
											}
										}
										rotationFigure+=Math.PI/30;
										break;
									case Torpedo.CIRCLE_LINE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 20;iSalve++)
										{
											Torpedo oneTorpedo= new Torpedo(this.theMap);
											oneTorpedo.setSatLev1(this.satLev1);
											oneTorpedo.setSatLev2(this.satLev2);
											oneTorpedo.setSatLev3(this.satLev3);
											oneTorpedo.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneTorpedo.setTargetPos(this.getPosX()+20, this.getPosY(), 0);
											oneTorpedo.setCurrentSpeed(150);
											oneTorpedo.setOrientation(this.getOrientation()+8*iSalve);
											oneTorpedo.setPosX(this.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
											oneTorpedo.setPosY(this.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
											oneTorpedo.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneTorpedo.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.CIRCLE_LINE);
											}
											if (satLev3)
											{
												this.theMap.addTorpedo(oneTorpedo,4);
											}
											else if (satLev2)
											{
												this.theMap.addTorpedo(oneTorpedo,3);
											}
											else if (satLev1)
											{
												this.theMap.addTorpedo(oneTorpedo,2);
											}
											else
											{
												this.theMap.addTorpedo(oneTorpedo,1);
											}
										}
										rotationFigure+=Math.PI/10;
										break;
									case Torpedo.CIRCLE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 20;iSalve++)
										{
											Torpedo oneTorpedo= new Torpedo(this.theMap);
											oneTorpedo.setSatLev1(this.satLev1);
											oneTorpedo.setSatLev2(this.satLev2);
											oneTorpedo.setSatLev3(this.satLev3);
											oneTorpedo.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneTorpedo.setTargetPos(this.getPosX()+20*Math.cos(rotationFigure+iSalve*(Math.PI/10)), this.getPosY()+20*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
											oneTorpedo.setCurrentSpeed(150);
											oneTorpedo.setOrientation(rotationFigure+iSalve*(Math.PI/10));
											oneTorpedo.setPosX(this.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
											oneTorpedo.setPosY(this.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
											oneTorpedo.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneTorpedo.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.CIRCLE);
											}
											if (satLev3)
											{
												this.theMap.addTorpedo(oneTorpedo,4);
											}
											else if (satLev2)
											{
												this.theMap.addTorpedo(oneTorpedo,3);
											}
											else if (satLev1)
											{
												this.theMap.addTorpedo(oneTorpedo,2);
											}
											else
											{
												this.theMap.addTorpedo(oneTorpedo,1);
											}
										}
										rotationFigure+=Math.PI/10;
										break;
									case Torpedo.SPIRAL:
										// Fire a missile !!!
										for (int iSalve=-10;iSalve < 10;iSalve++)
										{
											Torpedo oneTorpedo= new Torpedo(this.theMap);
											oneTorpedo.setSatLev1(this.satLev1);
											oneTorpedo.setSatLev2(this.satLev2);
											oneTorpedo.setSatLev3(this.satLev3);
											oneTorpedo.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneTorpedo.setTargetPos(this.getPosX()+20*Math.cos(rotationFigure+iSalve*(Math.PI/10)), this.getPosY()+20*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
											oneTorpedo.setCurrentSpeed(150);
											oneTorpedo.setOrientation(rotationFigure+iSalve*(Math.PI/10));
											oneTorpedo.setPosX(this.getPosX()+2*iSalve*Math.cos(rotationFigure));
											oneTorpedo.setPosY(this.getPosY()+2*iSalve*Math.sin(rotationFigure));
											oneTorpedo.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneTorpedo.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.SPIRAL);
											}
											if (satLev3)
											{
												this.theMap.addTorpedo(oneTorpedo,4);
											}
											else if (satLev2)
											{
												this.theMap.addTorpedo(oneTorpedo,3);
											}
											else if (satLev1)
											{
												this.theMap.addTorpedo(oneTorpedo,2);
											}
											else
											{
												this.theMap.addTorpedo(oneTorpedo,1);
											}
										}
										rotationFigure+=Math.PI/10;
										break;
									default:
										// Fire a torpedo !!!
										for (int iSalve=0;iSalve < 50;iSalve++)
										{
											Torpedo oneTorpedo= new Torpedo(this.theMap);
											oneTorpedo.setSatLev1(this.satLev1);
											oneTorpedo.setSatLev2(this.satLev2);
											oneTorpedo.setSatLev3(this.satLev3);
											oneTorpedo.createGfx(this.getPosX()+4*iSalve-100, this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneTorpedo.setTargetPos(this.getPosX(), this.getPosY()+50, 0);
											oneTorpedo.setCurrentSpeed(150);
											oneTorpedo.setOrientation(this.getOrientation()+8*iSalve);
											oneTorpedo.setPosX(this.getPosX()+4*iSalve-100);
											oneTorpedo.setPosY(this.getPosY());
											oneTorpedo.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneTorpedo.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.LINE);
											}
											if (satLev3)
											{
												this.theMap.addTorpedo(oneTorpedo,4);
											}
											else if (satLev2)
											{
												this.theMap.addTorpedo(oneTorpedo,3);
											}
											else if (satLev1)
											{
												this.theMap.addTorpedo(oneTorpedo,2);
											}
											else
											{
												this.theMap.addTorpedo(oneTorpedo,1);
											}
										}
								}
							}
						}
						else
						{
							if (this.theMap.getMissiles().size() < 800000)
							{
								switch (typeCreation)
								{
									case Torpedo.LINE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 50;iSalve++)
										{
											Missile oneMissile= new Missile(this.theMap);
											oneMissile.setSatLev1(this.satLev1);
											oneMissile.setSatLev2(this.satLev2);
											oneMissile.setSatLev3(this.satLev3);
											oneMissile.createGfx(this.getPosX()+4*iSalve-100, this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneMissile.setTargetPos(this.getPosX()+8*iSalve, this.getPosY()+50, 0);
											oneMissile.setCurrentSpeed(500);
											oneMissile.setOrientation(this.getOrientation()+8*iSalve);
											oneMissile.setPosX(this.getPosX());
											oneMissile.setPosY(this.getPosY());
											oneMissile.setColoredElement(this.myColor+1+nbSeparation);
											
											if (recursive)
											{
												oneMissile.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.LINE);
											}
											if (satLev3)
											{
												this.theMap.addMissile(oneMissile,4);
											}
											else if (satLev2)
											{
												this.theMap.addMissile(oneMissile,3);
											}
											else if (satLev1)
											{
												this.theMap.addMissile(oneMissile,2);
											}
											else
											{
												this.theMap.addMissile(oneMissile,1);
											}
										}
										break;
									case Torpedo.TRIANGLE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 3;iSalve++)
										{
											Missile oneMissile= new Missile(this.theMap);
											oneMissile.setSatLev1(this.satLev1);
											oneMissile.setSatLev2(this.satLev2);
											oneMissile.setSatLev3(this.satLev3);
											oneMissile.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneMissile.setTargetPos(this.getPosX()+50*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)), this.getPosY()+50*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)), 0);
											oneMissile.setCurrentSpeed(500);
											oneMissile.setOrientation(rotationFigure+iSalve*(2*Math.PI/3));
											oneMissile.setPosX(this.getPosX()+8*Math.cos(rotationFigure+iSalve*(2*Math.PI/3)));
											oneMissile.setPosY(this.getPosY()+8*Math.sin(rotationFigure+iSalve*(2*Math.PI/3)));
											oneMissile.setColoredElement(this.myColor+1+nbSeparation);
											
											if (recursive)
											{
												oneMissile.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.TRIANGLE);
											}
											if (satLev3)
											{
												this.theMap.addMissile(oneMissile,4);
											}
											else if (satLev2)
											{
												this.theMap.addMissile(oneMissile,3);
											}
											else if (satLev1)
											{
												this.theMap.addMissile(oneMissile,2);
											}
											else
											{
												this.theMap.addMissile(oneMissile,1);
											}
										}
										rotationFigure+=Math.PI/30;
										break;
									case Torpedo.CIRCLE_LINE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 20;iSalve++)
										{
											Missile oneMissile= new Missile(this.theMap);
											oneMissile.setSatLev1(this.satLev1);
											oneMissile.setSatLev2(this.satLev2);
											oneMissile.setSatLev3(this.satLev3);
											oneMissile.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneMissile.setTargetPos(this.getPosX()+20, this.getPosY(), 0);
											oneMissile.setCurrentSpeed(500);
											oneMissile.setOrientation(this.getOrientation()+8*iSalve);
											oneMissile.setPosX(this.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
											oneMissile.setPosY(this.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
											oneMissile.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneMissile.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.CIRCLE_LINE);
											}
											if (satLev3)
											{
												this.theMap.addMissile(oneMissile,4);
											}
											else if (satLev2)
											{
												this.theMap.addMissile(oneMissile,3);
											}
											else if (satLev1)
											{
												this.theMap.addMissile(oneMissile,2);
											}
											else
											{
												this.theMap.addMissile(oneMissile,1);
											}
										}
										rotationFigure+=Math.PI/10;
										break;
									case Torpedo.CIRCLE:
										// Fire a missile !!!
										for (int iSalve=0;iSalve < 20;iSalve++)
										{
											Missile oneMissile= new Missile(this.theMap);
											oneMissile.setSatLev1(this.satLev1);
											oneMissile.setSatLev2(this.satLev2);
											oneMissile.setSatLev3(this.satLev3);
											oneMissile.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneMissile.setTargetPos(this.getPosX()+50*Math.cos(rotationFigure+iSalve*(Math.PI/10)), this.getPosY()+50*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
											oneMissile.setCurrentSpeed(500);
											oneMissile.setOrientation(rotationFigure+iSalve*(Math.PI/10));
											oneMissile.setPosX(this.getPosX()+8*Math.cos(rotationFigure+iSalve*(Math.PI/10)));
											oneMissile.setPosY(this.getPosY()+8*Math.sin(rotationFigure+iSalve*(Math.PI/10)));
											oneMissile.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneMissile.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.CIRCLE);
											}
											if (satLev3)
											{
												this.theMap.addMissile(oneMissile,4);
											}
											else if (satLev2)
											{
												this.theMap.addMissile(oneMissile,3);
											}
											else if (satLev1)
											{
												this.theMap.addMissile(oneMissile,2);
											}
											else
											{
												this.theMap.addMissile(oneMissile,1);
											}
										}
										rotationFigure+=Math.PI/10;
										break;
									case Torpedo.SPIRAL:
										// Fire a missile !!!
										for (int iSalve=-10;iSalve < 10;iSalve++)
										{
											Missile oneMissile= new Missile(this.theMap);
											oneMissile.setSatLev1(this.satLev1);
											oneMissile.setSatLev2(this.satLev2);
											oneMissile.setSatLev3(this.satLev3);
											oneMissile.createGfx(this.getPosX(), this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneMissile.setTargetPos(this.getPosX()+50*Math.cos(rotationFigure+iSalve*(Math.PI/10)), this.getPosY()+50*Math.sin(rotationFigure+iSalve*(Math.PI/10)), 0);
											oneMissile.setCurrentSpeed(500);
											oneMissile.setOrientation(rotationFigure+iSalve*(Math.PI/10));
											oneMissile.setPosX(this.getPosX()+2*iSalve*Math.cos(rotationFigure));
											oneMissile.setPosY(this.getPosY()+2*iSalve*Math.sin(rotationFigure));
											oneMissile.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneMissile.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.SPIRAL);
											}
											if (satLev3)
											{
												this.theMap.addMissile(oneMissile,4);
											}
											else if (satLev2)
											{
												this.theMap.addMissile(oneMissile,3);
											}
											else if (satLev1)
											{
												this.theMap.addMissile(oneMissile,2);
											}
											else
											{
												this.theMap.addMissile(oneMissile,1);
											}
										}
										rotationFigure+=Math.PI/10;
										break;
									default:
										// Fire a torpedo !!!
										for (int iSalve=0;iSalve < 50;iSalve++)
										{
											Missile oneMissile= new Missile(this.theMap);
											oneMissile.setSatLev1(this.satLev1);
											oneMissile.setSatLev2(this.satLev2);
											oneMissile.setSatLev3(this.satLev3);
											oneMissile.createGfx(this.getPosX()+4*iSalve-100, this.getPosY(), 0, this.getOrientation()+8*iSalve, 500);
											oneMissile.setTargetPos(this.getPosX(), this.getPosY()+50, 0);
											oneMissile.setCurrentSpeed(500);
											oneMissile.setOrientation(this.getOrientation()+8*iSalve);
											oneMissile.setPosX(this.getPosX()+4*iSalve-100);
											oneMissile.setPosY(this.getPosY());
											oneMissile.setColoredElement(this.myColor+1+nbSeparation);
											if (recursive)
											{
												oneMissile.setCreating(this.stayAlive,true,false, this.timeSeparation*0.8,this.nbSeparation-1,Torpedo.LINE);
											}
											if (satLev3)
											{
												this.theMap.addMissile(oneMissile,4);
											}
											else if (satLev2)
											{
												this.theMap.addMissile(oneMissile,3);
											}
											else if (satLev1)
											{
												this.theMap.addMissile(oneMissile,2);
											}
											else
											{
												this.theMap.addMissile(oneMissile,1);
											}
										}
								}
							}
						}
					}
				}
			}
			if (energyLeft < 0)
			{
				dead=true;
				toRemove = true;
				explodeTime = 11;
			}
			else
			{
				if (seek && !target && !wpbased)
				{
					this.tmpSpeedX = (this.targetX - this.posX);
					this.tmpSpeedY = (this.targetY - this.posY);
					this.tmpSpeedN = Math.sqrt(tmpSpeedX*tmpSpeedX+ tmpSpeedY*tmpSpeedY);
					this.speedX = this.tmpSpeedX / tmpSpeedN;
					this.speedY = this.tmpSpeedY / tmpSpeedN;
					this.speedX*=this.currentSpeed;
					this.speedY*=this.currentSpeed;
					
					checkAndNormaliseSpeed();
					seek = false;
					seeking = true;
				}
				
				this.posX = this.posX + time*speedX+theMap.randomFactor*(Math.random()-0.5); // this.currentSpeed*Math.cos(this.orientation);
				this.posY = this.posY + time*speedY+theMap.randomFactor*(Math.random()-0.5); // this.currentSpeed*Math.sin(this.orientation);
				
				if ( ourLevelKeeper.getAlpha((int )this.posX, (int )this.posY) > 10)
				{
					dead=true;

					exploded=true;
					explodeTime=100;
					toRemove = true;
				}
				
				if (debugView)
				{
					targetLine.setPos(this.posX,this.posY,0);
					targetLine.setPosEnd(this.targetX, this.targetY);
				}

				//	If no waypoints and no target, we seek!
				if (!this.wpbased && target)
				{
					distToTarget = Torpedo.distSq(this.posX, this.posY, this.targetX, this.targetY);
					if ( distToTarget < stopTargetingRadiusSq)
					{
						if (!seek || seeking)
						{
							// Explode !!!
							dead=true;
							exploded=true;
							explodeTime=100;
							toRemove = true;
						}
						else
						{
							target = false; // Seek mode !!!
							seeking = true;

							if (debugView)
							{
								detectLine1.validate();
								detectLine2.validate();
								detectLine3.validate();
							}
						}
					}
					else
					{
						this.tmpSpeedX = (this.targetX - this.posX);
						this.tmpSpeedY = (this.targetY - this.posY);
						this.tmpSpeedN = Math.sqrt(tmpSpeedX*tmpSpeedX+ tmpSpeedY*tmpSpeedY);
						this.wantedSpeedX = this.tmpSpeedX / tmpSpeedN;
						this.wantedSpeedY = this.tmpSpeedY / tmpSpeedN;
						this.wantedSpeedX*=this.standardSpeed/4;
						this.wantedSpeedY*=this.standardSpeed/4;

						// Try to accelerate
						if ( (distToTarget < stopTargetingRadiusSq*2) && (this.currentSpeed > this.standardSpeed))
							this.currentSpeed -= 2;
						else if (this.currentSpeed < this.maxSpeed)
							this.currentSpeed += 2;

						//checkAndNormaliseSpeed();

						this.accX(wantedSpeedX);
						this.accY(wantedSpeedY);

						checkAndNormaliseSpeed();
					}
				}
				seekWP();
				if (seeking)
				{
					if (this.seek(time))
						target=true;
					else
					{
						// Continue to go straight... Until no more energy
						//target=false;
					}
				}

				this.updateMe(posX, posY, 0, this.orientation, this.currentSpeed);
			}
		}
		else
		{
			if (exploding)
			{
				if (explodeTime < 10)
				{
					explodeTime++;
					//explosion.setSize(explosion.getSize()+0.2);
				}
				else
				{
					//explosion.invalidate();
					toRemove = true;
				}
				if (!exploded)
				{
					damageUnits();
					exploded=true;
				}
			}
			if (timed)
			{
				localTime+=time;
				
				if (localTime > thresholdTime)
				{
					notWaiting=true;
				}
			}
		}
	}

	public void damageUnits()
	{
//		Submarine tmpSub;
//		Boat tmpBoat;
//		double distanceTmp;
//
//		ourSubs=theMap.getOurSubs();
//		ourBoats=theMap.getOurBoats();
//		alliesBoats=theMap.getAlliesBoats();
//		alliesSubs=theMap.getAlliesSubs();
//		enemiesSubs=theMap.getEnemiesSubs();
//		enemiesBoats=theMap.getEnemiesBoats();
//		neutralBoats=theMap.getNeutralBoats();
//		neutralSubs=theMap.getNeutralSubs();
//
//		if (ourSubs != null)
//		{
//			for (int iSub = 0; iSub < ourSubs.size() ; iSub++)
//			{
//				tmpSub = ourSubs.get(iSub);
//				// Update !!!
//
//				distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpSub.getPosX(), tmpSub.getPosY());
//				//System.out.println("Found our sub - dist "+distanceTmp);
//				if (distanceTmp < damageRadiusSq)
//				{
//					tmpSub.damageBoat(this.damage);
//				}
//
//			}
//		}
//		if (ourBoats != null)
//		{
//			for (int iBoat = 0; iBoat < ourBoats.size() ; iBoat++)
//			{		
//				tmpBoat = ourBoats.get(iBoat);
//
//				distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpBoat.getPosX(), tmpBoat.getPosY());
//				//System.out.println("Found our boat - dist "+distanceTmp);
//				if (distanceTmp < damageRadiusSq)
//				{
//					tmpBoat.damageBoat(this.damage);
//				}
//			}
//		}
//		if (alliesSubs != null)
//		{
//			for (int iSub = 0; iSub < alliesSubs.size() ; iSub++)
//			{
//				tmpSub = alliesSubs.get(iSub);
//				// Update !!!
//
//				distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpSub.getPosX(), tmpSub.getPosY());
//				//System.out.println("Found our sub - dist "+distanceTmp);
//				if (distanceTmp < damageRadiusSq)
//				{
//					tmpSub.damageBoat(this.damage);
//				}
//
//			}
//		}
//		if (alliesBoats != null)
//		{
//			for (int iBoat = 0; iBoat < alliesBoats.size() ; iBoat++)
//			{
//				tmpBoat = alliesBoats.get(iBoat);
//
//				distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpBoat.getPosX(), tmpBoat.getPosY());
//				//System.out.println("Found our boat - dist "+distanceTmp);
//				if (distanceTmp < damageRadiusSq)
//				{
//					tmpBoat.damageBoat(this.damage);
//				}
//			}
//		}
//		if (enemiesSubs != null)
//		{
//			for (int iSub = 0; iSub < enemiesSubs.size() ; iSub++)
//			{
//				tmpSub = enemiesSubs.get(iSub);
//				// Update !!!
//				distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpSub.getPosX(), tmpSub.getPosY());
//				//System.out.println("Found enemy sub - dist "+distanceTmp);
//				if (distanceTmp < damageRadiusSq)
//				{
//					//System.out.println("Damage enemy sub ");
//					tmpSub.damageBoat(this.damage);
//				}
//			}
//		}
//		if (enemiesBoats != null)
//		{
//			for (int iBoat = 0; iBoat < enemiesBoats.size() ; iBoat++)
//			{
//				tmpBoat = enemiesBoats.get(iBoat);
//
//				distanceTmp = Torpedo.distSq(this.posX, this.posY, tmpBoat.getPosX(), tmpBoat.getPosY());
//				//System.out.println("Found enemy boat - dist "+distanceTmp);
//				if (distanceTmp < damageRadiusSq)
//				{
//					//System.out.println("Damage enemy boat ");
//					tmpBoat.damageBoat(this.damage);
//				}
//			}
//		}
//		if (neutralSubs != null)
//		{
//			for (int iSub = 0; iSub < neutralSubs.size() ; iSub++)
//			{
//				// Update !!!
//				neutralSubs.get(iSub);
//			}
//		}
//		if (neutralBoats != null)
//		{
//			for (int iBoat = 0; iBoat < neutralBoats.size() ; iBoat++)
//			{
//				// Update !!!
//				neutralBoats.get(iBoat);
//			}
//		}
	}

	public double getDamageRadiusSq() {
		return damageRadiusSq;
	}


	public void setDamageRadiusSq(double damageRadiusSq) {
		this.damageRadiusSq = damageRadiusSq;
	}


	public int getDamageType() {
		return damageType;
	}


	public void setDamageType(int damageType) {
		this.damageType = damageType;
	}


	public int getExplodeTime() {
		return explodeTime;
	}


	public boolean toRemove() {
		if (toRemove && (explodeTime >= 10))
		{
			explosion.invalidate();
			return true;
		}
		else
			return false;
	}


	public void setToRemove(boolean toRemove) {
		this.toRemove = toRemove;
	}


	public int getIdOwner() {
		return idOwner;
	}


	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}
	
	@Override
	public void setTypeFaction(int typeFaction) {
		// TODO Auto-generated method stub
		
	}


	public double getEnergyLeft() {
		return energyLeft;
	}


	public void setEnergyLeft(double energyLeft) {
		this.energyLeft = energyLeft;
	}


	@Override
	public void doUpdateSM(double time)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void levelUp()
	{
		// TODO Auto-generated method stub
		this.level = this.newLevelReachable;
		// Nothing, shouldn't be called at all
		System.out.println("Torpedo levelled up ?!?");
	}
	
	@Override
	public void hideLabel()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showLabel()
	{
		// TODO Auto-generated method stub
		
	}


	/**
	 * @return the satLev1
	 */
	public boolean isSatLev1()
	{
		return satLev1;
	}


	/**
	 * @param satLev1 the satLev1 to set
	 */
	public void setSatLev1(boolean satLev1)
	{
		this.satLev1 = satLev1;
	}


	/**
	 * @return the satLev2
	 */
	public boolean isSatLev2()
	{
		return satLev2;
	}


	/**
	 * @param satLev2 the satLev2 to set
	 */
	public void setSatLev2(boolean satLev2)
	{
		this.satLev2 = satLev2;
	}


	/**
	 * @return the satLev3
	 */
	public boolean isSatLev3()
	{
		return satLev3;
	}


	/**
	 * @param satLev3 the satLev3 to set
	 */
	public void setSatLev3(boolean satLev3)
	{
		this.satLev3 = satLev3;
	}


	public void setSeeking(boolean seeking)
	{
		// TODO Auto-generated method stub
		
	}
	
	public void setWillExplod(boolean exploWanted)
	{
		this.willExplode = exploWanted;
	}


	public void setWillCollide(boolean collideWanted)
	{
		this.willCollide = collideWanted;
	}


	public void setWillSplitOnCollision(boolean collideSplitWanted)
	{
		this.willSplitOnCollision = collideSplitWanted;
	}


	public void setWillDieOnCollision(boolean collideDieWanted)
	{
		this.willDieOnCollision = collideDieWanted;
	}
	
	public void setSmart(boolean smartAllowed)
	{
		this.isSmart  = smartAllowed;
	}
}