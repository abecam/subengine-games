/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subgame;

/**
 * Main class to keep all information common to the game, like score, nb of units...
 * @author Alain Becam
 *
 */
public class GameKeeper {
	int Score=0;
	long costOur=0;
	long costAllies=0;
	long costEnemies=0;
	
	long budgetOur     = 2000000000L;
	long budgetAllies  = 1500000000L;
	long budgetEnemies =3000000000L;
	
	long humansOur=0;
	long humansAllies=0;
	long humansEnemies=0;
	
	long miaOur=0;
	long miaAllies=0;
	long miaEnemies=0;
	
	public int nbCarrierLeftOur=0;
	public int nbCarrierTotalOur=0;
	public int nbBoatLeftOur=0;
	public int nbBoatTotalOur=0;
	public int nbSubLeftOur=0;
	public int nbSubTotalOur=0;
	public int nbBasesLeftOur=0;
	public int nbBasesTotalOur=0;
	
	public int nbCarrierLeftAllies=0;
	public int nbCarrierTotalAllies=0;
	public int nbBoatLeftAllies=0;
	public int nbBoatTotalAllies=0;
	public int nbSubLeftAllies=0;
	public int nbSubTotalAllies=0;
	public int nbBasesLeftAllies=0;
	public int nbBasesTotalAllies=0;
	
	public int nbCarrierLeftEnemies=0;
	public int nbCarrierTotalEnemies=0;
	public int nbBoatLeftEnemies=0;
	public int nbBoatTotalEnemies=0;
	public int nbSubLeftEnemies=0;
	public int nbSubTotalEnemies=0;
	public int nbBasesLeftEnemies=0;
	public int nbBasesTotalEnemies=0;
	
	public int nbAmmo=0;
	public int nbAmmoAllies=0;
	public int nbAmmoEne=0;
	public int nbMissiles=0;
	public int nbMissilesAllies=0;
	public int nbMissilesEne=0;
	public int nbTorpedoes=0;
	public int nbTorpedoesAllies=0;
	public int nbTorpedoesEne=0;
	
	public int nbTurn=0;
	
	static private GameKeeper instance=null;
	
	private GameKeeper()
	{

	}

	public static GameKeeper getInstance()
	{
		if (instance== null)
			instance = new GameKeeper();
		
		return instance;
	}
	
	public void reset()
	{
		Score=0;
		costOur=0;
		costAllies=0;
		costEnemies=0;
		
		budgetOur     = 2000000000L;
		budgetAllies  = 1500000000L;
		budgetEnemies =3000000000L;
		
		humansOur=0;
		humansAllies=0;
		humansEnemies=0;
		
		miaOur=0;
		miaAllies=0;
		miaEnemies=0;
		
		nbCarrierLeftOur=0;
		nbCarrierTotalOur=0;
		nbBoatLeftOur=0;
		nbBoatTotalOur=0;
		nbSubLeftOur=0;
		nbSubTotalOur=0;
		nbBasesLeftOur=0;
		nbBasesTotalOur=0;
		
		nbCarrierLeftAllies=0;
		nbCarrierTotalAllies=0;
		nbBoatLeftAllies=0;
		nbBoatTotalAllies=0;
		nbSubLeftAllies=0;
		nbSubTotalAllies=0;
		nbBasesLeftAllies=0;
		nbBasesTotalAllies=0;
		
		nbCarrierLeftEnemies=0;
		nbCarrierTotalEnemies=0;
		nbBoatLeftEnemies=0;
		nbBoatTotalEnemies=0;
		nbSubLeftEnemies=0;
		nbSubTotalEnemies=0;
		nbBasesLeftEnemies=0;
		nbBasesTotalEnemies=0;
		
		nbAmmo=0;
		nbAmmoAllies=0;
		nbAmmoEne=0;
		nbMissiles=0;
		nbMissilesAllies=0;
		nbMissilesEne=0;
		nbTorpedoes=0;
		nbTorpedoesAllies=0;
		nbTorpedoesEne=0;
		
		nbTurn = 0;
	}
	
	public void addCostOur(long costToAdd)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			costOur+=costToAdd;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == -1)
		{
			ScoreKeeper.getInstance().addCostOur(costToAdd);
		}
	}
	
	public void addCostAllies(long costToAdd)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			costAllies+=costToAdd;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == -1)
		{
			ScoreKeeper.getInstance().addCostAllies(costToAdd);
		}
	}
	
	public void addCostEnemies(long costToAdd)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			costEnemies+=costToAdd;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == -1)
		{
			ScoreKeeper.getInstance().addCostEnemies(costToAdd);
		}
	}
	
	public void buySomethingAllies(long amount)
	{
		budgetAllies-=costAllies;
	}
	
	public void buySomethingOur(long amount)
	{
		budgetOur-=costOur;
	}
	
	public void buySomethingEnemies(long amount)
	{
		budgetEnemies-=costEnemies;
	}
	
	public void addPoint(int points)
	{
		Score+=points;
	}
	
	public void addComplementOur(long nb)
	{
		humansOur+=nb;
	}
	
	public void addComplementAllies(long nb)
	{
		humansAllies+=nb;
	}
	
	public void addMIAEnemies(long nb)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			miaEnemies+=nb;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == -1)
		{
			ScoreKeeper.getInstance().addMIAEnemies(nb);
		}
	}
	
	public void addMIAOur(long nb)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			miaOur+=nb;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == -1)
		{
			ScoreKeeper.getInstance().addMIAOur(nb);
		}
	}
	
	public void addMIAAllies(long nb)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			miaAllies+=nb;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == -1)
		{
			ScoreKeeper.getInstance().addMIAAllies(nb);
		}
	}
	
	public void addComplementEnemies(long nb)
	{
		humansEnemies+=nb;
	}

	public int getScore() {
		return Score;
	}

	public void setScore(int score) {
		Score = score;
	}

	public long getCostOur() {
		return costOur;
	}

	public void setCostOur(long costOur) {
		this.costOur = costOur;
	}

	public long getCostAllies() {
		return costAllies;
	}

	public void setCostAllies(long costAllies) {
		this.costAllies = costAllies;
	}

	public long getCostEnemies() {
		return costEnemies;
	}

	public void setCostEnemies(long costEnemies) {
		this.costEnemies = costEnemies;
	}

	public long getBudgetOur() {
		return budgetOur;
	}

	public void setBudgetOur(long budgetOur) {
		this.budgetOur = budgetOur;
	}

	public long getBudgetAllies() {
		return budgetAllies;
	}

	public void setBudgetAllies(long budgetAllies) {
		this.budgetAllies = budgetAllies;
	}

	public long getBudgetEnemies() {
		return budgetEnemies;
	}

	public void setBudgetEnemies(long budgetEnemies) {
		this.budgetEnemies = budgetEnemies;
	}

	public long getHumansOur() {
		return humansOur;
	}

	public void setHumansOur(long humansOur) {
		this.humansOur = humansOur;
	}

	public long getHumansAllies() {
		return humansAllies;
	}

	public void setHumansAllies(long humansAllies) {
		this.humansAllies = humansAllies;
	}

	public long getHumansEnemies() {
		return humansEnemies;
	}

	public void setHumansEnemies(long humansEnemies) {
		this.humansEnemies = humansEnemies;
	}

	public long getMiaOur()
	{
		return miaOur;
	}

	public void setMiaOur(long miaOur)
	{
		this.miaOur = miaOur;
	}

	public long getMiaAllies()
	{
		return miaAllies;
	}

	public void setMiaAllies(long miaAllies)
	{
		this.miaAllies = miaAllies;
	}

	public long getMiaEnemies()
	{
		return miaEnemies;
	}

	public void setMiaEnemies(long miaEnemies)
	{
		this.miaEnemies = miaEnemies;
	}

	public int getNbAmmo()
	{
		return nbAmmo;
	}

	public void setNbAmmo(int nbAmmo)
	{
		this.nbAmmo = nbAmmo;
	}

	public void addAmmo(int nbAmmo)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbAmmo += nbAmmo;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addAmmo(nbAmmo);
		}
	}

	
	public int getNbAmmoAllies()
	{
		return nbAmmoAllies;
	}

	public void setNbAmmoAllies(int nbAmmoAllies)
	{
		this.nbAmmoAllies = nbAmmoAllies;
	}
	
	public void addAmmoAllies(int nbAmmoAllies)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbAmmoAllies += nbAmmoAllies;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addAmmoAllies(nbAmmoAllies);
		}
	}

	public int getNbAmmoEne()
	{
		return nbAmmoEne;
	}

	public void setNbAmmoEne(int nbAmmoEne)
	{
		this.nbAmmoEne = nbAmmoEne;
	}
	
	public void addAmmoEne(int nbAmmoEne)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbAmmoEne += nbAmmoEne;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addAmmoEne(nbAmmoEne);
		}
	}

	public int getNbMissiles()
	{
		return nbMissiles;
	}

	public void setNbMissiles(int nbMissiles)
	{
		this.nbMissiles = nbMissiles;
	}
	
	public void addMissiles(int nbMissiles)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbMissiles += nbMissiles;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addMissiles(nbMissiles);
		}
	}

	public int getNbMissilesAllies()
	{
		return nbMissilesAllies;
	}

	public void setNbMissilesAllies(int nbMissilesAllies)
	{
		this.nbMissilesAllies = nbMissilesAllies;
	}

	public void addMissilesAllies(int nbMissilesAllies)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbMissilesAllies += nbMissilesAllies;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addMissilesAllies(nbMissiles);
		}
	}
	
	public int getNbMissilesEne()
	{
		return nbMissilesEne;
	}

	public void setNbMissilesEne(int nbMissilesEne)
	{
		this.nbMissilesEne = nbMissilesEne;
	}
	
	public void addMissilesEne(int nbMissilesEne)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbMissilesEne += nbMissilesEne;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addMissilesEne(nbMissilesEne);
		}
	}

	public int getNbTorpedoes()
	{
		return nbTorpedoes;
	}

	public void setNbTorpedoes(int nbTorpedoes)
	{
		this.nbTorpedoes = nbTorpedoes;
	}
	
	public void addTorpedoes(int nbTorpedoes)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbTorpedoes += nbTorpedoes;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addTorpedoes(nbTorpedoes);
		}
	}

	public int getNbTorpedoesAllies()
	{
		return nbTorpedoesAllies;
	}

	public void setNbTorpedoesAllies(int nbTorpedoesAllies)
	{
		this.nbTorpedoesAllies = nbTorpedoesAllies;
	}
	
	public void addTorpedoesAllies(int nbTorpedoesAllies)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbTorpedoesAllies += nbTorpedoesAllies;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addTorpedoesAllies(nbTorpedoesAllies);
		}
	}

	public int getNbTorpedoesEne()
	{
		return nbTorpedoesEne;
	}

	public void setNbTorpedoesEne(int nbTorpedoesEne)
	{
		this.nbTorpedoesEne = nbTorpedoesEne;
	}
	
	public void addTorpedoesEne(int nbTorpedoesEne)
	{
		if (LevelKeeper.getInstance().nextLevelWanted == 1)
		{
			this.nbTorpedoesEne += nbTorpedoesEne;
		}
		else if (LevelKeeper.getInstance().nextLevelWanted == 2)
		{
			ScoreKeeper.getInstance().addTorpedoesEne(nbTorpedoesEne);
		}
	}

	public int getNbTurn()
	{
		return nbTurn;
	}

	public void setNbTurn(int nbTurn)
	{
		this.nbTurn = nbTurn;
	}
	
	public void addTurn()
	{
		nbTurn++;
	}
	
	/**
	 * Calculate the percentage of allies loss compared to our loss
	 * @return
	 */
	public int calculateLossAlliesOur()
	{
//		if (nbTurn < 5)
//			return 0;
		int lossBoatAllies=this.nbBoatTotalAllies + this.nbCarrierTotalAllies - this.nbBoatLeftAllies - this.nbCarrierLeftAllies;
		int lossBoatOur=this.nbBoatTotalOur + this.nbCarrierTotalOur - this.nbBoatLeftOur - this.nbCarrierLeftOur;
		
		int lossBaseAllies=this.nbBasesTotalAllies - this.nbBasesLeftAllies;
		int lossBaseOur=this.nbBasesTotalOur - this.nbBasesLeftOur;
		
		int lossSubAllies=this.nbSubTotalAllies - this.nbSubLeftAllies;
		int lossSubOur=this.nbSubTotalOur - this.nbSubLeftOur;
		
		if ((lossBoatAllies+lossBaseAllies+lossSubAllies) == 0)
			return 0;
		if ((lossBoatOur+lossBaseOur+lossSubOur) == 0)
			return 100;
		
		int percentage=(100*(lossBoatAllies+lossBaseAllies+lossSubAllies))/((lossBoatAllies+lossBaseAllies+lossSubAllies)+lossBoatOur+lossBaseOur+lossSubOur);
		
		return percentage;
	}
}
