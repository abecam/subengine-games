/*
 * Created on 10 nov 2008
 */
 /*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

/** 
 * Facility to draw the different needed backgrounds.
 * Copyright (c)2008 Alain Becam
 */

package com.tgb.subgame;

import pulpcore.Stage;
import pulpcore.image.CoreImage;

import com.tgb.subengine.RenderingManager;
import com.tgb.subengine.gfxentities.FlatSpritePC;
import com.tgb.subengine.gfxentities.FlatSpritePCAdd;

public class DrawBackground
{	
	//static FlatSpritePCSpec background;
	static FlatSpritePCAdd background;
	static FlatSpritePC backgroundIntro;
	static FlatSpritePC backgroundTrans;
	static FlatSpritePC frontWinLose;
	static FlatSpritePCAdd backgroundMap;
	static long idBack,idBackSpl,idBackSpl2,idFrontWL,idBackMap;
	static boolean firstTime=true;
	static boolean firstTimeSplash=true;
	static boolean firstTimeMap=true;
	
	static boolean drawBack=true;
	
	public static void drawBack()
	{
		if (firstTime)
		{
			CoreImage backImage = CoreImage.load("Water3.png");
			CoreImage backImage2 = backImage.scale(Stage.getWidth(), Stage.getHeight());
			if (backImage2.isOpaque())
				System.out.println("Not ok");
			backImage2.setOpaque(false);
			//backImage2 = backImage2.fade(10);
			// Add background
			//add(new FilledSprite(0xFF00608A));
			//background = new FlatSpritePCSpec(backImage2);
			background = new FlatSpritePCAdd(backImage2);
			background.setAlpha(5);
			background.setPos(Stage.getWidth()/2, Stage.getHeight()/2, 0);
			firstTime = false;
		}
		if (drawBack)
		{
			idBack = RenderingManager.getInstance().addDrawableEntity(background,0);
		}
	}
	
	public static void setBack(CoreImage newBack)
	{
		if (newBack.isOpaque())
			System.out.println("Not ok");
		newBack.setOpaque(false);
		background.setImageToDraw(newBack);
	}
	
	public static void resetBack()
	{
		CoreImage backImage = CoreImage.load("Water3.png");
		CoreImage backImage2 = backImage.scale(Stage.getWidth(), Stage.getHeight());
		if (backImage2.isOpaque())
			System.out.println("Not ok");
		backImage2.setOpaque(false);
		
		background.setImageToDraw(backImage2);
	}
	
	public static void setAlphaBack(int theNewAlpha)
	{
		background.setAlpha(theNewAlpha);
	}
	
	public static void hideBack()
	{
		background.invalidate();
	}
	
	public static void showBack()
	{
		background.validate();
	}
	
	public static void drawBackMap()
	{
		if (firstTimeMap)
		{
			CoreImage imageToUse = CoreImage.load("MapVis.png");
			imageToUse.setOpaque(false);
			backgroundMap = new FlatSpritePCAdd(imageToUse);
	    	backgroundMap.setPos(350, Stage.getHeight()/2, 0);	
	    	backgroundMap.setAlpha(2);   	
			firstTimeMap = false;	
		}
		idBackMap = RenderingManager.getInstance().addDrawableEntity(backgroundMap,1);
	}
	
	public static void drawBackSplash()
	{
		//if (firstTimeSplash)
		{
			CoreImage backImage = CoreImage.load("WaterSplash2.png");
			CoreImage backImage2 = backImage.scale(Stage.getWidth(), Stage.getHeight());
			
			backgroundIntro = new FlatSpritePC(backImage2);
			backgroundIntro.setPos(Stage.getWidth()/2, Stage.getHeight()/2, 0);
			
			CoreImage backImageTrans = CoreImage.load("WaterSplash3.png");
			CoreImage backImageTrans2 = backImageTrans.scale(Stage.getWidth(), Stage.getHeight());
			// Add background
			//add(new FilledSprite(0xFF00608A));
			backgroundTrans = new FlatSpritePC(backImageTrans2);
			backgroundTrans.setPos(Stage.getWidth()/2, Stage.getHeight()/2, 0);
			firstTimeSplash = false;
		}
    	idBackSpl = RenderingManager.getInstance().addDrawableEntity(backgroundIntro,0);
    	idBackSpl2 = RenderingManager.getInstance().addDrawableEntity(backgroundTrans,0);
	}
	
	public static void revealTitle()
	{
		// Remove the background update, so the explosions (and others) will stay in the
		// mask
		 RenderingManager.getInstance().removeEntity(idBackSpl, 0);
	}

	public static void resetSplash()
	{
		idBackSpl = RenderingManager.getInstance().addDrawableEntity(background,0);
	}
	
	public static void removeSplash()
	{
		RenderingManager.getInstance().removeEntity(idBackSpl , 0);
		RenderingManager.getInstance().removeEntity(idBackSpl2 , 0);
		// We do not need them for a while, so save a little bit of memory
		// by forcing their removing
		backgroundIntro = null;
		backgroundTrans = null;
	}
	
	public static void reset()
	{
		idBack = RenderingManager.getInstance().addDrawableEntity(background,0);
	}
	
	public static void remove()
	{
		RenderingManager.getInstance().removeEntity(idBack , 0);
		RenderingManager.getInstance().removeEntity(idBackMap , 1);
	}
	
	public static void removeMap()
	{
		RenderingManager.getInstance().removeEntity(idBackMap , 1);
	}
	
	public static void drawWinLose(boolean isWon)
	{
		CoreImage frontImage;
		
		if (isWon)
		{
			frontImage = CoreImage.load("Win.png");
		}
		else
		{
			frontImage = CoreImage.load("Lose.png");
		}
		frontWinLose = new FlatSpritePC(frontImage);
		frontWinLose.setPos(Stage.getWidth()/3, Stage.getHeight()/2, 0);
		
		idFrontWL = RenderingManager.getInstance().addDrawableEntity(frontWinLose,39);
	}
	
	public static void unload()
	{
		RenderingManager.getInstance().removeEntity(idBackSpl , 0);
		RenderingManager.getInstance().removeEntity(idBackSpl2 , 0);
		RenderingManager.getInstance().removeEntity(idBack , 0);
		RenderingManager.getInstance().removeEntity(idBackMap , 1);
		background=null;
		backgroundTrans=null;
		frontWinLose=null;
		backgroundMap=null;
	}
}
