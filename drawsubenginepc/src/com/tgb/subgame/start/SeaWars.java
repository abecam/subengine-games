package com.tgb.subgame.start;

import java.io.File;

// From PulpCore BubbleMark implementation. See http://www.bubblemark.com/

import com.tgb.subengine.RenderingManager;
import com.tgb.subengine.gamesystems.StateManager;
import com.tgb.subengine.UtilsPC;
import com.tgb.subgame.LevelKeeper;
import com.tgb.subgame.SimMusic;
import com.tgb.subgame.unitspc.Missile;
import com.tgb.subgame.unitspc.Torpedo;
import com.tgb.subgame.DrawingPart;

import pulpcore.animation.Bool;
import pulpcore.animation.Fixed;
import pulpcore.image.CoreGraphics;
import pulpcore.image.CoreImage;
import pulpcore.scene.Scene2D;
import pulpcore.sprite.Button;
import pulpcore.sprite.Label;
import pulpcore.sprite.Group;
import pulpcore.sprite.FilledSprite;
import pulpcore.sprite.Slider;
import pulpcore.sprite.Sprite;
import pulpcore.Stage;
import pulpcore.Assets;
import pulpcore.image.BlendMode;

public class SeaWars extends Scene2D {
    
    Bool pixelSnapping = new Bool(false);
    Fixed frameRate = new Fixed();
    int nbOfElements = Missile.MAX_NB_ON_SCREEN + Torpedo.MAX_NB_ON_SCREEN;
    int nbOfCurrElements = 0;
    Slider mySlider;
    Button speakerButton;
    Button noteButton;
    
    RenderingManager myRenderingManager;
    com.tgb.subengine.RendererPulpeC myRenderer;
    
    DrawingPart myDrawingPart;
    
	StateManager theStateManager;
	
	SimMusic mySimMusic; // Player of streamed MP3
	
	boolean notInitialised=true;
	
	FilledSprite backgrd ;
	Label loadingLabel ;
	Group loadingFillingText ;
	double lFTwidth,lFTheight;
	static final String lineOfShakespeareText = "\"You do look, my son, in a moved sort,\nAs if you were dismay'd: be cheerful, sir.\nOur revels now are ended. These our actors,\nAs I foretold you, were all spirits, and\nAre melted into air, into thin air:\nAnd, like the baseless fabric of this vision,\nThe cloud-capp'd towers, the gorgeous palaces,\nThe solemn temples, the great globe itself,\nYea, all which it inherit, shall dissolve,\nAnd, like this insubstantial pageant faded,\nLeave not a rack behind. We are such stuff\nAs dreams are made on; and our little life\nIs rounded with a sleep. Sir, I am vex'd;\nBear with my weakness; my old brain is troubled:\nBe not disturb'd with my infirmity:\nIf you be pleased, retire into my cell,\nAnd there repose: a turn or two I'll walk,\nTo still my beating mind.\"\n\n    William Shakespeare, The Tempest,\n    From The Project Gutenberg (www.gutenberg.org)";
	
	int timePassed=0;
	
	private Label nbOfCurrElementsLabel;
	private Label nbOfElementsLabel;
//	CoreImage myImage;
//	CoreImage levelImage;
	
//	FlatSpritePC background;
	
    @Override
    public void load() {
    	
    	this.setDirtyRectanglesEnabled(false);
    	this.setMaxElapsedTime(5000);

    	System.out.println("Loading first asset");
    	
        Assets.addCatalogFromFileSystem("SeaWars.zip", new File("Assets/Draw/"));

    	System.out.println("Set loading screen");
    	backgrd = new FilledSprite(20, 20, 860, 760, 0x55ffffff); 
    	add(backgrd);
    	loadingLabel = new Label("Loading, please wait", 400, 350);
    	loadingLabel.setAnchor(Sprite.WEST);
    	add(loadingLabel);
    	loadingFillingText = Label.createMultilineLabel(lineOfShakespeareText, 450, 560);
    	loadingFillingText.setAnchor(Sprite.CENTER);
    	lFTwidth=loadingFillingText.width.get();
    	lFTheight=loadingFillingText.height.get();
    	loadingFillingText.setSize(0.01*lFTwidth, 0.01*lFTheight);
    	add(loadingFillingText);
    	
    	System.out.println("Create Gfx Managers");
    	System.out.println("Renderer");
    	myRenderer = com.tgb.subengine.RendererPulpeC.getInstance();
    	System.out.println("Rendering Manager");
		myRenderingManager=RenderingManager.getInstance();
		System.out.println("Create Gfx Managers - done");
		
		
//    	Assets.addCatalog("Sub.zip", UtilsPC.getURLInputStream(UtilsPC.getResource("~GBT/images/Sub.zip")));
//	
//        //add(new ImageSprite(backImage2,0,0,Stage.getWidth(),Stage.getHeight()));
//        
////        mySlider = new Slider("particles.png", "HourHand.png", 0, 80);
////
////        add(mySlider);
//    	remove(loadingLabel);
//    	remove(backgrd);
//    	
//        // Add fps display
//        Label frameRateLabel = new Label("%.1f fps", 5, 5);
//        frameRateLabel.setFormatArg(frameRate);
//        add(frameRateLabel);     
//        
//        CoreImage imagesSpeaker[] = new CoreImage[]{ CoreImage.load("Speaker.png"),
//        		CoreImage.load("SpeakerPreOn.png"),
//        		CoreImage.load("SpeakerPre2On.png"),
//        		CoreImage.load("SpeakerOff.png"),
//        		CoreImage.load("SpeakerPre2.png"),
//        		CoreImage.load("SpeakerPre.png")
//        };
//        speakerButton = new Button(imagesSpeaker, 930, 750,true);
//
//        add(speakerButton);
//        
//        CoreImage imagesNote[] = new CoreImage[]{ CoreImage.load("Note.png"),
//        		CoreImage.load("NoteSel2.png"),
//        		CoreImage.load("NoteSel3.png"),
//        		CoreImage.load("NoteOff.png"),
//        		CoreImage.load("NoteOffSel2.png"),
//        		CoreImage.load("NoteOffSel.png")
//        };
//        noteButton = new Button(imagesNote, 970, 750,true);
//
//        add(noteButton);
//
//        try
//    	{
//    		myRenderer = com.tgb.subengine.gfxentities.RendererPulpeC.getInstance();
//    		myRenderingManager=RenderingManager.getInstance();
//    		
//    		myMap=new com.tgb.subgame.TacticalMapPC();
//    		myDrawMap=new com.tgb.subgame.DrawMap();
//    		myStrategicMap=new com.tgb.subgame.StrategicMap();
//    		myScriptedMap=new com.tgb.subgame.ScriptedMap();
//    		myPrepareRandomMap=new com.tgb.subgame.PrepareRandomMap();
//    		myTutorialMapPC = new com.tgb.subgame.TutorialMap2();
//    		myTutorialMap1 = new com.tgb.subgame.TutorialMap1();
//    		myWonMap=new WonMap();
//    		myLevelScoreMap = new com.tgb.subgame.LevelScoreMap();
//
//    		theStateManager=StateManager.getInstance();
//
//    		theStateManager.addPart(myMap);
//    		theStateManager.addPart(myDrawMap);
//    		theStateManager.addPart(myStrategicMap);
//    		theStateManager.addPart(myScriptedMap);
//    		theStateManager.addPart(myPrepareRandomMap);
//    		theStateManager.addPart(myWonMap);
//    		theStateManager.addPart(myTutorialMapPC);
//    		theStateManager.addPart(myTutorialMap1);
//    		theStateManager.addPart(myLevelScoreMap);
//    		
//    		mySimMusic = SimMusic.getInstance();
//    		
//    		theStateManager.startPart(myScriptedMap.getStateName());
//    		
//    		//mySimMusic.playFile();
//    		//theStateManager.startPart(myStrategicMap.getStateName());
//
//    	}
//    	catch (Exception e)
//    	{
//    		e.printStackTrace();
//    	}
    }

    Thread worker; 
    int posInText=0;
    double sizeFT=0.01;
    String endString="...";
    
	@Override
	public synchronized void update(int elapsedTime) {   
		if (notInitialised)
		{
			//if (timePassed > 15)
			{
				loadingFillingText.setSize(sizeFT*lFTwidth, sizeFT*lFTheight);
				
				if (sizeFT < 1)
				{
					sizeFT+=0.0003;
				}
				else
				{
					sizeFT=1;
				}
//				loadingFillingText = Label.createMultilineLabel(lineOfShakespeareText.substring(0,posInText), 500, 450);
//				if (posInText < lineOfShakespeareText.length())
//				{
//					posInText++;
//				}
			}
			if (timePassed > 15)
			{
				loadingLabel.setText( "Loading, please wait"+endString.substring(0, posInText) );
				posInText++;
				if (posInText > endString.length())
				{
					posInText=0;
				}
				timePassed=0;
			}
			else
			{
				timePassed++;
			}
		}
		if (notInitialised && worker == null)
		{
			worker = new Thread() {

				@Override
                public void run() {              
        	    	
                	System.out.println("Set-up the common interface");
                	
        	        // Add fps display
        	        Label frameRateLabel = new Label("%.1f fps", 5, 5);
        	        frameRateLabel.setFont(frameRateLabel.getFont().tint(0xffff));
        	        frameRateLabel.setFormatArg(frameRate);
        	        add(frameRateLabel);     
        	        
        	        nbOfElementsLabel = new Label("%.1f max elements", 5, 20);
        	        nbOfElementsLabel.setFont(nbOfElementsLabel.getFont().tint(0xffff));
        	        nbOfElementsLabel.setFormatArg(nbOfElements);
        	        add(nbOfElementsLabel); 
        	        
        	        nbOfCurrElementsLabel = new Label("%.1f current elements", 5, 35);
        	        nbOfCurrElementsLabel.setFont(nbOfCurrElementsLabel.getFont().tint(0xffff));
        	        nbOfCurrElementsLabel.setFormatArg(nbOfCurrElements);
        	        add(nbOfCurrElementsLabel); 
        	        
        	        Label musicByLabel = new Label("Music by Kevin MacLeod", 5, 940);
        	        musicByLabel.setFont(frameRateLabel.getFont().tint(0xffff));
        	        musicByLabel.setFormatArg(frameRate);
        	        add(musicByLabel);
        	        
        	        CoreImage imagesSpeaker[] = new CoreImage[]{ CoreImage.load("Speaker.png"),
        	        		CoreImage.load("SpeakerPreOn.png"),
        	        		CoreImage.load("SpeakerPre2On.png"),
        	        		CoreImage.load("SpeakerOff.png"),
        	        		CoreImage.load("SpeakerPre2.png"),
        	        		CoreImage.load("SpeakerPre.png")
        	        };
        	        
        	        int xMinMenu = Stage.getWidth() - 200;
        	        
        	        speakerButton = new Button(imagesSpeaker, xMinMenu + 130, 850,true);

        	        add(speakerButton);
        	        
        	        CoreImage imagesNote[] = new CoreImage[]{ CoreImage.load("Note.png"),
        	        		CoreImage.load("NoteSel2.png"),
        	        		CoreImage.load("NoteSel3.png"),
        	        		CoreImage.load("NoteOff.png"),
        	        		CoreImage.load("NoteOffSel2.png"),
        	        		CoreImage.load("NoteOffSel.png")
        	        };
        	        noteButton = new Button(imagesNote,  xMinMenu + 170, 850,true);

        	        add(noteButton);

        	        System.out.println("Done");
        	        
        	        try
        	    	{
        	        	System.out.println("Create the State");
        	        	
//        	    		myMap=new com.tgb.subgame.TacticalMapPC();
//        	    		myDrawMap=new com.tgb.subgame.DrawMap();
//        	    		myStrategicMap=new com.tgb.subgame.StrategicMap();
//        	    		myScriptedMap=new com.tgb.subgame.ScriptedMap();
//        	    		myPrepareRandomMap=new com.tgb.subgame.PrepareRandomMap();
//        	    		myTutorialMapPC = new com.tgb.subgame.TutorialMap2();
//        	    		myTutorialMap1 = new com.tgb.subgame.TutorialMap1();
//        	    		myWonMap=new WonMap();
//        	    		myLevelScoreMap = new com.tgb.subgame.LevelScoreMap();
        	    		myDrawingPart = new DrawingPart();

        	    		theStateManager=StateManager.getInstance();

//        	    		theStateManager.addPart(myMap);
//        	    		theStateManager.addPart(myDrawMap);
//        	    		theStateManager.addPart(myStrategicMap);
//        	    		theStateManager.addPart(myScriptedMap);
//        	    		theStateManager.addPart(myPrepareRandomMap);
//        	    		theStateManager.addPart(myWonMap);
//        	    		theStateManager.addPart(myTutorialMapPC);
//        	    		theStateManager.addPart(myTutorialMap1);
//        	    		theStateManager.addPart(myLevelScoreMap);
        	    		theStateManager.addPart(myDrawingPart);
        	    		
        	    		System.out.println("Done");
        	    		
        	    		System.out.println("Setup the music and remove the loading screen");
        	    		
        	    		mySimMusic = SimMusic.getInstance();
        	    		
        	    		remove(loadingLabel);
            	    	remove(loadingFillingText);
            	    	remove(backgrd);
        	    		
        	    		theStateManager.startPart(myDrawingPart.getStateName());
        	    		
        	    		mySimMusic.playFile();
        	    		
        	    		System.out.println("Done");
        	    		//theStateManager.startPart(myStrategicMap.getStateName());
        	    		notInitialised = false;
        	    		System.out.println("Loading finished");

        	    	}
        	    	catch (Exception e)
        	    	{
        	    		e.printStackTrace();
        	    	}
                }
            };
            worker.start(); 
	
		}	
		
		// Game loop
		if (!notInitialised)
		{
			// Sound on/off
			if (speakerButton.isClicked())
			{
				com.tgb.subgame.unitspc.SndContainer.setSpeakerOn(!speakerButton.isSelected());
			}
			if (noteButton.isClicked())
			{
				if (noteButton.isSelected())
				{
					mySimMusic.stop();
				}
				else
				{
					mySimMusic.playLastFile();
				}
			}
			
			try
			{

				theStateManager.doLoop();
//				if (LevelKeeper.getInstance().loadSprite())
//				{
//				// Add a new foreground !
//				System.out.println("Add a new level");
//				levelImage=LevelKeeper.getInstance().getCurrentLevel();
//				add(new ImageSprite(levelImage,0,0,Stage.getWidth(),Stage.getHeight()));
//				}
				myRenderingManager=RenderingManager.getInstance();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			// Update fps display
			double fps = Stage.getActualFrameRate();
			if (fps >= 0) {
				frameRate.set(fps);
			}
			if (fps > 40)
			{
				theStateManager.doMore();
				nbOfElements = Missile.MAX_NB_ON_SCREEN + Torpedo.MAX_NB_ON_SCREEN;
				nbOfElementsLabel.setText(nbOfElements + " max elements");
			}
			if (fps < 10)
			{
				theStateManager.doLess();
				nbOfElements = Missile.MAX_NB_ON_SCREEN + Torpedo.MAX_NB_ON_SCREEN;
				nbOfElementsLabel.setText(nbOfElements + " max elements");
			}
			nbOfCurrElements = myDrawingPart.getMissiles().size() + myDrawingPart.getTorpedoes().size();
			nbOfCurrElementsLabel.setText(nbOfCurrElements + " elements");
		}
	}
    
    @Override
	public void drawScene(CoreGraphics arg0) {
    	
    	myRenderer.setGraphics(arg0);
    	arg0.setBlendMode(BlendMode.SrcOver());
    	
    	myRenderingManager.setMiddlePos(Stage.getWidth()/2-200); // 200 is the menu width...
		//
		// TODO Auto-generated method stub
		
		//super.drawScene(arg0);
    	if (!notInitialised)
		{
			myRenderingManager.paint();
		}
			
		super.drawScene(arg0);	
	}

	public void setCapFrameRate(final boolean capFrameRate) {
        invokeLater(new Runnable() {
            public void run() {
                Stage.setFrameRate(capFrameRate ? Stage.DEFAULT_FPS : Stage.MAX_FPS);
            }
        });
    }
    
    public void setPixelSnapping(final boolean pixelSnapping) {
        invokeLater(new Runnable() {
            public void run() {
                SeaWars.this.pixelSnapping.set(pixelSnapping);
            }
        });
    }

	@Override
	public synchronized void unload() {
		if (mySimMusic != null)
		{
			mySimMusic.stop();
			System.out.println("Javazoom streaming music stopped");
		}
		System.out.println("MapKeeper2 unloaded");
		StateManager.unload();
		System.out.println("StateManager unloaded");
		LevelKeeper.unload();
		System.out.println("LevelKeeper unloaded");
		System.out.println("Sub-Engine removed");
		// TODO Auto-generated method stub
		super.unload();
		
	}
}
