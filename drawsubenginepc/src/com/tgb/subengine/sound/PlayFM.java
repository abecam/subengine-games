/*
 * Created on 1 nov. 13
 *
 */

/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subengine.sound;

import javax.sound.sampled.AudioFormat;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.JavaSoundAudioDevice;

public class PlayFM extends Thread
{
	short sinWave[] = new short[1000];
	private JavaSoundAudioDevice myPlayer = new JavaSoundAudioDevice();
	private boolean notEnded = true;
	private boolean notRunning = true;
	
	public PlayFM()
	{
		generateSin();
		
		try
		{
			myPlayer.open(new AudioFormat(22050, 16, 1, true, false));
		}
		catch (RuntimeException ex)
		{
			ex.printStackTrace();
		} 
		catch (JavaLayerException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void generateSin()
	{	
		for (int i = 0 ; i < 1000 ; i++)
		{
			sinWave[i] = (short )(32767*Math.sin(2*Math.PI*((double )i)/1000));
		}
	}
	
	public void generateAndPlaySin()
	{	
		// Will change what is played;
		play();
	}
	
	public void play()
	{
		if (notRunning )
		{
			notRunning = false;
			this.start();
		}
	}

	public void stopPlay()
	{
		notEnded = false;
		notRunning = true;
	}
	
	public void quit()
	{
		myPlayer.close();
	}
	
	public void run()
	{
		while (notEnded)
		{
			try
			{
				myPlayer.write(sinWave, 0, sinWave.length);
			} 
			catch (JavaLayerException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		myPlayer.flush();
//		myPlayer.close();
		notEnded = true;
	}
}
