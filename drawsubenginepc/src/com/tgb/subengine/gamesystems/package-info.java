/**
 * The simple state machine used to navigate from one part of a game to another
 *
 * @author Alain Becam
 */
package com.tgb.subengine.gamesystems;