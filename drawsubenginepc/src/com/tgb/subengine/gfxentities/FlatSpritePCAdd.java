/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subengine.gfxentities;

import com.tgb.subengine.RendererPulpeC;

import pulpcore.image.BlendMode;
import pulpcore.image.CoreImage;
import pulpcore.image.CoreGraphics;


/**
 * A flat sprite, that can be translated  and resized, but not rotated.
 * @author Alain Becam
 *
 */
public class FlatSpritePCAdd extends Drawable2DEntity
{
	public static final int DST_OUT = 0;
	public static final int SRC_ATOP = 1;
	public static final int MULTIPLY = 2;
	public static final int XOR = 3;
	public static final int ADD = 100;

	int width,height;
	
	static BlendMode ourBlending;
	
	/* (non-Javadoc)
	 * @see subengine.gfxentities.Drawable2DEntity#getCenterX()
	 */
	@Override
	public double getCenterX()
	{
		// TODO Auto-generated method stub
		return this.getX();//((this.getX()+this.getSize()*ourImage.getWidth()/2));
	}
	/* (non-Javadoc)
	 * @see subengine.gfxentities.Drawable2DEntity#getCenterY()
	 */
	@Override
	public double getCenterY()
	{
		// TODO Auto-generated method stub
		return this.getY();//((this.getY()+this.getSize()*ourImage.getHeight()/2));
	}
	CoreImage ourImage ;
	CoreImage imageToDraw ;
	CoreGraphics graphicsToUse;
	RendererPulpeC ourRenderer;
	
	public FlatSpritePCAdd(CoreImage imageToUse)
	{
		super(0,0,1);
		super.setAlpha(255);
		ourRenderer = RendererPulpeC.getInstance();
		
		ourImage = imageToUse;
		imageToDraw = ourImage;
		
		ourBlending = BlendMode.DstOut(); 
	}
	
	public CoreImage getImageToDraw() {
		return imageToDraw;
	}
	public void setImageToDraw(CoreImage imageToDraw) {
		this.imageToDraw = imageToDraw;
		this.ourImage = imageToDraw;

		this.imageToDraw = ourImage.fade(this.getAlpha());
	}
	@Override
	public void setAlpha(int alpha) {
		// TODO Auto-generated method stub
		super.setAlpha(alpha);
		
		imageToDraw = ourImage.fade(alpha);
	}
	/**
	 * Set the blending for all sprites to come
	 * @param blending
	 */
	public static void setBlending(BlendMode blending)
	{
		ourBlending = blending;
	}
	/**
	 * Set the blending with int values
	 */
	public static void setBlending(int nbBlending)
	{
		switch (nbBlending)
		{
			case DST_OUT:
				ourBlending = BlendMode.DstOut(); 
				break;
			case SRC_ATOP:
				ourBlending = BlendMode.SrcAtop();
				break;
			case MULTIPLY:
				ourBlending = BlendMode.Multiply();
				break;
			case XOR:
				ourBlending = BlendMode.Xor();
				break;
			default:
				ourBlending = BlendMode.Add();
		}
	}
	/* (non-Javadoc)
	 * @see subengine.gfxentities.Drawable2DEntity#drawMe(java.awt.Graphics2D)
	 */
	@Override
	public boolean drawMe()
	{
		graphicsToUse = ourRenderer.getGraphics();
		graphicsToUse.setBlendMode(ourBlending);
		graphicsToUse.drawScaledImage(imageToDraw, (int )(this.getX()-this.getSize()*ourImage.getWidth()/2), (int )(this.getY()-this.getSize()*ourImage.getHeight()/2), (int )(ourImage.getWidth()*this.getSize()), (int )(ourImage.getHeight()*this.getSize()));
		graphicsToUse.setBlendMode(BlendMode.SrcOver());
		updateAttachedObjects();
		//System.out.println("Painted one sprite - PC");
		return true;
	}

}
