/*
 * Created on 1 mar 2009
 *
 */

/*
 * Copyright (c)2009 Alain Becam
 */

package javazoom.jl.player.advanced;

public class PlayerThread extends Thread
{
	AdvancedPlayer myPlayer;
	int start;
	int end;
	boolean endAsked=false;
	boolean endOfFile=false;
	boolean isRunning=false;
	
	public PlayerThread(AdvancedPlayer player,final int start, final int end)
	{
		super("PlayerThread");
		
		myPlayer = player;
		this.start=start;
		this.end=end;
	}

	@Override
	public void run()
	{
		isRunning = true;
		while (!endAsked && !endOfFile)
		{
			try
			{
				endOfFile = myPlayer.play(10);
				//System.out.println("Playing "+endAsked+" - "+endOfFile);
			}
			catch (Exception e)
			{
				//throw new RuntimeException(e.getMessage());
				e.printStackTrace();
			}
		}
		isRunning = false;
		
		if (!endOfFile)
			myPlayer.stop();	
		
		myPlayer.getPlayBackListener().playbackThreadFinished();
	}

	public void askEnd()
	{
		endAsked=true;
	}

	public boolean isRunning()
	{
		return isRunning;
	}

	/**
	 * If we want to use the same thread for all songs, you must set the new player 
	 * first (in jlap). PlayerThread should also become a singleton (because
	 * jlap's methods are static)
	 * @param myPlayer
	 */
	public void setMyPlayer(AdvancedPlayer myPlayer)
	{
		this.myPlayer = myPlayer;
	}
	
	
}
