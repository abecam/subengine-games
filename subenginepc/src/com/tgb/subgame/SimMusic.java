/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2008-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.subgame;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;
import javazoom.jl.player.advanced.AdvancedPlayer;

/**
 * Singleton class to play the music. Use a slightly enhanced version of JavaZoom
 * mp3 player (added simply a threaded player).<br>
 * Event based, use a callback to play the next file. If a problem occurs, we play
 * the next file. The pause is not available in JavaZoom, so stopping and playing
 * back play from the start.
 *  
 * @author Alain Becam
 *
 */
public class SimMusic extends PlaybackListener
{	
	String files[]={
			"Atlantean_Twilight.mp3",
			"Dark_Star.mp3",
			"Majestic_Hills.mp3",
			"Supernatural.mp3",
			"Achilles.mp3",
			"Exciting_Trailer.mp3",
			"Ghost_Processional.mp3",			
			"Chase.mp3",
			"Dangerous.mp3",			
			"Faceoff.mp3",
			"Feral_Chase.mp3",
			"Crisis.mp3",
			"Road_to_Hell.mp3",
			"A_Mission.mp3",		
			"At_Launch.mp3",
			"Impending_Boom.mp3",
			"Interloper.mp3",
			"Love_Song.mp3",
			"Noble_Race.mp3",
			"Serpentine_Trek.mp3",
			"Tectonic.mp3",
			"The_Reveal.mp3",
			"We_Got_Trouble.mp3",
			"Weight_of_Responsibility.mp3"};
	
	static AdvancedPlayer player = null;
	
	static String lastFile;
	static String currentMusic;
	static boolean isStopped=false;
	static boolean isPlaying=false;
	static boolean mute=false;
	
	static private SimMusic instance = null;
	
	static final int introMusic=0;
	static final int extroGoodMusic=1;
	static final int extroBadMusic=2;
	
	static boolean specialPlay=false; // If a special song is requested...
	static int specialSong=introMusic;
	
	static int currentSong=3;
	
	private SimMusic()
	{
		;
	}
	
	/**
	 * Be careful, it is not currently synchronised (as we know that in our current project it is called a first time very early).
	 * @return
	 */
	public static SimMusic getInstance()
	{
		if (instance== null)
			instance = new SimMusic();
		
		return instance;
	}
	
	/**
	 */
	protected InputStream getURLInputStream(String name)
		throws Exception
	{

		URL url = new URL(name);
		InputStream fin = url.openStream();
		BufferedInputStream bin = new BufferedInputStream(fin);
		return bin;
	}
	
	public void playFile()
	{
		//java.util.Random myRandom= new java.util.Random();
		
		int nextFileIndex=0;
		
		if (!specialPlay)
		{
			nextFileIndex=currentSong++; // myRandom.nextInt(files.length);
		}
		
		if (nextFileIndex >= files.length)
		{
			nextFileIndex = 3;
			currentSong=3;
		}
		
		isStopped=false;
		isPlaying = false;
		if (specialPlay)
		{
			lastFile="file:Assets\\Music\\"+files[specialSong];
			specialPlay=false;
		}
		else
		{
			lastFile="file:Assets\\Music\\"+files[nextFileIndex];
		}
		
		//System.out.println("Play file from "+lastFile+", init done ");
		
		try
		{
			playURL();
			System.out.println("Playing");
			
//			if (mute)
//				player.stop();
			currentMusic=lastFile.substring(lastFile.lastIndexOf('/')+1).replace('_', ' ');
			System.out.println("Playing "+currentMusic);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @throws Exception
	 * @throws JavaLayerException
	 */
	private void playURL() throws Exception, JavaLayerException
	{
		InputStream myInput=this.getURLInputStream(lastFile);
		System.out.println("Input created");
		if (myInput != null)
			player = javazoom.jl.player.advanced.jlap.playMp3(myInput, 0, Integer.MAX_VALUE, this);
		else
		{
			System.out.println("No input from "+lastFile);
		}
	}
	
	public void playLastFile()
	{
		isStopped=false;
		
		try
		{
			System.out.println("Re-Play file from "+lastFile);
			playURL();
			currentMusic=lastFile.substring(lastFile.lastIndexOf('/')+1).replace('_', ' ');
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void playSplashMusic()
	{
		specialPlay = true;
		specialSong = SimMusic.introMusic;
		
		if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
			javazoom.jl.player.advanced.jlap.getMyPlayerThread().askEnd();
		else
		{
			playFile();
		}
		
		
//		isStopped=false;
//		
//		lastFile="http://www.theGiantBall.com/~GBT/Music/"+files[3];
//		try
//		{
//			playURL();
//			currentMusic=lastFile.substring(lastFile.lastIndexOf('/')+1).replace('_', ' ');
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
	}
	
	public void playGoodEndMusic()
	{
		specialPlay = true;
		specialSong = SimMusic.extroGoodMusic;
		
		if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
			javazoom.jl.player.advanced.jlap.getMyPlayerThread().askEnd();
		else
		{
			playFile();
		}
//		isStopped=false;
//		
//		lastFile="http://www.theGiantBall.com/~GBT/Music/"+files[1];
//		try
//		{
//			playURL();
//			currentMusic=lastFile.substring(lastFile.lastIndexOf('/')+1).replace('_', ' ');
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
	}
	
	public void playBadEndMusic()
	{
		specialPlay = true;
		specialSong = SimMusic.extroBadMusic;
		
		if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
			javazoom.jl.player.advanced.jlap.getMyPlayerThread().askEnd();
		else
		{
			playFile();
		}
//		isStopped=false;
//		
//		lastFile="http://www.theGiantBall.com/~GBT/Music/"+files[2];
//		try
//		{
//			playURL();
//			currentMusic=lastFile.substring(lastFile.lastIndexOf('/')+1).replace('_', ' ');
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
	}
	
	public void stop()
	{
		isStopped=true;
		
		if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
			javazoom.jl.player.advanced.jlap.getMyPlayerThread().askEnd();
//		if (player != null)
//			player.stop();
	}
	
	public void next()
	{
		if (!isStopped)
		{
			if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
				javazoom.jl.player.advanced.jlap.getMyPlayerThread().askEnd();
//			if (player != null)
//				player.stop();
		}
		else
		{
			if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
			{
				if (!javazoom.jl.player.advanced.jlap.getMyPlayerThread().isRunning())
					playFile();
				else
				{
					System.err.println("Previous thread still running");
				}
			}
			else
			{
				playFile();
			}
		}
	}
	
	public void pause()
	{
		if (player != null)
			player.stop();
	}
	
	public void mute()
	{
		//javazoom.jl.player.advanced.jlap.mute();
		if (mute)
			mute=false;
		else
			mute=true;
	}
	
	public void setVolume(float volume)
	{
		;
	}

	/* (non-Javadoc)
	 * @see javazoom.jl.player.advanced.PlaybackListener#playbackFinished(javazoom.jl.player.advanced.PlaybackEvent)
	 */
	@Override
	public void playbackFinished(PlaybackEvent evt)
	{
		System.out.println("Playback finished, stopped  "+isStopped);
		
		isPlaying=false;
		
//		// Next file so...
//		if (!isStopped)
//		{
//			//playFile();
//			if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
//			{
//				if (!javazoom.jl.player.advanced.jlap.getMyPlayerThread().isRunning())
//					playFile();
//				else
//				{
//					System.err.println("Previous thread still running");
//				}
//			}
//			else
//			{
//				System.err.println("Playback finished without any existing thread ???");
//				playFile();
//			}
//		}
	}

	
	@Override
	public void playbackThreadFinished()
	{
		// Next file so...
		if (!isStopped)
		{
			//playFile();
			if (javazoom.jl.player.advanced.jlap.getMyPlayerThread() != null)
			{
				if (!javazoom.jl.player.advanced.jlap.getMyPlayerThread().isRunning())
					playFile();
				else
				{
					System.err.println("Previous thread still running");
				}
			}
			else
			{
				System.err.println("Playback finished without any existing thread ???");
				playFile();
			}
		}
	}

	/* (non-Javadoc)
	 * @see javazoom.jl.player.advanced.PlaybackListener#playbackStarted(javazoom.jl.player.advanced.PlaybackEvent)
	 */
	@Override
	public void playbackStarted(PlaybackEvent evt)
	{
		// Goooood.
		isPlaying = true;
		//simGame.setMusicName(currentMusic);
	}

	public String getLastFile()
	{
		return lastFile;
	}

	public void setLastFile(String lastFile)
	{
		this.lastFile = lastFile;
	}

	public String getCurrentMusic()
	{
		return currentMusic;
	}

	public void setCurrentMusic(String currentMusic)
	{
		this.currentMusic = currentMusic;
	}

	public boolean isStopped()
	{
		return isStopped;
	}

	public void setStopped(boolean isStopped)
	{
		this.isStopped = isStopped;
	}

	public boolean isMute()
	{
		return mute;
	}

	public void setMute(boolean mute)
	{
		this.mute = mute;
	}
}
