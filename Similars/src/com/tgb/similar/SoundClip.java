/*
 * Created on 23 mars 2020
 *
 */

/*
 * From https://www.javaworld.com/article/2077521/java-tip-24--how-to-play-audio-in-applications.html
 * 
 * "Low" level sound playing. Not really ideal...
 * 
 * Could be replaced by JavaFX methods
 */

package com.tgb.similar;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class SoundClip
{
	String fileName;
	AudioStream soundStreamToPlay;
	
	public SoundClip(String fileName)
	{
		loadClip(fileName);
	}
	
	public void loadClip(String fileName)
	{
		this.fileName = fileName;
	}
	
	public void play()
	{
		// The sound must be stopped first or it will become messy.
		stopSound();
		
		// Create an AudioStream object from the input stream.
		InputStream in;
		try
		{
			// Open an input stream  to the audio file.	
			in = new FileInputStream(fileName);
			soundStreamToPlay = new AudioStream(in);
			// Use the static class member "player" from class AudioPlayer to play clip.
			AudioPlayer.player.start(soundStreamToPlay);
		} 
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public void stopSound()
	{
		// Similarly, to stop the audio.
		AudioPlayer.player.stop(soundStreamToPlay);
	}
}
