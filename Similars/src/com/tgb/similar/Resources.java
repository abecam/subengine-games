/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2006-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.similar;

import java.net.URI;
import java.net.URL;

/**
 * 
 */
public class Resources {
    
    /**
     * Do not construct an instance of this class.
     */
    private Resources() {
        
    }

    /**
     * Return the URL of the filename under the resources directory
     */
    public static URL getResource(String filename)
    {
        // URL url = Resources.class.getResource(filename);
        try
        {
            //if (!filename.startsWith("file:"))
            //    filename = "file:///C:/Program%20Files/Eclipse/workspace/" + filename;
        	/*if (!filename.startsWith("file:"))
             	filename = "file:///C:/Program/EclipseWTP/eclipse/workspace/" + filename;*/
//            if (!filename.startsWith("file:"))
//                filename = "http://www.thegiantball.com/~TGB06/" + filename;
            URI uri = new URI(filename);
            URL url = uri.toURL();
            return url;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
