/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2006-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.similar;

import javax.swing.JFrame;

/**
 * @author Alain Becam
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class StartSimilar extends JFrame
{
    Similar2DEmb Similar;
    
    public void destroy()
    {
        // TODO Auto-generated method stub
    	Similar.destroy();
        System.out.println("StartSIM: leaving");
        this.removeAll();
        this.setEnabled(false);
        //super.destroy();
    }

    public void init()
	{
    	JFrame.setDefaultLookAndFeelDecorated(true);
		this.setName("Draw map");
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		this.setSize(1200, 800);
		this.setLocationRelativeTo( null );
		//this.addKeyListener(this);	
		
		this.setVisible(true);
		
		// 2D version !!!
		Similar = new Similar2DEmb();

		Similar.startSIM(this);
		
		//pack();
		
		
		//pack();
	}

	/**
     * @param args the command line arguments
     */
    public static void main(final String args[])
    {
    	StartSimilar ourNewSimapp = new StartSimilar();
    	
    	ourNewSimapp.init();
    }
}
