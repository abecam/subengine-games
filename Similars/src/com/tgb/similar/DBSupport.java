/*
 * This software is distributed under the MIT License
 *
 * Copyright (c) 2006-2020 Alain Becam
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

package com.tgb.similar;

import java.io.IOException;
import java.sql.*;
import java.util.*;
//import org.apache.commons.dbutils.*;
//import org.apache.commons.dbutils.handlers.*;

/**
 * @author Alain Becam
 *
 */
public class DBSupport
{
	static Connection conn = null;
	/*
	 * Just add one score to the database. The date is added automatically
	 */
//	public static void addScore(String name, int score, int nbColors, int size)
//	{
//		connect();
//		
//		try
//		{
//			Statement statement = conn.createStatement();
//			
//			java.sql.Date mySQLDate = new java.sql.Date(new java.util.Date().getTime());
//			System.out.println("Score date: "+mySQLDate);
//			statement.executeUpdate("INSERT INTO Scores (Name,Score,NBColors,Size, 	DateOfScore) VALUES('"+name+"',"+score+","+nbColors+","+size+",'"+mySQLDate.toString()+"')");
//		} catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//		disconnect();
//	}
	
//	public static int findPos(int score, int nbColors, int size)
//	{
//		connect();
//		
//		try
//		{
//			//Statement statement = conn.createStatement();
//			
//			// pr�paration d'un gestionnaire de requ�te
//			QueryRunner qRunner = new QueryRunner();
//			
//			//statement.execute("SELECT * FROM SCORES WHERE score<"+score+" AND nbColors="+nbColors+" AND size="+size);
//			List list1 = new ArrayList();
//			try {
//				list1 =
//					(List) qRunner.query(
//						conn,
//						"SELECT * FROM Scores WHERE Score>"+score+" AND NBColors="+nbColors+" AND Size="+size,
//						new ArrayListHandler());
//			} catch (SQLException e2) {
//				e2.printStackTrace();
//			}
//			disconnect();
//			return list1.size();
//		} catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//		disconnect();
//		return -1; // Unknown
//	}
//	
//	public static void connect()
//	{
//		conn = null;
//		//String jdbcURL = "jdbc:mysql://localhost:3306/scores";
//		String jdbcURL = "jdbc:mysql://www.theGiantBall.com:3306/SimScores";
//		String jdbcDriver = "org.gjt.mm.mysql.Driver";
//		//chargement du Driver
//		boolean connect;
//		connect=DbUtils.loadDriver(jdbcDriver);
//		if(!connect){
//			//pas de Driver on s'arr�te
//			System.out.println("Driver absent.");
//			System.exit(0) ;
//		}
//		try {
//			//�tablissement de la connexion au SGBD
//			//conn = DriverManager.getConnection(jdbcURL, "root", "password");
//			conn = DriverManager.getConnection(jdbcURL, "simplayer2", "B1c8Ima");
//		} catch (SQLException e1) {
//			e1.printStackTrace();
//		}
//	}
//	
//	public static void disconnect()
//	{
//		DbUtils.closeQuietly(conn);
//	}
//	
//		public static void main(String[] args) {
//			conn = null;
//			String jdbcURL = "jdbc:mysql://localhost:3306/scores";
//			String jdbcDriver = "org.gjt.mm.mysql.Driver";
//			//chargement du Driver
//			boolean connect;
//			connect=DbUtils.loadDriver(jdbcDriver);
//			if(!connect){
//				//pas de Driver on s'arr�te
//				System.out.println("Driver absent.");
//				System.exit(0) ;
//			}
//			try {
//				//�tablissement de la connexion au SGBD
//				conn = DriverManager.getConnection(jdbcURL, "root", "password");
//			} catch (SQLException e1) {
//				e1.printStackTrace();
//			}
//			//pr�paration d'un gestionnaire de requ�te
//			QueryRunner qRunner = new QueryRunner();
//			//utilisation simple avec un ArrayList
//			List list1 = new ArrayList();
//			try {
//				list1 =
//					(List) qRunner.query(
//						conn,
//						"SELECT * FROM scores ORDER BY Score",
//						new ArrayListHandler());
//			} catch (SQLException e2) {
//				e2.printStackTrace();
//			}
//			//affichage du r�sultat
//			System.out.println("en ArrayList");
//			for (int i = 0; i < list1.size(); i++) {
//				Object data[] = (Object[]) list1.get(i);
//				System.out.println("\t"+data[0]+" -> "+data[1]+" -> "+data[2]+" -> "+data[3]+" -> "+data[5]);
//			}
//			
//			try
//			{
//				Statement statement = conn.createStatement();
//				//qRunner.query("ADD "ALAIN" ", arg1)
//				java.sql.Date mySQLDate = new java.sql.Date(new java.util.Date().getTime());
//				System.out.println(mySQLDate);
//				int col = statement.executeUpdate("INSERT INTO SCORES (Name,Score,NBCOLORS,SIZE,dateOfScore) VALUES('Alain4',100,31,2,'"+mySQLDate.toString()+"')");
//			} catch (Exception e)
//			{
//				e.printStackTrace();
//			}
//			
//			//affichage du r�sultat.
//			System.out.println("Requete avec parametre");
//			
//			DbUtils.closeQuietly(conn);
//		}
}
